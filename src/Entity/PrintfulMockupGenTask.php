<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="`printful_mockup_gen_task`")
 * @ORM\Entity(repositoryClass="App\Repository\PrintfulMockupGenTaskRepository")
 */
class PrintfulMockupGenTask extends AbstractEntity
{
    const STATUS_PENDING = 0;
    const STATUS_PROCESSING = 100;
    const STATUS_COMPLETED = 200;
    const STATUS_FAILED = 400;

    /**
     * @var string|null
     *
     * @ORM\Column(name="task_key", type="string", length=255, nullable=true)
     */
    protected $taskKey;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    protected $status = self::STATUS_PENDING;

    /**
     * @var string|null
     *
     * @ORM\Column(name="error", type="text", nullable=true)
     */
    protected $error;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mockups", type="text", nullable=true)
     */
    protected $mockups;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_placements", type="text", nullable=true)
     */
    protected $imagePlacements;

    /**
     * @var int
     *
     * @ORM\Column(name="printful_product_id", type="integer", length=11, nullable=false)
     */
    protected $printfulProductId = 0;

    /**
     * @var Product|null
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @var ProductType|null
     *
     * @ORM\ManyToOne(targetEntity="ProductType")
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id")
     */
    protected $productType;

    /**
     * @var bool
     *
     * @ORM\Column(name="reinitiated", type="boolean", nullable=false)
     */
    protected $reinitiated = false;


    /**
     * GETTERS AND SETTERS:
     */


    /**
     * @return null|string
     */
    public function getTaskKey()
    {
        return $this->taskKey;
    }

    /**
     * @param null|string $taskKey
     * @return PrintfulMockupGenTask
     */
    public function setTaskKey($taskKey)
    {
        $this->taskKey = $taskKey;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return PrintfulMockupGenTask
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param null|string $error
     * @return PrintfulMockupGenTask
     */
    public function setError($error)
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getMockups()
    {
        return $this->mockups;
    }

    /**
     * @param null|string $mockups
     * @return PrintfulMockupGenTask
     */
    public function setMockups($mockups)
    {
        $this->mockups = $mockups;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrintfulProductId(): int
    {
        return $this->printfulProductId;
    }

    /**
     * @param int $printfulProductId
     * @return PrintfulMockupGenTask
     */
    public function setPrintfulProductId(int $printfulProductId): PrintfulMockupGenTask
    {
        $this->printfulProductId = $printfulProductId;
        return $this;
    }

    /**
     * @return Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     * @return PrintfulMockupGenTask
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return ProductType|null
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param ProductType|null $productType
     * @return PrintfulMockupGenTask
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getImagePlacements()
    {
        return $this->imagePlacements;
    }

    /**
     * @param null|string $imagePlacements
     * @return PrintfulMockupGenTask
     */
    public function setImagePlacements($imagePlacements)
    {
        $this->imagePlacements = $imagePlacements;
        return $this;
    }

    /**
     * @return bool
     */
    public function getReinitiated(): bool
    {
        return $this->reinitiated;
    }

    /**
     * @param bool $reinitiated
     * @return PrintfulMockupGenTask
     */
    public function setReinitiated(bool $reinitiated): PrintfulMockupGenTask
    {
        $this->reinitiated = $reinitiated;
        return $this;
    }
}