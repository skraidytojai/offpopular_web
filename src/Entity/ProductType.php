<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Finder\Finder;
use App\Service\HumanFriendlyService;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="`product_type`", indexes={
 *           @ORM\Index(name="group_idx", columns={"group"}),
 *           @ORM\Index(name="is_free_ship_excluded_idx", columns={"is_free_ship_excluded"})
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\ProductTypeRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class ProductType extends AbstractEntity
{
    public function __construct()
    {
        $this->productVariants = new ArrayCollection;
        $this->reviews = new ArrayCollection;

        parent::__construct();
    }

    const STRATEGY_DEFAULT = 0;
    const STRATEGY_MIRRORED_SLEEVE_RIGHT = 1; // T-Shirts. For sleeve_right placement file, so that it would look the same as the left sleeve.
    const STRATEGY_EXTENDED_MIRRORED = 2; // Pants, Shorts. So that both sides would look symmetrically.

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title_short", type="string", length=50, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $titleShort;

    /**
     * Plural form
     *
     * @var string|null
     *
     * @ORM\Column(name="title_for_menu", type="string", length=50, nullable=true)
     */
    protected $titleForMenu;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title_for_search", type="string", length=50, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $titleForSearch;

    /**
     * separated by comma (,)
     *
     * @var string|null
     *
     * @ORM\Column(name="title_for_search_alternatives", type="text", nullable=true)
     */
    protected $titleForSearchAlternatives;

    /**
     * @var string|null
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="slug", type="string", unique=true, length=255, nullable=true)
     */
    protected $slug;

    /**
     * @var int|null
     *
     * @ORM\Column(name="printful_product_id", type="integer", nullable=false)
     */
    protected $printfulProductId;

    /**
     * @var string|null
     *
     * Comma separated
     *
     * @ORM\Column(name="printful_variant_ids", type="string", length=255, nullable=true)
     */
    protected $printfulVariantIds;

    /**
     * @var string|null
     *
     * Raw info about variants
     *
     * @ORM\Column(name="printful_variants_json", type="text", nullable=true)
     */
    protected $printfulVariantsJson;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_published", type="boolean", nullable=false)
     */
    protected $isPublished = false;

    /**
     * @var string|null
     *
     * Comma separated, like: default,back,front
     *
     * @ORM\Column(name="placement_files", type="string", length=255, nullable=true)
     */
    protected $placementFiles;

    /**
     * @var ArrayCollection|ProductVariant[]
     *
     * @ORM\OneToMany(targetEntity="ProductVariant", mappedBy="productType", fetch="EAGER")
     * @ORM\OrderBy({"rank" = "ASC"})
     *
     * @JMS\Expose()
     */
    protected $productVariants;

    /**
     * @var int
     *
     * @ORM\Column(name="mockup_generation_strategy", type="smallint", nullable=false)
     */
    protected $mockupGenerationStrategy = self::STRATEGY_DEFAULT;

    /**
     * @var string|null
     *
     * @ORM\Column(name="`group`", type="string", length=50, nullable=true)
     */
    protected $group;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sizing_intro_text", type="text", nullable=true)
     */
    protected $sizingIntroText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="features_intro_text", type="text", nullable=true)
     */
    protected $featuresIntroText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="features_bullet_points", type="text", nullable=true)
     */
    protected $featuresBulletPoints;

    /**
     * @var ProductTypeCategory|null
     *
     * @ORM\ManyToOne(targetEntity="ProductTypeCategory", inversedBy="productTypes")
     * @ORM\JoinColumn(name="product_type_category_id", referencedColumnName="id")
     *
     * @JMS\Expose()
     */
    protected $productTypeCategory;

    /**
     * @var int
     *
     * @ORM\Column(name="rank", type="integer", nullable=false)
     */
    protected $rank = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="short_description", type="text", nullable=true)
     */
    protected $shortDescription;

    /**
     * @var bool
     *
     * @ORM\Column(name="hide_size_guide", type="boolean", nullable=false)
     */
    protected $hideSizeGuide = false;

    /**
     * @var bool
     *
     * If true, this product type does not participate towards the free shipping calculation
     *
     * @ORM\Column(name="is_free_ship_excluded", type="boolean", nullable=false)
     *
     * @JMS\Expose()
     */
    protected $isFreeShipExcluded = false;

    /**
     * @var ArrayCollection|Review[]
     *
     * @ORM\OneToMany(targetEntity="Review", mappedBy="productType")
     */
    protected $reviews;


    public function __toString()
    {
        return $this->getId() . ': ' . $this->getTitle();
    }

    /**
     * CUSTOM METHODS:
     */



    /**
     * GETTERS AND SETTERS:
     */


    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return ProductType
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param null|string $slug
     * @return ProductType
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPrintfulProductId()
    {
        return $this->printfulProductId;
    }

    /**
     * @param int|null $printfulProductId
     * @return ProductType
     */
    public function setPrintfulProductId($printfulProductId)
    {
        $this->printfulProductId = $printfulProductId;
        return $this;
    }

    /**
     * @param bool $asArray
     * @return array|null|string
     */
    public function getPrintfulVariantIds($asArray = false)
    {
        return $asArray ? explode(',', $this->printfulVariantIds) : $this->printfulVariantIds;
    }

    /**
     * @param null|string $printfulVariantIds
     * @return ProductType
     */
    public function setPrintfulVariantIds($printfulVariantIds)
    {
        $this->printfulVariantIds = $printfulVariantIds;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPrintfulVariantsJson()
    {
        return $this->printfulVariantsJson;
    }

    /**
     * @param null|string $printfulVariantsJson
     * @return ProductType
     */
    public function setPrintfulVariantsJson($printfulVariantsJson)
    {
        $this->printfulVariantsJson = $printfulVariantsJson;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool $isPublished
     * @return ProductType
     */
    public function setIsPublished(bool $isPublished): ProductType
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @param bool $asArray
     * @return array|null|string
     */
    public function getPlacementFiles($asArray = false)
    {
        return $asArray ? explode(',', $this->placementFiles) : $this->placementFiles;
    }

    /**
     * @param null|string $placementFiles
     * @return ProductType
     */
    public function setPlacementFiles($placementFiles)
    {
        $this->placementFiles = $placementFiles;
        return $this;
    }

    /**
     * @return ProductVariant[]|ArrayCollection
     */
    public function getProductVariants()
    {
        return $this->productVariants;
    }

    /**
     * @param ProductVariant[]|ArrayCollection $productVariants
     * @return ProductType
     */
    public function setProductVariants($productVariants)
    {
        $this->productVariants = $productVariants;
        return $this;
    }

    /**
     * @return int
     */
    public function getMockupGenerationStrategy(): int
    {
        return $this->mockupGenerationStrategy;
    }

    /**
     * @param int $mockupGenerationStrategy
     * @return ProductType
     */
    public function setMockupGenerationStrategy(int $mockupGenerationStrategy): ProductType
    {
        $this->mockupGenerationStrategy = $mockupGenerationStrategy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param null|string $group
     * @return ProductType
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitleShort()
    {
        return $this->titleShort;
    }

    /**
     * @param null|string $titleShort
     * @return ProductType
     */
    public function setTitleShort($titleShort)
    {
        $this->titleShort = $titleShort;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSizingIntroText()
    {
        return $this->sizingIntroText;
    }

    /**
     * @param null|string $sizingIntroText
     * @return ProductType
     */
    public function setSizingIntroText($sizingIntroText)
    {
        $this->sizingIntroText = $sizingIntroText;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFeaturesIntroText()
    {
        return $this->featuresIntroText;
    }

    /**
     * @param null|string $featuresIntroText
     * @return ProductType
     */
    public function setFeaturesIntroText($featuresIntroText)
    {
        $this->featuresIntroText = $featuresIntroText;
        return $this;
    }

    /**
     * @param bool $asArray
     * @return array|null|string
     */
    public function getFeaturesBulletPoints($asArray = false)
    {
        if ($asArray === true) {
            if (!$this->featuresBulletPoints) {
                return [];
            }
            return explode(';', $this->featuresBulletPoints);
        }

        return $this->featuresBulletPoints;
    }

    /**
     * @param null|string $featuresBulletPoints
     * @return ProductType
     */
    public function setFeaturesBulletPoints($featuresBulletPoints)
    {
        $this->featuresBulletPoints = $featuresBulletPoints;
        return $this;
    }

    /**
     * @return ProductTypeCategory|null
     */
    public function getProductTypeCategory()
    {
        return $this->productTypeCategory;
    }

    /**
     * @param ProductTypeCategory|null $productTypeCategory
     * @return ProductType
     */
    public function setProductTypeCategory($productTypeCategory)
    {
        $this->productTypeCategory = $productTypeCategory;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitleForMenu()
    {
        return $this->titleForMenu;
    }

    /**
     * @param null|string $titleForMenu
     * @return ProductType
     */
    public function setTitleForMenu($titleForMenu)
    {
        $this->titleForMenu = $titleForMenu;
        return $this;
    }

    /**
     * @return int
     */
    public function getRank(): int
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     * @return ProductType
     */
    public function setRank(int $rank): ProductType
    {
        $this->rank = $rank;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param null|string $shortDescription
     * @return ProductType
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHideSizeGuide(): bool
    {
        return $this->hideSizeGuide;
    }

    /**
     * @param bool $hideSizeGuide
     * @return ProductType
     */
    public function setHideSizeGuide(bool $hideSizeGuide): ProductType
    {
        $this->hideSizeGuide = $hideSizeGuide;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsFreeShipExcluded(): bool
    {
        return $this->isFreeShipExcluded;
    }

    /**
     * @param bool $isFreeShipExcluded
     * @return ProductType
     */
    public function setIsFreeShipExcluded(bool $isFreeShipExcluded): ProductType
    {
        $this->isFreeShipExcluded = $isFreeShipExcluded;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitleForSearch()
    {
        return $this->titleForSearch;
    }

    /**
     * @param null|string $titleForSearch
     * @return ProductType
     */
    public function setTitleForSearch($titleForSearch)
    {
        $this->titleForSearch = $titleForSearch;
        return $this;
    }

    /**
     * @param bool $asArray
     * @return array|null|string
     */
    public function getTitleForSearchAlternatives($asArray = false)
    {
        if ($asArray) {
            $array = explode(',', $this->titleForSearchAlternatives);
            $array = array_filter($array, function ($element) {
                return !empty($element);
            });

            return $array;
        }

        return $this->titleForSearchAlternatives;
    }

    /**
     * @param null|string $titleForSearchAlternatives
     * @return ProductType
     */
    public function setTitleForSearchAlternatives($titleForSearchAlternatives)
    {
        $this->titleForSearchAlternatives = $titleForSearchAlternatives;
        return $this;
    }

    /**
     * @return Review[]|ArrayCollection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param Review[]|ArrayCollection $reviews
     * @return ProductType
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
        return $this;
    }
}