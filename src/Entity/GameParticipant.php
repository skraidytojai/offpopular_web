<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="`game_participant`")
 * @ORM\Entity(repositoryClass="App\Repository\GameParticipantRepository")
 */
class GameParticipant extends AbstractEntity
{

    /**
     * @var int
     *
     * @ORM\Column(name="game_no", type="integer", nullable=false)
     */
    protected $gameNo = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ticket_count", type="integer", nullable=false)
     */
    protected $ticketCount = 1;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string|null
     *
     * @ORM\Column(name="source", type="string", nullable=true, length=255)
     */
    protected $source;


    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return int
     */
    public function getGameNo(): int
    {
        return $this->gameNo;
    }

    /**
     * @param int $gameNo
     * @return GameParticipant
     */
    public function setGameNo(int $gameNo): GameParticipant
    {
        $this->gameNo = $gameNo;
        return $this;
    }

    /**
     * @return int
     */
    public function getTicketCount(): int
    {
        return $this->ticketCount;
    }

    /**
     * @param int $ticketCount
     * @return GameParticipant
     */
    public function setTicketCount(int $ticketCount): GameParticipant
    {
        $this->ticketCount = $ticketCount;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return GameParticipant
     */
    public function setUser(User $user): GameParticipant
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param null|string $source
     * @return GameParticipant
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }
}