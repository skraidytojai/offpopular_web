<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Finder\Finder;
use App\Service\HumanFriendlyService;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Table(name="`product_mockup`", indexes={
 *           @ORM\Index(name="type_idx", columns={"type"}),
 *           @ORM\Index(name="is_obsolete_idx", columns={"is_obsolete"}),
 *           @ORM\Index(name="is_uploaded_to_s3_idx", columns={"is_uploaded_to_s3"}),
 *           @ORM\Index(name="is_thumbs_checked_idx", columns={"is_thumbs_checked"})
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\ProductMockupRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class ProductMockup extends AbstractEntity
{
    const TYPE_MAIN = 1;
    const TYPE_EXTRA = 2;

    public function __construct()
    {
        $this->productVariants = new ArrayCollection;
        $this->jpgFullCreatedAt = new \DateTime;

        parent::__construct();
    }

    /**
     * @var string|null
     *
     * @ORM\Column(name="printful_picture_url", type="string", length=255, nullable=true)
     */
    protected $printfulPictureUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="server_picture_path", type="string", length=255, nullable=true)
     *
     * @JMS\Expose()
     *
     * @deprecated Because we're migrating to aws, no storage of files on web server anymore
     */
    protected $serverPicturePath;

    /**
     * @var Product[]|array|Collection
     *
     * @ORM\ManyToMany(targetEntity="ProductVariant", mappedBy="productMockups")
     */
    protected $productVariants;

    /**
     * @var ProductType|null
     *
     * @ORM\ManyToOne(targetEntity="ProductType")
     * @ORM\JoinColumn(name="default_product_type_id", referencedColumnName="id")
     */
    protected $defaultProductType;

    /**
     * @var Product|null
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="productMockups")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @var string|null
     *
     * @ORM\Column(name="placement", type="string", length=255, nullable=true)
     */
    protected $placement;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint", nullable=false)
     *
     * @JMS\Expose()
     */
    protected $type = self::TYPE_MAIN;

    /**
     * Mainly for log/debug reasons. See ManyToMany: productVariants instead.
     *
     * @var string|null
     *
     * @ORM\Column(name="printful_variant_ids", type="string", length=255, nullable=true)
     */
    protected $printfulVariantIds;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_obsolete", type="boolean", nullable=false)
     */
    protected $isObsolete = false;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="jpg_full_created_at", type="datetime", nullable=true)
     */
    protected $jpgFullCreatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="jpg350px_created_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $jpg350pxCreatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="jpg350px_failed_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $jpg350pxFailedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="webp350px_created_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $webp350pxCreatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="webp350px_failed_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $webp350pxFailedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="thumb50px_created_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $thumb50pxCreatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="thumb100px_created_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $thumb100pxCreatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="thumb500px_created_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $thumb500pxCreatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="thumb50px_failed_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $thumb50pxFailedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="thumb100px_failed_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $thumb100pxFailedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="thumb500px_failed_at", type="datetime", nullable=true)
     *
     * @deprecated
     */
    protected $thumb500pxFailedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_uploaded_to_s3", type="boolean", nullable=false)
     */
    protected $isUploadedToS3 = false;

    /**
     * @var bool
     *
     * Because of automatic s3->lambda bug, we'll be rechecking product mockups up to id: 500
     *
     * @ORM\Column(name="is_thumbs_checked", type="boolean", nullable=false)
     */
    protected $isThumbsChecked = false;

    /**
     * @var \DateTime|null
     *
     * If needed - if thumbs don't exist, we'll initiate their generation from our side
     *
     * @ORM\Column(name="thumbs_regen_initiated_at", type="datetime", nullable=true)
     */
    protected $thumbsRegenInitiatedAt;

    /**
     * @var bool
     *
     * If thumb regen was initiated from our side - did we get a sucessful 200 response from lambda function?
     *
     * @ORM\Column(name="is_thumbs_regen_success", type="boolean", nullable=false)
     */
    protected $isThumbsRegenSuccess = false;


    /**
     * CUSTOM METHODS:
     */

    public function generateS3FileKey()
    {
        return $this->getProduct()->getId() . '/' . $this->getId() . '/' . $this->getId() .  '.jpg';
    }

    public function addProductVariant(ProductVariant $productVariant)
    {
        $this->productVariants->add($productVariant);

        return $this;
    }

    public function removeProductVariant(ProductVariant $productVariant)
    {
        $this->productVariants->removeElement($productVariant);

        return $this;
    }


    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return null|string
     */
    public function getPrintfulPictureUrl()
    {
        return $this->printfulPictureUrl;
    }

    /**
     * @param null|string $printfulPictureUrl
     * @return ProductMockup
     */
    public function setPrintfulPictureUrl($printfulPictureUrl)
    {
        $this->printfulPictureUrl = $printfulPictureUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getServerPicturePath()
    {
        return $this->serverPicturePath;
    }

    /**
     * @param null|string $serverPicturePath
     * @return ProductMockup
     */
    public function setServerPicturePath($serverPicturePath)
    {
        $this->serverPicturePath = $serverPicturePath;
        return $this;
    }

    /**
     * @return Product[]|array|Collection
     */
    public function getProductVariants()
    {
        return $this->productVariants;
    }

    /**
     * @param Product[]|array|Collection $productVariants
     * @return ProductMockup
     */
    public function setProductVariants($productVariants)
    {
        $this->productVariants = $productVariants;
        return $this;
    }

    /**
     * @return ProductType|null
     */
    public function getDefaultProductType()
    {
        return $this->defaultProductType;
    }

    /**
     * @param ProductType|null $defaultProductType
     * @return ProductMockup
     */
    public function setDefaultProductType($defaultProductType)
    {
        $this->defaultProductType = $defaultProductType;
        return $this;
    }

    /**
     * @return Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     * @return ProductMockup
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPlacement()
    {
        return $this->placement;
    }

    /**
     * @param null|string $placement
     * @return ProductMockup
     */
    public function setPlacement($placement)
    {
        $this->placement = $placement;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return ProductMockup
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return ProductMockup
     */
    public function setType(int $type): ProductMockup
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPrintfulVariantIds()
    {
        return $this->printfulVariantIds;
    }

    /**
     * @param null|string $printfulVariantIds
     * @return ProductMockup
     */
    public function setPrintfulVariantIds($printfulVariantIds)
    {
        $this->printfulVariantIds = $printfulVariantIds;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsObsolete(): bool
    {
        return $this->isObsolete;
    }

    /**
     * @param bool $isObsolete
     * @return ProductMockup
     */
    public function setIsObsolete(bool $isObsolete): ProductMockup
    {
        $this->isObsolete = $isObsolete;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getJpgFullCreatedAt()
    {
        return $this->jpgFullCreatedAt;
    }

    /**
     * @param \DateTime|null $jpgFullCreatedAt
     * @return ProductMockup
     */
    public function setJpgFullCreatedAt($jpgFullCreatedAt)
    {
        $this->jpgFullCreatedAt = $jpgFullCreatedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getJpg350pxCreatedAt()
    {
        return $this->jpg350pxCreatedAt;
    }

    /**
     * @param \DateTime|null $jpg350pxCreatedAt
     * @return ProductMockup
     */
    public function setJpg350pxCreatedAt($jpg350pxCreatedAt)
    {
        $this->jpg350pxCreatedAt = $jpg350pxCreatedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getWebp350pxCreatedAt()
    {
        return $this->webp350pxCreatedAt;
    }

    /**
     * @param \DateTime|null $webp350pxCreatedAt
     * @return ProductMockup
     */
    public function setWebp350pxCreatedAt($webp350pxCreatedAt)
    {
        $this->webp350pxCreatedAt = $webp350pxCreatedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getJpg350pxFailedAt()
    {
        return $this->jpg350pxFailedAt;
    }

    /**
     * @param \DateTime|null $jpg350pxFailedAt
     * @return ProductMockup
     */
    public function setJpg350pxFailedAt($jpg350pxFailedAt)
    {
        $this->jpg350pxFailedAt = $jpg350pxFailedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getWebp350pxFailedAt()
    {
        return $this->webp350pxFailedAt;
    }

    /**
     * @param \DateTime|null $webp350pxFailedAt
     * @return ProductMockup
     */
    public function setWebp350pxFailedAt($webp350pxFailedAt)
    {
        $this->webp350pxFailedAt = $webp350pxFailedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getThumb50pxCreatedAt()
    {
        return $this->thumb50pxCreatedAt;
    }

    /**
     * @param \DateTime|null $thumb50pxCreatedAt
     * @return ProductMockup
     */
    public function setThumb50pxCreatedAt($thumb50pxCreatedAt)
    {
        $this->thumb50pxCreatedAt = $thumb50pxCreatedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getThumb100pxCreatedAt()
    {
        return $this->thumb100pxCreatedAt;
    }

    /**
     * @param \DateTime|null $thumb100pxCreatedAt
     * @return ProductMockup
     */
    public function setThumb100pxCreatedAt($thumb100pxCreatedAt)
    {
        $this->thumb100pxCreatedAt = $thumb100pxCreatedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getThumb500pxCreatedAt()
    {
        return $this->thumb500pxCreatedAt;
    }

    /**
     * @param \DateTime|null $thumb500pxCreatedAt
     * @return ProductMockup
     */
    public function setThumb500pxCreatedAt($thumb500pxCreatedAt)
    {
        $this->thumb500pxCreatedAt = $thumb500pxCreatedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getThumb50pxFailedAt()
    {
        return $this->thumb50pxFailedAt;
    }

    /**
     * @param \DateTime|null $thumb50pxFailedAt
     * @return ProductMockup
     */
    public function setThumb50pxFailedAt($thumb50pxFailedAt)
    {
        $this->thumb50pxFailedAt = $thumb50pxFailedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getThumb100pxFailedAt()
    {
        return $this->thumb100pxFailedAt;
    }

    /**
     * @param \DateTime|null $thumb100pxFailedAt
     * @return ProductMockup
     */
    public function setThumb100pxFailedAt($thumb100pxFailedAt)
    {
        $this->thumb100pxFailedAt = $thumb100pxFailedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getThumb500pxFailedAt()
    {
        return $this->thumb500pxFailedAt;
    }

    /**
     * @param \DateTime|null $thumb500pxFailedAt
     * @return ProductMockup
     */
    public function setThumb500pxFailedAt($thumb500pxFailedAt)
    {
        $this->thumb500pxFailedAt = $thumb500pxFailedAt;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsUploadedToS3(): bool
    {
        return $this->isUploadedToS3;
    }

    /**
     * @param bool $isUploadedToS3
     * @return ProductMockup
     */
    public function setIsUploadedToS3(bool $isUploadedToS3): ProductMockup
    {
        $this->isUploadedToS3 = $isUploadedToS3;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsThumbsChecked(): bool
    {
        return $this->isThumbsChecked;
    }

    /**
     * @param bool $isThumbsChecked
     * @return ProductMockup
     */
    public function setIsThumbsChecked(bool $isThumbsChecked): ProductMockup
    {
        $this->isThumbsChecked = $isThumbsChecked;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsThumbsRegenSuccess(): bool
    {
        return $this->isThumbsRegenSuccess;
    }

    /**
     * @param bool $isThumbsRegenSuccess
     * @return ProductMockup
     */
    public function setIsThumbsRegenSuccess(bool $isThumbsRegenSuccess): ProductMockup
    {
        $this->isThumbsRegenSuccess = $isThumbsRegenSuccess;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getThumbsRegenInitiatedAt()
    {
        return $this->thumbsRegenInitiatedAt;
    }

    /**
     * @param \DateTime|null $thumbsRegenInitiatedAt
     * @return ProductMockup
     */
    public function setThumbsRegenInitiatedAt($thumbsRegenInitiatedAt)
    {
        $this->thumbsRegenInitiatedAt = $thumbsRegenInitiatedAt;
        return $this;
    }
}