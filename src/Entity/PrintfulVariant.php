<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Finder\Finder;
use App\Service\HumanFriendlyService;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="`printful_variant`")
 * @ORM\Entity(repositoryClass="App\Repository\PrintfulVariantRepository")
 */
class PrintfulVariant extends AbstractEntity
{
    public function __construct()
    {
        $this->productVariants = new ArrayCollection;

        parent::__construct();
    }

    /**
     * @var int|null
     *
     * @ORM\Column(name="printful_variant_id", type="integer", nullable=false)
     */
    protected $printfulVariantId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="printful_product_id", type="integer", nullable=false)
     */
    protected $printfulProductId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="size", type="string", length=255, nullable=true)
     */
    protected $size;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    protected $color;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color_code", type="string", length=255, nullable=true)
     */
    protected $colorCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     */
    protected $imageUrl;

    /**
     * @var float
     *
     * @ORM\Column(name="printful_price", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $printfulPrice = 0.00;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_in_stock", type="boolean", nullable=false)
     */
    protected $isInStock = false;

    /**
     * @var ArrayCollection|ProductVariant[]
     *
     * @ORM\OneToMany(targetEntity="ProductVariant", mappedBy="printfulVariant")
     */
    protected $productVariants;


    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return int|null
     */
    public function getPrintfulVariantId()
    {
        return $this->printfulVariantId;
    }

    /**
     * @param int|null $printfulVariantId
     * @return PrintfulVariant
     */
    public function setPrintfulVariantId($printfulVariantId)
    {
        $this->printfulVariantId = $printfulVariantId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPrintfulProductId()
    {
        return $this->printfulProductId;
    }

    /**
     * @param int|null $printfulProductId
     * @return PrintfulVariant
     */
    public function setPrintfulProductId($printfulProductId)
    {
        $this->printfulProductId = $printfulProductId;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return PrintfulVariant
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param null|string $size
     * @return PrintfulVariant
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param null|string $color
     * @return PrintfulVariant
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getColorCode()
    {
        return $this->colorCode;
    }

    /**
     * @param null|string $colorCode
     * @return PrintfulVariant
     */
    public function setColorCode($colorCode)
    {
        $this->colorCode = $colorCode;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param null|string $imageUrl
     * @return PrintfulVariant
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrintfulPrice(): float
    {
        return $this->printfulPrice;
    }

    /**
     * @param float $printfulPrice
     * @return PrintfulVariant
     */
    public function setPrintfulPrice(float $printfulPrice): PrintfulVariant
    {
        $this->printfulPrice = $printfulPrice;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsInStock(): bool
    {
        return $this->isInStock;
    }

    /**
     * @param bool $isInStock
     * @return PrintfulVariant
     */
    public function setIsInStock(bool $isInStock): PrintfulVariant
    {
        $this->isInStock = $isInStock;
        return $this;
    }

    /**
     * @return ProductVariant[]|ArrayCollection
     */
    public function getProductVariants()
    {
        return $this->productVariants;
    }

    /**
     * @param ProductVariant[]|ArrayCollection $productVariants
     * @return PrintfulVariant
     */
    public function setProductVariants($productVariants)
    {
        $this->productVariants = $productVariants;
        return $this;
    }
}