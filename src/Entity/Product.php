<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @ORM\Table(name="`product`", indexes={
 *           @ORM\Index(name="is_published_idx", columns={"is_published"}),
 *           @ORM\Index(name="is_deleted_idx", columns={"is_deleted"}),
 *           @ORM\Index(name="group_idx", columns={"group"})
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class Product extends AbstractEntity
{
    public function __construct()
    {
        $this->tags = new ArrayCollection;
        $this->productMockups = new ArrayCollection;

        parent::__construct();
    }

    const SOURCE_FREEPIK = 0;
    const SOURCE_EBAY = 1;


    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $title;

    /**
     * The title that came from source (e.g. freepik)
     *
     * @var string|null
     *
     * @ORM\Column(name="title_original", type="string", length=255, nullable=true)
     */
    protected $titleOriginal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description_headline", type="string", length=255, nullable=true)
     */
    protected $descriptionHeadline;

    /**
     * @var string|null
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=true)
     */
    protected $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_published", type="boolean", nullable=false)
     */
    protected $isPublished = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_exceptional", type="boolean", nullable=false)
     */
    protected $isExceptional = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    protected $isDeleted = false;

    /**
     * @var int
     *
     * @ORM\Column(name="rank", type="integer", nullable=false)
     */
    protected $rank = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="currently_viewed_count", type="integer", nullable=false)
     */
    protected $currentlyViewedCount = 0;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="jpg_design_processing_started_at", type="datetime", nullable=true)
     */
    protected $jpgDesignProcessingStartedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="jpg_design_created_at", type="datetime", nullable=true)
     */
    protected $jpgDesignCreatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="mockup_gen_init_started_at", type="datetime", nullable=true)
     */
    protected $mockupGenInitStartedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="mockup_gen_init_finished_at", type="datetime", nullable=true)
     */
    protected $mockupGenInitFinishedAt;

    /**
     * @var Tag[]|array|Collection
     *
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="products")
     * @ORM\JoinTable(name="products_tags")
     *
     * @ORM\OrderBy({"monthlyGoogleSearch" = "DESC"})
     */
    protected $tags;

    /**
     * @var ArrayCollection|ProductMockup[]
     *
     * @ORM\OneToMany(targetEntity="ProductMockup", mappedBy="product")
     */
    protected $productMockups;

    /**
     * @var bool
     *
     * @ORM\Column(name="design_is_single_image", type="boolean", nullable=false)
     */
    protected $designIsSingleImage = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="design_has_text", type="boolean", nullable=false)
     */
    protected $designHasText = false;

    /**
     * @var string|null
     *
     * @ORM\Column(name="author_name", type="string", length=100, nullable=true)
     */
    protected $authorName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="author_url", type="string", length=255, nullable=true)
     */
    protected $authorUrl;

    /**
     * Where did the image come from?
     *
     * @var int
     *
     * @ORM\Column(name="source", type="smallint", nullable=false)
     *
     * @JMS\Expose()
     */
    protected $source = self::SOURCE_FREEPIK;

    /**
     * Unique id at the source where the image came from. Used to prevent duplicate submissions.
     *
     * @var int|null
     *
     * @ORM\Column(name="id_at_source", type="integer", nullable=true)
     *
     * @JMS\Expose()
     */
    protected $idAtSource;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="aws_zip_uploaded_at", type="datetime", nullable=true)
     */
    protected $awsZipUploadedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="aws_unzip_request_made_at", type="datetime", nullable=true)
     */
    protected $awsUnzipRequestMadeAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="aws_unzip_succeeded_at", type="datetime", nullable=true)
     */
    protected $awsUnzipSucceededAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="aws_eps_to_jpg_request_made_at", type="datetime", nullable=true)
     */
    protected $awsEpsToJpgRequestMadeAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="aws_eps_to_jpg_succeeded_at", type="datetime", nullable=true)
     */
    protected $awsEpsToJpgSucceededAt;

    /**
     * FOR product JPG resize, not mockups
     *
     * @var \DateTime|null
     *
     * @ORM\Column(name="aws_jpg_resize_request_made_at", type="datetime", nullable=true)
     */
    protected $awsJpgResizeRequestMadeAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="aws_jpg_resize_succeeded_at", type="datetime", nullable=true)
     */
    protected $awsJpgResizeSucceededAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="quality_control_accepted_at", type="datetime", nullable=true)
     *
     * @JMS\Expose()
     */
    protected $qualityControlAcceptedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="quality_control_rejected_at", type="datetime", nullable=true)
     *
     * @JMS\Expose()
     */
    protected $qualityControlRejectedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_priority_for_processing", type="boolean", nullable=false)
     */
    protected $hasPriorityForProcessing = false;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_mockup_cache_at", type="datetime", nullable=true)
     */
    protected $lastMockupCacheAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="`group`", type="string", length=50, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $group;


    /**
     * CUSTOM METHODS:
     */

    public function hasTag(Tag $tag)
    {
        return $this->tags->contains($tag);
    }

    public function addTag(Tag $tag)
    {
        $tag->addProduct($this); // synchronously updating inverse side
        $this->tags->add($tag);

        return $this;
    }

    public function addManyTags($tags)
    {
        foreach ($tags as $tag) {
            $this->addTag($tag);
        }

        return $this;
    }

    public function removeTag(Tag $tag)
    {
        $tag->removeProduct($this);
        $this->tags->removeElement($tag);

        return $this;
    }

    public function mainImageRelativePath()
    {
        $path = '/uploads/images/processed_design_jpg_files_1k/' . $this->getId() . '.jpg';

        return getenv('APP_ENV') == 'prod' ? $path : getenv('BASE_URL') . $path;
    }

    public function productTypeMainImagePath($productType)
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('defaultProductType', $productType))
            ->andWhere(Criteria::expr()->eq('isObsolete', false))
            ->setMaxResults(1);

        $mockups = $this->getProductMockups()->matching($criteria);

        /** @var ProductMockup $mockup */
        $mockup = $mockups->first();

        if (!$mockup) {
            return '';
        }

        return getenv('PRODUCT_MOCKUPS_FULL_SIZE_BASE_URL') . '/' . $this->getId() . '/' . $mockup->getId() . '/' . $mockup->getId() . '.jpg';
    }

    /**
     * @param ProductType $productType
     * @return ProductMockup[]|array
     */
    public function getProductMockupsByVariants(ProductType $productType)
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('defaultProductType', $productType))
            ->andWhere(Criteria::expr()->eq('isObsolete', false));

        /** @var ProductMockup[] $mockups */
        $mockups = $this->getProductMockups()->matching($criteria);

        $mockupsByVariant = [];
        foreach ($mockups as $mockup) {
            foreach ($mockup->getProductVariants() as $productVariant) {
                $mockupsByVariant[$productVariant->getId()][] = $mockup;
            }
        }

        return $mockupsByVariant;
    }

    /**
     * @return ProductMockup[]|array
     */
    public function getMainProductMockupsByVariants()
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('type', ProductMockup::TYPE_MAIN))
            ->andWhere(Criteria::expr()->eq('isObsolete', false));

        /** @var ProductMockup[] $mockups */
        $mockups = $this->getProductMockups()->matching($criteria);

        $mockupsByVariant = [];
        foreach ($mockups as $mockup) {
            foreach ($mockup->getProductVariants() as $productVariant) {
                if (!isset($mockupsByVariant[$productVariant->getId()])) {
                    $mockupsByVariant[$productVariant->getId()] = $mockup;
                }
            }
        }

        return $mockupsByVariant;
    }

    /**
     * @return null|string
     */
    public function getZipFilePath()
    {
        $fs = new Filesystem();
        $zipRealPath = PUBLIC_DIRECTORY . "/uploads/images/new_design_zip_files/{$this->getId()}.zip";

        return $fs->exists($zipRealPath) ? $zipRealPath : null;
    }

    public function qualityControlAccept($isExceptional = false)
    {
        $this->setQualityControlAcceptedAt(new \DateTime);
        $this->setQualityControlRejectedAt(null);
        $this->setIsExceptional($isExceptional);

        return $this;
    }

    public function qualityControlReject()
    {
        $this->setQualityControlRejectedAt(new \DateTime);
        $this->setQualityControlAcceptedAt(null);
        $this->setIsPublished(false);
        $this->setIsDeleted(true);

        return $this;
    }

    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param null|string $slug
     * @return Product
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool $isPublished
     * @return Product
     */
    public function setIsPublished(bool $isPublished): Product
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @return int
     */
    public function getRank(): int
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     * @return Product
     */
    public function setRank(int $rank): Product
    {
        $this->rank = $rank;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentlyViewedCount(): int
    {
        return $this->currentlyViewedCount;
    }

    /**
     * @param int $currentlyViewedCount
     * @return Product
     */
    public function setCurrentlyViewedCount(int $currentlyViewedCount): Product
    {
        $this->currentlyViewedCount = $currentlyViewedCount;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getJpgDesignCreatedAt()
    {
        return $this->jpgDesignCreatedAt;
    }

    /**
     * @param \DateTime|null $jpgDesignCreatedAt
     * @return Product
     */
    public function setJpgDesignCreatedAt($jpgDesignCreatedAt)
    {
        $this->jpgDesignCreatedAt = $jpgDesignCreatedAt;
        return $this;
    }

    /**
     * @param bool $asString
     * @return Tag[]|array|Collection|string
     */
    public function getTags($asString = false)
    {
        if (true === $asString) {
            return implode(', ', $this->tags->toArray());
        }
        return $this->tags;
    }

    /**
     * @param Tag[]|array|Collection $tags
     * @return Product
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return ProductMockup[]|ArrayCollection
     */
    public function getProductMockups()
    {
        return $this->productMockups;
    }

    /**
     * @param ProductMockup[]|ArrayCollection $productMockups
     * @return Product
     */
    public function setProductMockups($productMockups)
    {
        $this->productMockups = $productMockups;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getJpgDesignProcessingStartedAt()
    {
        return $this->jpgDesignProcessingStartedAt;
    }

    /**
     * @param \DateTime|null $jpgDesignProcessingStartedAt
     * @return Product
     */
    public function setJpgDesignProcessingStartedAt($jpgDesignProcessingStartedAt)
    {
        $this->jpgDesignProcessingStartedAt = $jpgDesignProcessingStartedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getMockupGenInitStartedAt()
    {
        return $this->mockupGenInitStartedAt;
    }

    /**
     * @param \DateTime|null $mockupGenInitStartedAt
     * @return Product
     */
    public function setMockupGenInitStartedAt($mockupGenInitStartedAt)
    {
        $this->mockupGenInitStartedAt = $mockupGenInitStartedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getMockupGenInitFinishedAt()
    {
        return $this->mockupGenInitFinishedAt;
    }

    /**
     * @param \DateTime|null $mockupGenInitFinishedAt
     * @return Product
     */
    public function setMockupGenInitFinishedAt($mockupGenInitFinishedAt)
    {
        $this->mockupGenInitFinishedAt = $mockupGenInitFinishedAt;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDesignIsSingleImage(): bool
    {
        return $this->designIsSingleImage;
    }

    /**
     * @param bool $designIsSingleImage
     * @return Product
     */
    public function setDesignIsSingleImage(bool $designIsSingleImage): Product
    {
        $this->designIsSingleImage = $designIsSingleImage;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDesignHasText(): bool
    {
        return $this->designHasText;
    }

    /**
     * @param bool $designHasText
     * @return Product
     */
    public function setDesignHasText(bool $designHasText): Product
    {
        $this->designHasText = $designHasText;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * @param null|string $authorName
     * @return Product
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAuthorUrl()
    {
        return $this->authorUrl;
    }

    /**
     * @param null|string $authorUrl
     * @return Product
     */
    public function setAuthorUrl($authorUrl)
    {
        $this->authorUrl = $authorUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescriptionHeadline()
    {
        return $this->descriptionHeadline;
    }

    /**
     * @param null|string $descriptionHeadline
     * @return Product
     */
    public function setDescriptionHeadline($descriptionHeadline)
    {
        $this->descriptionHeadline = $descriptionHeadline;
        return $this;
    }

    /**
     * @return int
     */
    public function getSource(): int
    {
        return $this->source;
    }

    /**
     * @param int $source
     * @return Product
     */
    public function setSource(int $source): Product
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getIdAtSource()
    {
        return $this->idAtSource;
    }

    /**
     * @param int|null $idAtSource
     * @return Product
     */
    public function setIdAtSource($idAtSource)
    {
        $this->idAtSource = $idAtSource;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     * @return Product
     */
    public function setIsDeleted(bool $isDeleted): Product
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitleOriginal()
    {
        return $this->titleOriginal;
    }

    /**
     * @param null|string $titleOriginal
     * @return Product
     */
    public function setTitleOriginal($titleOriginal)
    {
        $this->titleOriginal = $titleOriginal;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getAwsZipUploadedAt()
    {
        return $this->awsZipUploadedAt;
    }

    /**
     * @param \DateTime|null $awsZipUploadedAt
     * @return Product
     */
    public function setAwsZipUploadedAt($awsZipUploadedAt)
    {
        $this->awsZipUploadedAt = $awsZipUploadedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getAwsUnzipRequestMadeAt()
    {
        return $this->awsUnzipRequestMadeAt;
    }

    /**
     * @param \DateTime|null $awsUnzipRequestMadeAt
     * @return Product
     */
    public function setAwsUnzipRequestMadeAt($awsUnzipRequestMadeAt)
    {
        $this->awsUnzipRequestMadeAt = $awsUnzipRequestMadeAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getAwsUnzipSucceededAt()
    {
        return $this->awsUnzipSucceededAt;
    }

    /**
     * @param \DateTime|null $awsUnzipSucceededAt
     * @return Product
     */
    public function setAwsUnzipSucceededAt($awsUnzipSucceededAt)
    {
        $this->awsUnzipSucceededAt = $awsUnzipSucceededAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getAwsEpsToJpgRequestMadeAt()
    {
        return $this->awsEpsToJpgRequestMadeAt;
    }

    /**
     * @param \DateTime|null $awsEpsToJpgRequestMadeAt
     * @return Product
     */
    public function setAwsEpsToJpgRequestMadeAt($awsEpsToJpgRequestMadeAt)
    {
        $this->awsEpsToJpgRequestMadeAt = $awsEpsToJpgRequestMadeAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getAwsEpsToJpgSucceededAt()
    {
        return $this->awsEpsToJpgSucceededAt;
    }

    /**
     * @param \DateTime|null $awsEpsToJpgSucceededAt
     * @return Product
     */
    public function setAwsEpsToJpgSucceededAt($awsEpsToJpgSucceededAt)
    {
        $this->awsEpsToJpgSucceededAt = $awsEpsToJpgSucceededAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getAwsJpgResizeRequestMadeAt()
    {
        return $this->awsJpgResizeRequestMadeAt;
    }

    /**
     * @param \DateTime|null $awsJpgResizeRequestMadeAt
     * @return Product
     */
    public function setAwsJpgResizeRequestMadeAt($awsJpgResizeRequestMadeAt)
    {
        $this->awsJpgResizeRequestMadeAt = $awsJpgResizeRequestMadeAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getAwsJpgResizeSucceededAt()
    {
        return $this->awsJpgResizeSucceededAt;
    }

    /**
     * @param \DateTime|null $awsJpgResizeSucceededAt
     * @return Product
     */
    public function setAwsJpgResizeSucceededAt($awsJpgResizeSucceededAt)
    {
        $this->awsJpgResizeSucceededAt = $awsJpgResizeSucceededAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getQualityControlAcceptedAt()
    {
        return $this->qualityControlAcceptedAt;
    }

    /**
     * @param \DateTime|null $qualityControlAcceptedAt
     * @return Product
     */
    public function setQualityControlAcceptedAt($qualityControlAcceptedAt)
    {
        $this->qualityControlAcceptedAt = $qualityControlAcceptedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getQualityControlRejectedAt()
    {
        return $this->qualityControlRejectedAt;
    }

    /**
     * @param \DateTime|null $qualityControlRejectedAt
     * @return Product
     */
    public function setQualityControlRejectedAt($qualityControlRejectedAt)
    {
        $this->qualityControlRejectedAt = $qualityControlRejectedAt;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsExceptional(): bool
    {
        return $this->isExceptional;
    }

    /**
     * @param bool $isExceptional
     * @return Product
     */
    public function setIsExceptional(bool $isExceptional): Product
    {
        $this->isExceptional = $isExceptional;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasPriorityForProcessing(): bool
    {
        return $this->hasPriorityForProcessing;
    }

    /**
     * @param bool $hasPriorityForProcessing
     * @return Product
     */
    public function setHasPriorityForProcessing(bool $hasPriorityForProcessing): Product
    {
        $this->hasPriorityForProcessing = $hasPriorityForProcessing;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastMockupCacheAt()
    {
        return $this->lastMockupCacheAt;
    }

    /**
     * @param \DateTime|null $lastMockupCacheAt
     * @return Product
     */
    public function setLastMockupCacheAt($lastMockupCacheAt)
    {
        $this->lastMockupCacheAt = $lastMockupCacheAt;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param null|string $group
     * @return Product
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }
}