<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="`tag`", indexes={
 *           @ORM\Index(name="title_idx", columns={"title"}),
 *           @ORM\Index(name="monthly_google_search_idx", columns={"monthly_google_search"})
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class Tag extends AbstractEntity
{
    public function __construct($title = null)
    {
        $this->products = new ArrayCollection();

        if ($title) {
            $this->title = $title;
        }

        parent::__construct();
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $title;

    /**
     * @var string|null
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=true)
     */
    protected $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_published", type="boolean", nullable=false)
     */
    protected $isPublished = false;

    /**
     * @var int
     *
     * @ORM\Column(name="monthly_google_search", type="integer", nullable=false)
     */
    protected $monthlyGoogleSearch = 0;

    /**
     * @var Product[]|array|Collection
     *
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="tags")
     */
    protected $products;


    /**
     * CUSTOM METHODS:
     */

    public function addProduct(Product $product)
    {
        $this->products->add($product);

        return $this;
    }

    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);

        return $this;
    }

    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return Tag
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param null|string $slug
     * @return Tag
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool $isPublished
     * @return Tag
     */
    public function setIsPublished(bool $isPublished): Tag
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @return int
     */
    public function getMonthlyGoogleSearch(): int
    {
        return $this->monthlyGoogleSearch;
    }

    /**
     * @param int $monthlyGoogleSearch
     * @return Tag
     */
    public function setMonthlyGoogleSearch(int $monthlyGoogleSearch): Tag
    {
        $this->monthlyGoogleSearch = $monthlyGoogleSearch;
        return $this;
    }
}