<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="`external_api_call`")
 * @ORM\Entity(repositoryClass="App\Repository\ExternalApiCallRepository")
 */
class ExternalApiCall extends AbstractEntity
{
    const OUTCOME_SUCCESSFUL = 200;
    const OUTCOME_FAILED = 500;

    const PURPOSE_MOCKUP_GEN_INIT = 'mockup_gen_init';
    const PURPOSE_MOCKUP_GEN_REINIT = 'mockup_gen_reinit';
    const PURPOSE_MOCKUP_GEN_RETRIEVAL = 'mockup_gen_retrieval';
    const PURPOSE_SHIPPING_PRICE_CALCULATE = 'shipping_price_calculate';

    /**
     * @var string|null
     *
     * printful, etc.
     *
     * @ORM\Column(name="api_name", type="string", length=50, nullable=true)
     */
    protected $apiName;

    /**
     * @var string|null
     *
     * mockup_gen_init, mockup_gen_reinit, mockup_gen_retrieval, etc.
     *
     * @ORM\Column(name="purpose", type="string", length=50, nullable=true)
     */
    protected $purpose;

    /**
     * @var int
     *
     * @ORM\Column(name="outcome", type="smallint", nullable=false)
     */
    protected $outcome = self::OUTCOME_SUCCESSFUL;

    /**
     * @var string|null
     *
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    protected $result;

    /**
     * @var string|null
     *
     * @ORM\Column(name="error", type="text", nullable=true)
     */
    protected $error;

    /**
     * @var Product|null If it's related to a product, mark it here, leave null otherwise
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @var ProductType|null If it's related to a product type, mark it here, leave null oterwise
     *
     * @ORM\ManyToOne(targetEntity="ProductType")
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id")
     */
    protected $productType;


    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return null|string
     */
    public function getApiName()
    {
        return $this->apiName;
    }

    /**
     * @param null|string $apiName
     * @return ExternalApiCall
     */
    public function setApiName($apiName)
    {
        $this->apiName = $apiName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @param null|string $purpose
     * @return ExternalApiCall
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;
        return $this;
    }

    /**
     * @return int
     */
    public function getOutcome(): int
    {
        return $this->outcome;
    }

    /**
     * @param int $outcome
     * @return ExternalApiCall
     */
    public function setOutcome(int $outcome): ExternalApiCall
    {
        $this->outcome = $outcome;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param null|string $error
     * @return ExternalApiCall
     */
    public function setError($error)
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     * @return ExternalApiCall
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return ProductType|null
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param ProductType|null $productType
     * @return ExternalApiCall
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param null|string $result
     * @return ExternalApiCall
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }
}