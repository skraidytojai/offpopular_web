<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="`currency_rate`")
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRateRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class CurrencyRate extends AbstractEntity
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="currency_from", type="string", length=3, nullable=true)
     */
    protected $currencyFrom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="currency_to", type="string", length=3, nullable=true)
     */
    protected $currencyTo;

    /**
     * @var float
     *
     * @ORM\Column(name="rate", type="decimal", precision=10, scale=4, nullable=false)
     */
    protected $rate = 0.0000;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_captured", type="datetime", nullable=false)
     */
    protected $dateCaptured;


    /**
     * GETTERS AND SETTERS:
     */


    /**
     * @return null|string
     */
    public function getCurrencyFrom()
    {
        return $this->currencyFrom;
    }

    /**
     * @param null|string $currencyFrom
     * @return CurrencyRate
     */
    public function setCurrencyFrom($currencyFrom)
    {
        $this->currencyFrom = $currencyFrom;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCurrencyTo()
    {
        return $this->currencyTo;
    }

    /**
     * @param null|string $currencyTo
     * @return CurrencyRate
     */
    public function setCurrencyTo($currencyTo)
    {
        $this->currencyTo = $currencyTo;
        return $this;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     * @return CurrencyRate
     */
    public function setRate(float $rate): CurrencyRate
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCaptured(): \DateTime
    {
        return $this->dateCaptured;
    }

    /**
     * @param \DateTime $dateCaptured
     * @return CurrencyRate
     */
    public function setDateCaptured(\DateTime $dateCaptured): CurrencyRate
    {
        $this->dateCaptured = $dateCaptured;
        return $this;
    }
}