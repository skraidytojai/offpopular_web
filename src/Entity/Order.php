<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle as FOS;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="`order`")
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class Order extends AbstractEntity
{

    const STATUS_PLACED_UNPAID = 0;
    const STATUS_PAID = 5;
    const STATUS_PROCESSING = 10;
    const STATUS_SHIPPED = 100;
    const STATUS_SHIPMENT_RECEIVED = 200;
    const STATUS_PICKED_UP = 201;

    const SHIPPING_TYPE_PICKUP = 'pickup';
    const SHIPPING_TYPE_COURIER = 'courier';

    public function __construct()
    {
        parent::__construct();

        $this->orderItems = new ArrayCollection();

        $this->setBraintreeEnvironment(getenv('BRAINTREE_ENVIRONMENT'));
    }

    /**
     * Who ordered?
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shipping_name", type="string", length=255, nullable=true)
     */
    protected $shippingName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shipping_address", type="string", length=255, nullable=true)
     */
    protected $shippingAddress;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shipping_city", type="string", length=255, nullable=true)
     */
    protected $shippingCity;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shipping_post_code", type="string", length=255, nullable=true)
     */
    protected $shippingPostCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shipping_country", type="string", length=255, nullable=true)
     */
    protected $shippingCountry;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shipping_state", type="string", length=255, nullable=true)
     */
    protected $shippingState;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shipping_country_two_letter", type="string", length=2, nullable=true)
     */
    protected $shippingCountryTwoLetter;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shipping_type", type="string", length=255, nullable=true)
     */
    protected $shippingType;

    /**
     * @var ArrayCollection|OrderItem[]
     *
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="order")
     */
    protected $orderItems;

    /**
     * @var float
     *
     * @ORM\Column(name="sub_total_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $subTotalAmount = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="shipping_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $shippingAmount = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $totalAmount = 0.00;

    /**
     * 3 letter currency code
     * @var string|null
     *
     * @ORM\Column(name="currency", type="string", length=3, nullable=true)
     */
    protected $currency;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    protected $status = self::STATUS_PLACED_UNPAID;

    /**
     * @var string|null
     *
     * @ORM\Column(name="buyer_first_name", type="string", length=255, nullable=true)
     */
    protected $buyerFirstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="buyer_last_name", type="string", length=255, nullable=true)
     */
    protected $buyerLastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="buyer_email", type="string", length=255, nullable=true)
     */
    protected $buyerEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="buyer_phone", type="string", length=255, nullable=true)
     */
    protected $buyerPhone;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_test", type="boolean", nullable=false)
     */
    protected $isTest = false;

    /**
     * @var string|null
     *
     * @ORM\Column(name="braintree_environment", type="string", length=255, nullable=true)
     */
    protected $braintreeEnvironment;

    /**
     * @var float
     *
     * In our settlement/home currency
     *
     * @ORM\Column(name="final_charge_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $finalChargeAmount = 0.00;

    /**
     * 3 letter currency code
     * @var string|null
     *
     * @ORM\Column(name="final_charge_currency", type="string", length=3, nullable=true)
     */
    protected $finalChargeCurrency;


    /**
     * CUSTOM METHODS:
     */

    public function updateFinalChargeInformation(string $finalChargeCurrency, float $finalChargeExchangeRate)
    {
        $finalChargeAmount = $finalChargeExchangeRate * $this->getTotalAmount();

        $this->setFinalChargeAmount($finalChargeAmount);
        $this->setFinalChargeCurrency($finalChargeCurrency);

        return $this;
    }

    public function calculateTotalAmount()
    {
        $totalAmount = $this->getSubTotalAmount() + $this->getShippingAmount();
        $this->setTotalAmount($totalAmount);

        return $this;
    }

    public function calculateSubTotalAmount()
    {
        $subTotal = 0;
        foreach ($this->getOrderItems() as $orderItem) {
            $subTotal += $orderItem->getPrice();
        }

        $this->setSubTotalAmount($subTotal);

        return $this;
    }

    public function confirmOrder()
    {
        $this->setStatus(self::STATUS_PAID);

        return $this;
    }

    /**
     * @return OrderItem[]
     */
    /* public function getGroupedOrderItemsByQuantity()
    {
        $orderItems = [];
        foreach ($this->getOrderItems() as $orderItem) {
            $orderItems[$orderItem->getPicture()->getId() . '-' . $orderItem->getProduct()->getId()][] = $orderItem;
        }

        return $orderItems;
    } */

    /**
     * @param OrderItem $orderItem
     * @return Order
     */
    public function addOrderItem(OrderItem $orderItem)
    {
        $this->orderItems->add($orderItem);
        $orderItem->setOrder($this);

        return $this;
    }

    public function getBuyerFullName()
    {
        return $this->getBuyerFirstName() . ' ' . $this->getBuyerLastName();
    }


    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return null|User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param null|User $user
     * @return Order
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getShippingName()
    {
        return $this->shippingName;
    }

    /**
     * @param null|string $shippingName
     * @return Order
     */
    public function setShippingName($shippingName)
    {
        $this->shippingName = $shippingName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * @param null|string $shippingAddress
     * @return Order
     */
    public function setShippingAddress($shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getShippingCity()
    {
        return $this->shippingCity;
    }

    /**
     * @param null|string $shippingCity
     * @return Order
     */
    public function setShippingCity($shippingCity)
    {
        $this->shippingCity = $shippingCity;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getShippingPostCode()
    {
        return $this->shippingPostCode;
    }

    /**
     * @param null|string $shippingPostCode
     * @return Order
     */
    public function setShippingPostCode($shippingPostCode)
    {
        $this->shippingPostCode = $shippingPostCode;
        return $this;
    }

    /**
     * @return ArrayCollection|OrderItem[]
     */
    public function getOrderItems()
    {
        return $this->orderItems;
    }

    /**
     * @param ArrayCollection|OrderItem[] $orderItems
     * @return Order
     */
    public function setOrderItems($orderItems)
    {
        $this->orderItems = $orderItems;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     * @return Order
     */
    public function setTotalAmount(float $totalAmount): Order
    {
        $this->totalAmount = $totalAmount;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param null|string $currency
     * @return Order
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Order
     */
    public function setStatus(int $status): Order
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getShippingCountry()
    {
        return $this->shippingCountry;
    }

    /**
     * @param null|string $shippingCountry
     * @return Order
     */
    public function setShippingCountry($shippingCountry)
    {
        $this->shippingCountry = $shippingCountry;
        return $this;
    }

    /**
     * @return float
     */
    public function getSubTotalAmount(): float
    {
        return $this->subTotalAmount;
    }

    /**
     * @param float $subTotalAmount
     * @return Order
     */
    public function setSubTotalAmount(float $subTotalAmount): Order
    {
        $this->subTotalAmount = $subTotalAmount;
        return $this;
    }

    /**
     * @return float
     */
    public function getShippingAmount(): float
    {
        return $this->shippingAmount;
    }

    /**
     * @param float $shippingAmount
     * @return Order
     */
    public function setShippingAmount(float $shippingAmount): Order
    {
        $this->shippingAmount = $shippingAmount;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getShippingType()
    {
        return $this->shippingType;
    }

    /**
     * @param null|string $shippingType
     * @return Order
     */
    public function setShippingType($shippingType)
    {
        $this->shippingType = $shippingType;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getBuyerEmail()
    {
        return $this->buyerEmail;
    }

    /**
     * @param null|string $buyerEmail
     * @return Order
     */
    public function setBuyerEmail($buyerEmail)
    {
        $this->buyerEmail = $buyerEmail;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getBuyerPhone()
    {
        return $this->buyerPhone;
    }

    /**
     * @param null|string $buyerPhone
     * @return Order
     */
    public function setBuyerPhone($buyerPhone)
    {
        $this->buyerPhone = $buyerPhone;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsTest(): bool
    {
        return $this->isTest;
    }

    /**
     * @param bool $isTest
     * @return Order
     */
    public function setIsTest(bool $isTest): Order
    {
        $this->isTest = $isTest;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getBuyerFirstName()
    {
        return $this->buyerFirstName;
    }

    /**
     * @param null|string $buyerFirstName
     * @return Order
     */
    public function setBuyerFirstName($buyerFirstName)
    {
        $this->buyerFirstName = $buyerFirstName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getBuyerLastName()
    {
        return $this->buyerLastName;
    }

    /**
     * @param null|string $buyerLastName
     * @return Order
     */
    public function setBuyerLastName($buyerLastName)
    {
        $this->buyerLastName = $buyerLastName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getShippingCountryTwoLetter()
    {
        return $this->shippingCountryTwoLetter;
    }

    /**
     * @param null|string $shippingCountryTwoLetter
     * @return Order
     */
    public function setShippingCountryTwoLetter($shippingCountryTwoLetter)
    {
        $this->shippingCountryTwoLetter = $shippingCountryTwoLetter;
        return $this;
    }

    /**
     * @return float
     */
    public function getFinalChargeAmount(): float
    {
        return $this->finalChargeAmount;
    }

    /**
     * @param float $finalChargeAmount
     * @return Order
     */
    public function setFinalChargeAmount(float $finalChargeAmount): Order
    {
        $this->finalChargeAmount = $finalChargeAmount;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFinalChargeCurrency()
    {
        return $this->finalChargeCurrency;
    }

    /**
     * @param null|string $finalChargeCurrency
     * @return Order
     */
    public function setFinalChargeCurrency($finalChargeCurrency)
    {
        $this->finalChargeCurrency = $finalChargeCurrency;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getBraintreeEnvironment()
    {
        return $this->braintreeEnvironment;
    }

    /**
     * @param null|string $braintreeEnvironment
     * @return Order
     */
    public function setBraintreeEnvironment($braintreeEnvironment)
    {
        $this->braintreeEnvironment = $braintreeEnvironment;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getShippingState()
    {
        return $this->shippingState;
    }

    /**
     * @param null|string $shippingState
     * @return Order
     */
    public function setShippingState($shippingState)
    {
        $this->shippingState = $shippingState;
        return $this;
    }
}