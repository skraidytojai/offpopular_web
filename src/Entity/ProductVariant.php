<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Table(name="`product_variant`", indexes={
 *           @ORM\Index(name="rank_idx", columns={"rank"})
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\ProductVariantRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class ProductVariant extends AbstractEntity
{
    public function __construct()
    {
        $this->productMockups = new ArrayCollection;

        parent::__construct();
    }

    /**
     * @var PrintfulVariant|null
     *
     * @ORM\ManyToOne(targetEntity="PrintfulVariant", inversedBy="productVariants")
     * @ORM\JoinColumn(name="printful_variant_id", referencedColumnName="id")
     */
    protected $printfulVariant;

    /**
     * @var ProductType|null
     *
     * @ORM\ManyToOne(targetEntity="ProductType", inversedBy="productVariants")
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id")
     *
     * @JMS\Expose()
     */
    protected $productType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="size", type="string", length=255, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $size;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    protected $color;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color_code", type="string", length=255, nullable=true)
     */
    protected $colorCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     */
    protected $imageUrl;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false)
     *
     * @JMS\Expose()
     */
    protected $price = 0.00;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_in_stock", type="boolean", nullable=false)
     *
     * @JMS\Expose()
     */
    protected $isInStock = false;

    /**
     * @var ProductMockup[]|array|Collection
     *
     * @ORM\ManyToMany(targetEntity="ProductMockup", inversedBy="productVariants")
     * @ORM\JoinTable(name="product_variants_product_mockups")
     */
    protected $productMockups;

    /**
     * @var int
     *
     * @ORM\Column(name="rank", type="smallint", nullable=false)
     */
    protected $rank = 0;

    /**
     * US SYSTEM (in)
     *
     * @var string|null
     *
     * @ORM\Column(name="sizing_explanation_us", type="string", length=255, nullable=true)
     */
    protected $sizingExplanationUs;

    /**
     * METRIC SYSTEM (cm)
     *
     * @var string|null
     *
     * @ORM\Column(name="sizing_explanation_metric", type="string", length=255, nullable=true)
     */
    protected $sizingExplanationMetric;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_free_shipping", type="boolean", nullable=false)
     *
     * @JMS\Expose()
     */
    protected $isFreeShipping = false;


    /**
     * CUSTOM METHODS:
     */

    public function addProductMockup(ProductMockup $productMockup)
    {
        $productMockup->addProductVariant($this); // synchronously updating inverse side
        $this->productMockups->add($productMockup);

        return $this;
    }

    public function removeProductMockup(ProductMockup $productMockup)
    {
        $productMockup->removeProductVariant($this);
        $this->productMockups->removeElement($productMockup);

        return $this;
    }

    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return PrintfulVariant|null
     */
    public function getPrintfulVariant()
    {
        return $this->printfulVariant;
    }

    /**
     * @param PrintfulVariant|null $printfulVariant
     * @return ProductVariant
     */
    public function setPrintfulVariant($printfulVariant)
    {
        $this->printfulVariant = $printfulVariant;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return ProductVariant
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param null|string $size
     * @return ProductVariant
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param null|string $color
     * @return ProductVariant
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getColorCode()
    {
        return $this->colorCode;
    }

    /**
     * @param null|string $colorCode
     * @return ProductVariant
     */
    public function setColorCode($colorCode)
    {
        $this->colorCode = $colorCode;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param null|string $imageUrl
     * @return ProductVariant
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return ProductVariant
     */
    public function setPrice(float $price): ProductVariant
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsInStock(): bool
    {
        return $this->isInStock;
    }

    /**
     * @param bool $isInStock
     * @return ProductVariant
     */
    public function setIsInStock(bool $isInStock): ProductVariant
    {
        $this->isInStock = $isInStock;
        return $this;
    }

    /**
     * @return ProductMockup[]|array|Collection|ArrayCollection
     */
    public function getProductMockups()
    {
        return $this->productMockups;
    }

    /**
     * @param ProductMockup[]|array|Collection $productMockups
     * @return ProductVariant
     */
    public function setProductMockups($productMockups)
    {
        $this->productMockups = $productMockups;
        return $this;
    }

    /**
     * @return int
     */
    public function getRank(): int
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     * @return ProductVariant
     */
    public function setRank(int $rank): ProductVariant
    {
        $this->rank = $rank;
        return $this;
    }

    /**
     * @return ProductType|null
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param ProductType|null $productType
     * @return ProductVariant
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSizingExplanationUs()
    {
        return $this->sizingExplanationUs;
    }

    /**
     * @param null|string $sizingExplanationUs
     * @return ProductVariant
     */
    public function setSizingExplanationUs($sizingExplanationUs)
    {
        $this->sizingExplanationUs = $sizingExplanationUs;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSizingExplanationMetric()
    {
        return $this->sizingExplanationMetric;
    }

    /**
     * @param null|string $sizingExplanationMetric
     * @return ProductVariant
     */
    public function setSizingExplanationMetric($sizingExplanationMetric)
    {
        $this->sizingExplanationMetric = $sizingExplanationMetric;
        return $this;
    }
}