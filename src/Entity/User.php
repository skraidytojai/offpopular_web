<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle as FOS;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * User
 *
 * @ORM\Table(name="`user`", indexes={
 *           @ORM\Index(name="is_newsletter_subscriber_idx", columns={"is_newsletter_subscriber"})
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class User extends FOS\Model\User
{
    const ROLE_USER = 'ROLE_USER'; // Just a regular user.
    const ROLE_ADMIN = 'ROLE_ADMIN'; // Somebody who is able to access Admin panel and has complete control of the project
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    const SOURCE_PILLOW_CLUB = 'PILLOW_CLUB';
    const SOURCE_PILLOW_CLUB_FRIEND_INVITATION = 'PILLOW_CLUB_FRIEND_INVITATION';
    const SOURCE_PILLOW_CLUB_SOCIAL = 'PILLOW_CLUB_SOCIAL';

    public function __construct()
    {
        parent::__construct();

        $this->enabled = true;
    }

    public function __toString()
    {
        return $this->getFirstName() . ' (' . $this->getEmail() . ') ';
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @JMS\Expose
     */
    protected $email;

    /**
     * First name of the user
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="string", nullable=true, length=255)
     */
    protected $firstName;

    /**
     * Last name of the user
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="string", nullable=true, length=255)
     */
    protected $lastName;

    /**
     * Phone number
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", nullable=true, length=255)
     */
    protected $phone;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="source", type="string", nullable=true, length=255)
     */
    protected $source;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_newsletter_subscriber", type="boolean", nullable=false)
     */
    protected $isNewsletterSubscriber = false;


    /**
     * CUSTOM METHODS:
     */


    /** OVERRIDEN METHODS */

    public function setEmail($email)
    {
        $this->email = $email;
        $this->username = $email;

        return $this;
    }


    /**
     * GETTERS AND SETTERS:
     */


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param null|string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return null|string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param null|string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return null|string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsNewsletterSubscriber(): bool
    {
        return $this->isNewsletterSubscriber;
    }

    /**
     * @param bool $isNewsletterSubscriber
     * @return User
     */
    public function setIsNewsletterSubscriber(bool $isNewsletterSubscriber): User
    {
        $this->isNewsletterSubscriber = $isNewsletterSubscriber;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param null|string $source
     * @return User
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }
}

