<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="`product_type_category`")
 * @ORM\Entity(repositoryClass="App\Repository\ProductTypeCategoryRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class ProductTypeCategory extends AbstractEntity
{
    public function __construct()
    {
        $this->productTypes = new ArrayCollection;
        $this->childCategories = new ArrayCollection;

        parent::__construct();
    }

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title_for_search", type="string", length=50, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $titleForSearch;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sysname", type="string", length=50, nullable=true)
     */
    protected $sysname;

    /**
     * @var string|null
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="slug", type="string", unique=true, length=255, nullable=true)
     *
     * @JMS\Expose()
     */
    protected $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_top_level", type="boolean", nullable=false)
     *
     * @JMS\Expose()
     */
    protected $isTopLevel = false;

    /**
     * @var ArrayCollection|ProductType[]
     *
     * @ORM\OneToMany(targetEntity="ProductType", mappedBy="productTypeCategory", fetch="EAGER")
     * @ORM\OrderBy({"rank" = "ASC"})
     */
    protected $productTypes;

    /**
     * @var ArrayCollection|ProductTypeCategory[]
     *
     * @ORM\OneToMany(targetEntity="ProductTypeCategory", mappedBy="parentProductTypeCategory", fetch="EAGER")
     * @ORM\OrderBy({"rank" = "ASC"})
     */
    protected $childCategories;

    /**
     * @var ProductTypeCategory|null
     *
     * @ORM\ManyToOne(targetEntity="ProductTypeCategory", inversedBy="childCategories")
     * @ORM\JoinColumn(name="parent_product_type_category_id", referencedColumnName="id")
     *
     * @JMS\Expose()
     */
    protected $parentProductTypeCategory;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_published", type="boolean", nullable=false)
     */
    protected $isPublished = false;

    /**
     * @var int
     *
     * @ORM\Column(name="rank", type="integer", nullable=false)
     */
    protected $rank = 0;

    /**
     * separated by comma (,)
     *
     * @var string|null
     *
     * @ORM\Column(name="title_for_search_alternatives", type="text", nullable=true)
     */
    protected $titleForSearchAlternatives;


    public function __toString()
    {
        return $this->getId() . ': ' . $this->getTitle();
    }


    /**
     * CUSTOM METHODS:
     */

    /**
     * @return ProductTypeCategory[]|ArrayCollection|Collection
     */
    public function getPublishedChildCategories()
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('isPublished', true))
            ->orderBy(['rank' => 'ASC']);

        return $this->getChildCategories()->matching($criteria);
    }

    /**
     * GETTERS AND SETTERS:
     */


    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return ProductTypeCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSysname()
    {
        return $this->sysname;
    }

    /**
     * @param null|string $sysname
     * @return ProductTypeCategory
     */
    public function setSysname($sysname)
    {
        $this->sysname = $sysname;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param null|string $slug
     * @return ProductTypeCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsTopLevel(): bool
    {
        return $this->isTopLevel;
    }

    /**
     * @param bool $isTopLevel
     * @return ProductTypeCategory
     */
    public function setIsTopLevel(bool $isTopLevel): ProductTypeCategory
    {
        $this->isTopLevel = $isTopLevel;
        return $this;
    }

    /**
     * @return ProductType[]|ArrayCollection
     */
    public function getProductTypes()
    {
        return $this->productTypes;
    }

    /**
     * @param ProductType[]|ArrayCollection $productTypes
     * @return ProductTypeCategory
     */
    public function setProductTypes($productTypes)
    {
        $this->productTypes = $productTypes;
        return $this;
    }

    /**
     * @return ProductTypeCategory[]|ArrayCollection
     */
    public function getChildCategories()
    {
        return $this->childCategories;
    }

    /**
     * @param ProductTypeCategory[]|ArrayCollection $childCategories
     * @return ProductTypeCategory
     */
    public function setChildCategories($childCategories)
    {
        $this->childCategories = $childCategories;
        return $this;
    }

    /**
     * @return ProductTypeCategory|null
     */
    public function getParentProductTypeCategory()
    {
        return $this->parentProductTypeCategory;
    }

    /**
     * @param ProductTypeCategory|null $parentProductTypeCategory
     * @return ProductTypeCategory
     */
    public function setParentProductTypeCategory($parentProductTypeCategory)
    {
        $this->parentProductTypeCategory = $parentProductTypeCategory;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool $isPublished
     * @return ProductTypeCategory
     */
    public function setIsPublished(bool $isPublished): ProductTypeCategory
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @return int
     */
    public function getRank(): int
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     * @return ProductTypeCategory
     */
    public function setRank(int $rank): ProductTypeCategory
    {
        $this->rank = $rank;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitleForSearch()
    {
        return $this->titleForSearch;
    }

    /**
     * @param null|string $titleForSearch
     * @return ProductTypeCategory
     */
    public function setTitleForSearch($titleForSearch)
    {
        $this->titleForSearch = $titleForSearch;
        return $this;
    }

    /**
     * @param bool $asArray
     * @return array|null|string
     */
    public function getTitleForSearchAlternatives($asArray = false)
    {
        if ($asArray) {
            return explode(',', $this->titleForSearchAlternatives);
        }

        return $this->titleForSearchAlternatives;
    }

    /**
     * @param null|string $titleForSearchAlternatives
     * @return ProductTypeCategory
     */
    public function setTitleForSearchAlternatives($titleForSearchAlternatives)
    {
        $this->titleForSearchAlternatives = $titleForSearchAlternatives;
        return $this;
    }
}