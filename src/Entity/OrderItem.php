<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle as FOS;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use App\Service\HumanFriendlyService;

/**
 * @ORM\Table(name="`order_item`")
 * @ORM\Entity(repositoryClass="App\Repository\OrderItemRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class OrderItem extends AbstractEntity
{
    /**
     * @var Order|null
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderItems")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    protected $order;

    /**
     * @var Product|null
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @var ProductType|null
     *
     * @ORM\ManyToOne(targetEntity="ProductType")
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id")
     */
    protected $productType;

    /**
     * @var ProductVariant|null
     *
     * @ORM\ManyToOne(targetEntity="ProductVariant")
     * @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
     */
    protected $productVariant;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $price = 0.00;

    /**
     * 3 letter currency code
     * @var string|null
     *
     * @ORM\Column(name="currency", type="string", length=3, nullable=true)
     */
    protected $currency;

    /**
     * CUSTOM METHODS:
     */

    public function getHumanFriendlyShortDescription()
    {
        return $this->getProductType()->getTitle() . ' / ' . $this->getProductVariant()->getSize();
    }


    /**
     * GETTERS AND SETTERS:
     */


    /**
     * @return Order|null
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order|null $order
     * @return OrderItem
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     * @return OrderItem
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return OrderItem
     */
    public function setPrice(float $price): OrderItem
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param null|string $currency
     * @return OrderItem
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return ProductType|null
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param ProductType|null $productType
     * @return OrderItem
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
        return $this;
    }

    /**
     * @return ProductVariant|null
     */
    public function getProductVariant()
    {
        return $this->productVariant;
    }

    /**
     * @param ProductVariant|null $productVariant
     * @return OrderItem
     */
    public function setProductVariant($productVariant)
    {
        $this->productVariant = $productVariant;
        return $this;
    }

}