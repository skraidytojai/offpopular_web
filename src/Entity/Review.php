<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="`review`")
 * @ORM\Entity(repositoryClass="App\Repository\ReviewRepository")
 */
class Review extends AbstractEntity
{
    const SOURCE_PRINTFUL = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="remote_picture_urls", type="text", nullable=true)
     */
    protected $remotePictureUrls;

    /**
     * @var string|null
     *
     * @ORM\Column(name="our_picture_urls", type="text", nullable=true)
     */
    protected $ourPictureUrls;

    /**
     * @var int
     *
     * @ORM\Column(name="source", type="integer", nullable=false)
     */
    protected $source = self::SOURCE_PRINTFUL;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_published", type="boolean", nullable=false)
     */
    protected $isPublished = false;

    /**
     * @var ProductType|null
     *
     * @ORM\ManyToOne(targetEntity="ProductType", inversedBy="productTypes")
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id")
     */
    protected $productType;


    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return Review
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param null|string $content
     * @return Review
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getRemotePictureUrls()
    {
        return $this->remotePictureUrls;
    }

    /**
     * @param null|string $remotePictureUrls
     * @return Review
     */
    public function setRemotePictureUrls($remotePictureUrls)
    {
        $this->remotePictureUrls = $remotePictureUrls;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getOurPictureUrls()
    {
        return $this->ourPictureUrls;
    }

    /**
     * @param null|string $ourPictureUrls
     * @return Review
     */
    public function setOurPictureUrls($ourPictureUrls)
    {
        $this->ourPictureUrls = $ourPictureUrls;
        return $this;
    }

    /**
     * @return int
     */
    public function getSource(): int
    {
        return $this->source;
    }

    /**
     * @param int $source
     * @return Review
     */
    public function setSource(int $source): Review
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool $isPublished
     * @return Review
     */
    public function setIsPublished(bool $isPublished): Review
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @return ProductType|null
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param ProductType|null $productType
     * @return Review
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
        return $this;
    }
}