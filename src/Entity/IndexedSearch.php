<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Finder\Finder;
use App\Service\HumanFriendlyService;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="`indexed_search`")
 * @ORM\Entity(repositoryClass="App\Repository\IndexedSearchRepository")
 */
class IndexedSearch extends AbstractEntity
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="keyword", type="string", length=255, nullable=true)
     */
    protected $keyword;

    /**
     * @var int
     *
     * @ORM\Column(name="search_volume", type="integer", nullable=false)
     */
    protected $searchVolume = 0;

    /**
     * @var float $/click
     *
     * @ORM\Column(name="cpc", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $cpc = 0.00;

    /**
     * @var float from 0 to 1
     *
     * @ORM\Column(name="competition_score", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $competitionScore = 0.00;

    /**
     * @var int
     *
     * @ORM\Column(name="search_volume_global", type="integer", nullable=false)
     */
    protected $searchVolumeGlobal = 0;

    /**
     * @var float $/click
     *
     * @ORM\Column(name="cpc_global", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $cpcGlobal;

    /**
     * @var float from 0 to 1
     *
     * @ORM\Column(name="competition_score_global", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $competitionScoreGlobal;


    /**
     * @return null|string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param null|string $keyword
     * @return IndexedSearch
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
        return $this;
    }

    /**
     * @return int
     */
    public function getSearchVolume(): int
    {
        return $this->searchVolume;
    }

    /**
     * @param int $searchVolume
     * @return IndexedSearch
     */
    public function setSearchVolume(int $searchVolume): IndexedSearch
    {
        $this->searchVolume = $searchVolume;
        return $this;
    }

    /**
     * @return float
     */
    public function getCpc(): float
    {
        return $this->cpc;
    }

    /**
     * @param float $cpc
     * @return IndexedSearch
     */
    public function setCpc(float $cpc): IndexedSearch
    {
        $this->cpc = $cpc;
        return $this;
    }

    /**
     * @return float
     */
    public function getCompetitionScore(): float
    {
        return $this->competitionScore;
    }

    /**
     * @param float $competitionScore
     * @return IndexedSearch
     */
    public function setCompetitionScore(float $competitionScore): IndexedSearch
    {
        $this->competitionScore = $competitionScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getSearchVolumeGlobal(): int
    {
        return $this->searchVolumeGlobal;
    }

    /**
     * @param int $searchVolumeGlobal
     * @return IndexedSearch
     */
    public function setSearchVolumeGlobal(int $searchVolumeGlobal): IndexedSearch
    {
        $this->searchVolumeGlobal = $searchVolumeGlobal;
        return $this;
    }

    /**
     * @return float
     */
    public function getCpcGlobal(): float
    {
        return $this->cpcGlobal;
    }

    /**
     * @param float $cpcGlobal
     * @return IndexedSearch
     */
    public function setCpcGlobal(float $cpcGlobal): IndexedSearch
    {
        $this->cpcGlobal = $cpcGlobal;
        return $this;
    }

    /**
     * @return float
     */
    public function getCompetitionScoreGlobal(): float
    {
        return $this->competitionScoreGlobal;
    }

    /**
     * @param float $competitionScoreGlobal
     * @return IndexedSearch
     */
    public function setCompetitionScoreGlobal(float $competitionScoreGlobal): IndexedSearch
    {
        $this->competitionScoreGlobal = $competitionScoreGlobal;
        return $this;
    }
}