<?php

namespace App\Service;

use Printful\Exceptions\PrintfulApiException;
use Printful\Exceptions\PrintfulException;
use Printful\PrintfulApiClient;
use Doctrine\ORM\EntityManagerInterface;
use Printful\PrintfulMockupGenerator;
use Printful\Structures\Generator\MockupGenerationParameters;
use Printful\Structures\Placements;
use App\Entity;
use App\Helper\MockupGenerationHelper;
use App\Service\CloudStorageService;

class PrintfulService
{

    const DISALLOWED_PLACEMENT_FILES = ['preview', 'label_inside', 'mockup'];

    /** @var EntityManagerInterface */
    protected $em;

    /** @var PrintfulApiClient */
    protected $pf;

    /** @var CloudStorageService */
    protected $cs;


    public function __construct
    (
        EntityManagerInterface $em,
        CloudStorageService $cs
    )
    {
        $this->pf = new PrintfulApiClient(getenv('PRINTFUL_API_KEY'));
        $this->em = $em;
        $this->cs = $cs;
    }

    public static function isPlacementFileAllowed($file)
    {
        return !in_array($file, self::DISALLOWED_PLACEMENT_FILES);
    }

    public function getProductInfoAndVariants(int $printfulProductId)
    {
        return $this->pf->get("products/$printfulProductId");
    }

    public function getAllProductsInfo()
    {
        return $this->pf->get('products');
    }


    public function test()
    {
        try {
            // Get variants for product 10

            $variants = $this->pf->get('products');
            foreach ($variants as $variant) {
                echo $variant['id'] . ': ' . $variant['model'] . '<br>';
            }

            dump($variants);

            $variants = $this->pf->get('products/257');
            dump($variants);

            // Get information about Variant 1007
            $data = $this->pf->get('products/variant/8852');
            dump($data);
        } catch (PrintfulApiException $e) { //API response status code was not successful
            echo 'Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
        } catch (PrintfulException $e) { //API call failed
            echo 'Printful Exception: ' . $e->getMessage();
            var_export($this->pf->getLastResponseRaw());
        }
    }


    public function initiateMockupGenTask(Entity\ProductType $productType, Entity\Product $product = null, $isReinitiated = false)
    {
        $generator = new PrintfulMockupGenerator($this->pf);

        $params = new MockupGenerationParameters();
        $params->productId = $productType->getPrintfulProductId();
        $params->variantIds = $productType->getPrintfulVariantIds(true);
        $apiCallPurpose = $isReinitiated ? Entity\ExternalApiCall::PURPOSE_MOCKUP_GEN_REINIT : Entity\ExternalApiCall::PURPOSE_MOCKUP_GEN_INIT;

        $imagePlacements = [];
        foreach ($productType->getPlacementFiles(true) as $placementFile) {
            if (false === self::isPlacementFileAllowed($placementFile)) {
                continue;
            }
            $pictureUrl = $this->cs->getAwsJpgDesignFileSignedUrl($product, $productType, $placementFile);
            $imagePlacements[$placementFile] = $pictureUrl;

            $params->addImageUrl($placementFile, $pictureUrl);
        }

        $retries = 0;
        while (true) {
            try {
                $retries++;

                $externalApiCall = $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('printful', $apiCallPurpose, [
                    'product' => $product,
                    'productType' => $productType,
                ]);
                $resultItem = $generator->createGenerationTask($params);
                break;
            } catch (\Exception $e) {
                if ($retries >= 3) {
                    throw $e;
                }
                sleep(15); // Might be because of CURL/connectivity to Printful errors, retry after 30 seconds
            }
        }

        $repo = $this->em->getRepository(Entity\PrintfulMockupGenTask::class);
        $repo->createNewTask($resultItem->taskKey, $productType, $imagePlacements, $resultItem->error, $product);

        if ($resultItem->error) {
            $externalApiCall->setOutcome(Entity\ExternalApiCall::OUTCOME_FAILED)
                ->setError($resultItem->error);

            $this->em->flush();
        }

        return true;
    }

    /**
     * @param Entity\PrintfulMockupGenTask $genTask
     * @return \Printful\Structures\Generator\GenerationResultItem
     */
    public function retrieveMockupGenTaskResult(Entity\PrintfulMockupGenTask $genTask)
    {
        // Log api call first, on our side
        $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('printful', Entity\ExternalApiCall::PURPOSE_MOCKUP_GEN_RETRIEVAL, [
            'product' => $genTask->getProduct(),
            'productType' => $genTask->getProductType(),
        ]);

        $generator = new PrintfulMockupGenerator($this->pf);
        $result = $generator->getGenerationTask($genTask->getTaskKey());

        return $result;
    }

    /**
     * @param array $recipientInfo
     * @param Entity\ProductVariant[] $productVariants
     * @param string $currency
     * @return float
     * @throws \Exception
     */
    public function calculatePrintfulShippingPrice(array $recipientInfo, array $productVariants, string $currency)
    {
        $externalApiCall = $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('printful', Entity\ExternalApiCall::PURPOSE_SHIPPING_PRICE_CALCULATE);

        $items = [];
        foreach ($productVariants as $productVariant) {
            $items[] = [
                'variant_id' => $productVariant->getPrintfulVariant()->getPrintfulVariantId(),
                'quantity' => 1,
                'value' => $productVariant->getPrice(),
            ];
        }

        try {
            $shippingMethods = $this->pf->post('shipping/rates', [
                'recipient' => [
                    'country_code' => $recipientInfo['country'],
                ],
                'items' => $items,
                'currency' => $currency,
            ]);
        } catch (\Exception $e) {
            $externalApiCall
                ->setError($e->getMessage())
                ->setOutcome(500);

            $this->em->flush();

            throw $e;
        }

        usort($shippingMethods, function ($a, $b) {
            return $a['rate'] < $b['rate'] ? -1 : 1;
        });

        if (isset($shippingMethods[0]['rate'])) {
            return (float) $shippingMethods[0]['rate'];
        }

        return 0.00;
    }
}