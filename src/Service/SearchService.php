<?php

namespace App\Service;

use App\Entity;
use Psr\Log\LoggerInterface;
use Elastica\Query;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use App\DataObject\ProductResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;

class SearchService
{
    /**
     * ElasticSearch Repository Manager instance
     *
     * @var RepositoryManagerInterface
     */
    protected $erm;

    /** @var CacheService */
    protected $cacheService;

    /** @var FastMemoryService */
    protected $fms;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var RouterInterface */
    protected $router;

    public function __construct
    (
        RepositoryManagerInterface $erm,
        CacheService $cacheService,
        FastMemoryService $fms,
        EntityManagerInterface $em,
        RouterInterface $router
    )
    {
        $this->erm = $erm;
        $this->cacheService = $cacheService;
        $this->fms = $fms;
        $this->em = $em;
        $this->router = $router;
    }

    public function determineProductTypeCategory(string $searchString)
    {
        $productTypeCategoryTitles = $this->cacheService->getPublishedProductTypeCategorySearchTitles();
        if (!$productTypeCategoryTitles) {
            return ['', null];
        }

        foreach ($productTypeCategoryTitles as $productTypeCategoryTitle => $productTypeCategory) {
            if (stripos($searchString, $productTypeCategoryTitle) !== false) {
                return [$productTypeCategoryTitle, $productTypeCategory];
            }
        }

        return ['', null];
    }

    public function determineProductType(string $searchString)
    {
        $titles = $this->cacheService->getPublishedProductTypeSearchTitles();
        if (!$titles) {
            return ['', null];
        }

        foreach ($titles as $title => $productType) {
            if (stripos($searchString, $title) !== false) {
                return [$title, $productType];
            }
        }

        return ['', null];
    }

    public function prepareProductSearchString(string $searchString, $type = 'matched_product_type')
    {
        switch ($type) {
            case 'matched_product_type':
                $productTypeTitles = $this->cacheService->getPublishedProductTypeSearchTitles();
                if ($productTypeTitles) {
                    $searchString = str_ireplace(array_keys($productTypeTitles), '', $searchString);
                }
                break;
            case 'matched_product_type_category':
                $productTypeCategoryTitles = $this->cacheService->getPublishedProductTypeCategorySearchTitles();
                if ($productTypeCategoryTitles) {
                    $searchString = str_ireplace(array_keys($productTypeCategoryTitles), '', $searchString);
                }
                break;
            default:
                throw new \Exception('Unsupported type');
        }

        return trim($searchString);
    }

    /**
     * @param string $searchString
     * @param int $limit
     * @return Entity\ProductTypeCategory[]|array
     */
    public function searchProductTypeCategories(string $searchString, int $limit = 100)
    {
        $repository = $this->erm->getRepository('App:ProductTypeCategory');

        // old status quo: $query = (new Query\MultiMatch())->setQuery($searchString)->setFields(['title'])->setFuzziness(1);
        $query = (new Query\Match)->setFieldQuery('title', $searchString)->setFieldFuzziness('title', 1)->setFieldMinimumShouldMatch('title', '100%');
        $productTypeCategories = $repository->find($query, $limit, []);

        return $productTypeCategories;
    }

    /**
     * @param string $searchString
     * @param int $limit
     * @return Entity\ProductType[]|array
     */
    protected function searchProductTypes(string $searchString, int $limit = 100)
    {
        $repository = $this->erm->getRepository('App:ProductType');

        // other candidates:
        // $productTypes = $repository->find('*' . $searchString . '*', null, []);
        //$query = (new Query\Fuzzy('title', $searchString))->setFieldOption('fuzziness', 2)->setFieldOption('transpositions', true);
        //$query = (new Query\Match('title', $searchString));
        //$query = (new Query\MatchAll('title', $searchString));

        // old status quo: $query = (new Query\MultiMatch())->setQuery($searchString)->setFields(['title'])->setFuzziness(1);
        $query = (new Query\Match)->setFieldQuery('title', $searchString)->setFieldFuzziness('title', 1)->setFieldMinimumShouldMatch('title', '100%');

        $productTypes = $repository->find($query, $limit, []);

        return $productTypes;
    }

    /**
     * @param string $searchString
     * @param int $maxPerPage
     * @return \Pagerfanta\Pagerfanta
     */
    public function searchProducts(string $searchString, int $maxPerPage)
    {
        $repository = $this->erm->getRepository('App:Product');
        // other candidates:
        //$query = (new Query\MultiMatch())->setQuery($searchString)->setFields(['title'])->setFuzziness(1);
        //$query = (new Query\BoolQuery())->addMust(['match' => ['title' => ['query' => $searchString, 'fuzziness' => 1]]]);
        //$query = (new Query\BoolQuery())->addMust(['match' => ['tags' => ['query' => $searchString, 'fuzziness' => 1]]]);

        if (strlen($searchString) <= 1) {
            $query = new Query\MatchAll();
        } else {
            $query = (new Query\BoolQuery())
                ->addShould(['match' => ['tags' => ['query' => $searchString, 'fuzziness' => 1]]])
                ->addShould(['match' => ['title' => ['query' => $searchString, 'fuzziness' => 1]]])
            ;
        }

        $products = $repository->findPaginated($query)->setMaxPerPage($maxPerPage);
        $products->setAllowOutOfRangePages(true); // to be able to return empty page result (as empty array) instead of throwing an exception. this is essential for lazy loader

        return $products;
    }

    /**
     * @param string $searchString
     * @param int $maxPerPage
     * @param int $page
     * @return array
     */
    public function search(string $searchString, int $maxPerPage = 15, int $page = 1)
    {
        $finalSearchString = $searchString;
        $determinedProductTypeCategory = $this->determineProductTypeCategory($searchString);
        $determinedProductType = $this->determineProductType($searchString);

        $matchedProductTypeCategory = null;

        $matchedProductType = $determinedProductType[1];

        $matchedProductTypeCategory = $determinedProductTypeCategory[1];

        // Product type category string filtering should always go first because they're usually longer, plural-form words.
        // If we have string: "dresses", we match product type category ("dresses") first, instead of product type ("dress").
        // And we get an empty string - our desired outcome. Otherwise, if we match product type firt, we get a "s" string - which is
        // a very problematic string. Instead of 2000 results in search, customer might only see 10, because that "s" is used for product title/tag search.

        if ($matchedProductTypeCategory) {
            $finalSearchString = $this->prepareProductSearchString($finalSearchString, 'matched_product_type_category');
        }

        if ($matchedProductType) {
            $finalSearchString = $this->prepareProductSearchString($finalSearchString, 'matched_product_type');
        }

        $paginatorResult = $this->searchProducts($finalSearchString, $maxPerPage)->setCurrentPage($page);

        $this->fms->logSearch([
            'originalSearchString' => $searchString,
            'finalSearchString' => $finalSearchString,
            'matchedProductTypeId' => $matchedProductType ? $matchedProductType->getId() : null,
            'matchedProductTypeCategoryId' => $matchedProductTypeCategory ? $matchedProductTypeCategory->getId() : null,
            'totalResults' => $paginatorResult->getNbResults(),
            'searchedAt' => (new \DateTime())->format('Y-m-d H:i:s'),
        ]);

        $searchResult = [
            'products' => $paginatorResult->getCurrentPageResults(),
            'matchedProductType' => $matchedProductType,
            'matchedProductTypeCategory' => $matchedProductTypeCategory,
            'originalSearchString' => $searchString,
            'finalSearchString' => $finalSearchString,
            'totalResults' => $paginatorResult->getNbResults(),
        ];

        return $this->enhanceSearchResult($searchResult, $matchedProductType, $matchedProductTypeCategory);
    }

    public function enhanceSearchResult(array $searchResult, Entity\ProductType $productType = null, Entity\ProductTypeCategory $productTypeCategory = null)
    {
        $productTypePool = null;
        if (!$productType && !$productTypeCategory) {
            $productTypePool = $this->em->getRepository(Entity\ProductType::class)->getAllPublished();
        } else if ($productTypeCategory) {
            $productTypePool = $this->em->getRepository(Entity\ProductTypeCategory::class)->getPublishedChildProductTypes($productTypeCategory);
        }

        /** @var Entity\Product[] $products */
        $products = $searchResult['products'];

        $productResults = [];
        foreach ($products as $product) {
            $productTypeForProductResult = is_null($productType) ? $productTypePool[array_rand($productTypePool)] : $productType;
            $productResult = new ProductResult($product, $productTypeForProductResult);
            $productResult->url = $this->router->generate('view_product', [
                'id' => $product->getId(),
                'slug' => $product->getSlug(),
                'productTypeSlug' => $productTypeForProductResult->getSlug(),
            ]);

            $productResults[$product->getId()] = $productResult;
        }

        // DEPRECATED START:
        /* $mainProductMockups = $this->em->getRepository(Entity\ProductMockup::class)->getMainProductMockupsForProductResultArray($productResults);

        foreach ($mainProductMockups as $mainProductMockup) {
            /** @var ProductResult $productResult */
            /*$productResult = $productResults[$mainProductMockup->getProduct()->getId()];
            if (!$productResult->mainProductMockup) { // to avoid overrides by lower-priority mockups
                $productResult->mainProductMockup = $mainProductMockup;
            }
        }
        */
        // DEPRECATED END:

        $this->fms->populateProductResultArrayWithMockupIds($productResults);

        $searchResult['productResults'] = array_values($productResults); // rearrange array indexes from 0, to avoid JS transforming from array to object on the frontend...

        return $searchResult;
    }
}