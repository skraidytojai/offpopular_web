<?php

namespace App\Service;

use App\DataObject\ProductResult;
use Predis;

class FastMemoryService
{
    const REDIS_KEY_PREFIX = 'offpop';

    /** @var  Predis\Client */
    protected $redis;

    /**
     * FastMemoryService constructor.
     * @param Predis\Client $redis
     */
    public function __construct(Predis\Client $redis)
    {
        $this->redis = $redis;
    }


    public function logSearch(array $params)
    {
        // dont forget our project prefix_env
        //$redis = $this->get('snc_redis.default');
        $this->redis->rpush(self::REDIS_KEY_PREFIX . ':search_logs', [json_encode($params)]);

        return true;
    }

    public function cacheMockups(int $productId, array $mockupIdsByProductTypeIds)
    {
        $finalArray = [];

        foreach ($mockupIdsByProductTypeIds as $productTypeId => $mockupId) {
            $key = self::REDIS_KEY_PREFIX . ':product_mockup_cache:' . $productId . ':' . $productTypeId;
            $finalArray[$key] = $mockupId;
        }

        $this->redis->mset($finalArray);
    }

    /**
     * @param ProductResult[] $productResults
     * @return array
     */
    public function populateProductResultArrayWithMockupIds(array $productResults)
    {
        foreach ($productResults as $productResult) {
            $mockupId = $this->redis->get(self::REDIS_KEY_PREFIX . ':product_mockup_cache:' . $productResult->product->getId() . ':' . $productResult->productType->getId());
            $productResult->mainProductMockupId = $mockupId;
        }

        return $productResults;
    }
}