<?php

namespace App\Service;

use App\Entity;
use Psr\Log\LoggerInterface;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Doctrine\ORM\EntityManagerInterface;
use App\Helper\MockupGenerationHelper;

class CloudStorageService
{
    const PRODUCT_ZIP_BUCKET = 'prochub-zip-in';
    const PRODUCT_JPGS_OUT_BUCKET = 'prochub-product-jpgs';
    const PRODUCT_MOCKUPS_BUCKET = 'prochub-product-mockups';
    const PRODUCT_MOCKUPS_RESIZED_BUCKET = 'prochub-product-mockups-resized';

    /** @var S3Client */
    protected $s3;

    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->s3 = new S3Client([
            'version' => getenv('AWS_VERSION'),
            'region'  => getenv('AWS_REGION'),
        ]);

        $this->em = $em;
    }


    public function uploadProductZipFile(Entity\Product $product, string $sourcePath, string $key)
    {
        // try/catch: S3Exception $e

        $result = $this->s3->putObject([
            'Bucket' => self::PRODUCT_ZIP_BUCKET,
            'Key'    => $key,
            'SourceFile' => $sourcePath,
            //'ACL'    => 'public-read'
        ]);

        $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('aws_s3', 'put_product_zip', [
            'product' => $product,
            'result' => (string) $result,
        ]);

        // Print the URL to the object.
        return $result;
    }

    public function getAwsJpgDesignFileSignedUrl(Entity\Product $product, Entity\ProductType $productType = null, string $placement = 'default', string $resolution = '3k')
    {
        $genStrategyString = MockupGenerationHelper::getGenerationStrategyString($product, $productType, $placement);
        $hashedFilename = self::getHashedJpgFilename($product, $resolution, $genStrategyString);

        // first: check object size

        $bucket = self::PRODUCT_JPGS_OUT_BUCKET;
        $key = $product->getId() . '/' . $hashedFilename;

        $fileSize = $this->getS3ObjectFileSize($bucket, $key);

        if ($fileSize < 1000) { // might be that extended_mirrored failed to generate, so let's try the usual one instead:
            $hashedFilename = self::getHashedJpgFilename($product, $resolution, '');
            $key = $product->getId() . '/' . $hashedFilename;

            $fileSize = $this->getS3ObjectFileSize($bucket, $key);

            if ($fileSize < 1000) { // failed again, so no options left...
                return false;
            }
        }

        $cmd = $this->s3->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key'    => $key,
        ]);

        $request = $this->s3->createPresignedRequest($cmd, '+180 minutes');

        return (string) $request->getUri();
    }

    public function uploadRemoteFileToS3Bucket(string $url, string $bucket, string $key, bool $isPublic = false)
    {
        $params = [
            'Bucket' => $bucket,
            'Key'    => $key,
            'Body'   => file_get_contents($url),
            'ContentType' => 'image/jpeg', // required because streamed body doesnt have it inside
        ];

        if ($isPublic) {
            $params['ACL'] = 'public-read';
        }

        $result = $this->s3->putObject($params);

        return (bool) $result->get('ObjectURL');
    }

    protected static function getHashedJpgFilename(Entity\Product $product, string $resolution, string $genStrategyString)
    {
        $salt = getenv('JPG_FILENAME_SALT');
        $string = $product->getId() . '_' . $resolution . $genStrategyString . '.jpg' . $salt;

        return hash('sha256', $string) . '.jpg';
    }

    /**
     * @param string $bucket
     * @param string $key
     * @return int
     */
    public function getS3ObjectFileSize(string $bucket, string $key)
    {
        try {
            $objectInfo = $this->s3->headObject([
                'Bucket' => $bucket,
                'Key' => $key,
            ]);

            return (int) $objectInfo['@metadata']['headers']['content-length'];
        } catch (\Exception $e) {
            return 0; // file doesnt exist
        }

    }
}