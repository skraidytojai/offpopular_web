<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Swift_Mailer;
use Twig_Environment;

class EmailService
{
    const FROM_EMAIL = 'tomas@offpopular.com';
    const FROM_NAME = 'Tomas / OffPopular.com';

    /** @var EntityManagerInterface */
    protected $em;

    /** @var RouterInterface */
    protected $router;

    /** @var TwigEngine */
    protected $templating;

    /** @var Swift_Mailer */
    protected $mailer;

    public function __construct
    (
        EntityManagerInterface $em,
        RouterInterface $router,
        Twig_Environment $templating,
        Swift_Mailer $mailer
    )
    {
        $this->em = $em;
        $this->router = $router;
        $this->templating = $templating;
        $this->mailer = $mailer;
    }

    public function sendPurchaseReceipt(Entity\Order $order)
    {
        /*  return $this->templating->render(
             'Emails/purchaseReceipt.html.twig',
             [
                 'order' => $order,
             ]
         ); */

        $message = (new \Swift_Message('Thank you for your order at (' . $order->getId() . ') OffPopular.com!'))
            ->setFrom(self::FROM_EMAIL, self::FROM_NAME)
            ->setTo($order->getBuyerEmail())
            ->setBody(
                $this->templating->render(
                    'Emails/purchaseReceipt.html.twig',
                    [
                        'order' => $order,
                    ]
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'Emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
        ;

        $this->mailer->send($message);

        // SEND A COPY TO US

        $message = (new \Swift_Message('NEW ORDER (' . $order->getId() . ') OffPopular.com!'))
            ->setFrom(self::FROM_EMAIL, self::FROM_NAME)
            ->setTo(self::FROM_EMAIL)
            ->setReplyTo($order->getBuyerEmail(), $order->getBuyerFullName())
            ->setBody(
                $this->templating->render(
                    'Emails/purchaseReceipt.html.twig',
                    [
                        'order' => $order,
                    ]
                ),
                'text/html'
            )
        ;

        $this->mailer->send($message);

        return true;
    }

    public function sendExclusiveBuyoutInquiry(array $params)
    {
        $message = (new \Swift_Message('Exclusive buyout inquiry at OffPopular.com (' . $params['form']['email'] . ')!'))
            ->setFrom(self::FROM_EMAIL, self::FROM_NAME)
            ->setTo(self::FROM_EMAIL)
            ->setReplyTo($params['form']['email'], $params['form']['name'])
            ->setBody(
                $this->templating->render(
                    'Emails/exclusiveBuyoutInquiry.html.twig',
                    [
                        'params' => $params,
                    ]
                ),
                'text/html'
            )
        ;

        $this->mailer->send($message);

        return true;
    }

    public function sendParticipateInGameConfirmation(string $gameRoute, Entity\User $user)
    {
        $message = (new \Swift_Message("You're participating in OffPopular's giveaway!"))
            ->setFrom(self::FROM_EMAIL, self::FROM_NAME)
            ->setTo($user->getEmail(), $user->getFirstName())
            ->setBody(
                $this->templating->render(
                    'Emails/Game/participateConfirmation.html.twig',
                    [
                        'gameRoute' => $gameRoute,
                        'user' => $user,
                    ]
                ),
                'text/html'
            )
        ;

        $this->mailer->send($message);

        return true;
    }

    public function sendFriendsInvitationToContest(string $gameRoute, Entity\User $senderUser, Entity\User $invitedUser)
    {
        $senderFirstName = $senderUser->getFirstName() ?? '';
        $message = (new \Swift_Message("{$senderFirstName} ({$senderUser->getEmail()}) invited you to participate in a giveaway!"))
            ->setFrom(self::FROM_EMAIL, self::FROM_NAME)
            ->setTo($invitedUser->getEmail(), $invitedUser->getFirstName())
            ->setBody(
                $this->templating->render(
                    'Emails/Game/sendFriendsInvitation.html.twig',
                    [
                        'gameRoute' => $gameRoute,
                        'senderUser' => $senderUser,
                        'invitedUser' => $invitedUser,
                    ]
                ),
                'text/html'
            )
        ;

        $this->mailer->send($message);

        return true;
    }
}