<?php

namespace App\Service;

use Braintree;
use App\Entity;
use Psr\Log\LoggerInterface;

class BraintreeService
{
    const STATUS_SUCCESS = 200;
    const STATUS_ERROR = 500;

    const REAL_ENV = 'production';
    const TEST_ENV = 'sandbox';


    /** @var Braintree\Gateway */
    protected $gateway;

    protected $currentEnv;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->initiateGateway(getenv('BRAINTREE_ENVIRONMENT'));
        $this->logger = $logger;
    }

    public function testErrorLog()
    {
        $this->logger->error('Braintree error test');
    }

    public function initiateGateway($env)
    {
        if ($env == 'sandbox') {
            $params = [
                'environment' => 'sandbox',
                'merchantId' => getenv('BRAITNREE_MERCHANT_ID_SANDBOX'),
                'publicKey' => getenv('BRAINTREE_PUBLIC_KEY_SANDBOX'),
                'privateKey' => getenv('BRAINTREE_PRIVATE_KEY_SANDBOX'),
            ];
        } else if ($env == 'production') {
            $params = [
                'environment' => 'production',
                'merchantId' => getenv('BRAITNREE_MERCHANT_ID_PRODUCTION'),
                'publicKey' => getenv('BRAINTREE_PUBLIC_KEY_PRODUCTION'),
                'privateKey' => getenv('BRAINTREE_PRIVATE_KEY_PRODUCTION'),
            ];
        } else {
            throw new \Exception('Unknown Braintree environment');
        }

        $this->currentEnv = $env;

        $this->gateway = new Braintree\Gateway($params);
    }

    public function getClientToken($customerId = null)
    {
        return $this->gateway->clientToken()->generate([
            "customerId" => $customerId
        ]);
    }

    /**
     * @return Braintree\Transaction|bool
     */
    public function initiateTransaction(Entity\Order $order, $nonce)
    {
        $result = $this->gateway->transaction()->sale([
            'amount' => $order->getFinalChargeAmount(),
            'orderId' => $order->getId(),
            'merchantAccountId' => $this->currentEnv == static::REAL_ENV ? getenv('BRAINTREE_MERCHANT_ACCOUNT_ID_PRODUCTION') : getenv('BRAINTREE_MERCHANT_ACCOUNT_ID_SANDBOX'),
            'paymentMethodNonce' => $nonce, // fake-valid-nonce also available for testing purposes
            'customer' => [
                'firstName' => $order->getBuyerFirstName(),
                'lastName' => $order->getBuyerLastName(),
                'phone' => $order->getBuyerPhone(),
                'email' => $order->getBuyerEmail()
            ],
            'billing' => [
                'firstName' => $order->getBuyerFirstName(),
                'lastName' => $order->getBuyerLastName(),
                'streetAddress' => $order->getShippingAddress(),
                'locality' => $order->getShippingCity(),
                'postalCode' => $order->getShippingPostCode(),
                'countryCodeAlpha2' => $order->getShippingCountryTwoLetter(),
            ],
            'shipping' => [
                'firstName' => $order->getBuyerFirstName(),
                'lastName' => $order->getBuyerLastName(),
                'streetAddress' => $order->getShippingAddress(),
                'locality' => $order->getShippingCity(),
                'postalCode' => $order->getShippingPostCode(),
                'countryCodeAlpha2' => $order->getShippingCountryTwoLetter(),
            ],
            'options' => [
                'submitForSettlement' => true
            ]
        ]);

        if ($result->success) {
            $transaction = $result->transaction;
            if (in_array($transaction->status, ['submitted_for_settlement', 'settling', 'settled'])) {
                return static::STATUS_SUCCESS;
            }
        } else {
            foreach($result->errors->deepAll() AS $error) {
                $this->logger->error('BRAINTREE ERROR at InitiateTransaction(), order id: ' . $order->getId() . ' : ' . $error->code . ": " . $error->message);
            }
        }

        return static::STATUS_ERROR;
    }
}