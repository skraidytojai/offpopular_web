<?php

namespace App\Service;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity;

class CacheService
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var AdapterInterface */
    protected $cache;

    public function __construct
    (
        EntityManagerInterface $em,
        AdapterInterface $cache
    )
    {
        $this->em = $em;
        $this->cache = $cache;
    }

    public function getPublishedProductsCount($resetCache = false)
    {
        $cacheContainer = $this->cache->getItem('published_products_count');
        if (!$resetCache && $cacheContainer->isHit()) {
            return $cacheContainer->get() ?: [];
        }

        $result = $this->em->getRepository(Entity\Product::class)->getPublishedProductsCount();
        $cacheContainer->set($result);
        $this->cache->save($cacheContainer);

        return $result;
    }

    public function getPublishedProductTypesCount($resetCache = false)
    {
        $cacheContainer = $this->cache->getItem('published_product_types_count');
        if (!$resetCache && $cacheContainer->isHit()) {
            return $cacheContainer->get() ?: [];
        }

        $result = $this->em->getRepository(Entity\ProductType::class)->getPublishedCount();
        $cacheContainer->set($result);
        $this->cache->save($cacheContainer);

        return $result;
    }

    /**
     * @return Entity\ProductType[]|array
     */
    public function getAllPublishedProductTypes($resetCache = false)
    {
        $cacheContainer = $this->cache->getItem('all_published_product_types');
        if (!$resetCache && $cacheContainer->isHit()) {
            return $cacheContainer->get() ?: [];
        }

        $result = $this->em->getRepository(Entity\ProductType::class)->getAllPublished();
        $cacheContainer->set($result);
        $this->cache->save($cacheContainer);

        return $result;
    }

    /**
     * @return Entity\ProductTypeCategory[]|array
     */
    public function getAllPublishedProductTypeCategoriesTopLevel($resetCache = false)
    {
        $cacheContainer = $this->cache->getItem('all_published_product_type_categories_top_level');
        if (!$resetCache && $cacheContainer->isHit()) {
            return $cacheContainer->get() ?: [];
        }

        $result = $this->em->getRepository(Entity\ProductTypeCategory::class)->getPublishedTopLevel();
        $cacheContainer->set($result);
        $this->cache->save($cacheContainer);

        return $result;
    }

    /**
     * @param bool $resetCache
     * @return Entity\ProductType[]|array
     */
    public function getProductTypesPerProductTypeCategory($resetCache = false)
    {
        $cacheContainer = $this->cache->getItem('product_types_per_product_type_category');
        if (!$resetCache && $cacheContainer->isHit()) {
            return $cacheContainer->get() ?: [];
        }

        $productTypeCategories = $this->em->getRepository(Entity\ProductTypeCategory::class)->getAllPublished();
        $result = [];
        foreach ($productTypeCategories as $productTypeCategory) {
            $result[$productTypeCategory->getId()] = $this->em->getRepository(Entity\ProductTypeCategory::class)->getPublishedChildProductTypes($productTypeCategory);
        }

        $cacheContainer->set($result);
        $this->cache->save($cacheContainer);

        return $result;
    }

    public function getTopLevelCategoriesPerProductType($resetCache = false)
    {
        $cacheContainer = $this->cache->getItem('top_level_categories_per_product_type');
        if (!$resetCache && $cacheContainer->isHit()) {
            return $cacheContainer->get() ?: [];
        }

        $productTypes = $this->em->getRepository(Entity\ProductType::class)->getAllPublished();
        $result = [];
        foreach ($productTypes as $productType) {
            $result[$productType->getId()] = $productType->getProductTypeCategory()->getIsTopLevel() ? $productType->getProductTypeCategory() : $productType->getProductTypeCategory()->getParentProductTypeCategory();
        }

        $cacheContainer->set($result);
        $this->cache->save($cacheContainer);

        return $result;
    }

    /**
     * @return array
     */
    public function getPublishedProductTypeSearchTitles($resetCache = false)
    {
        $cacheContainer = $this->cache->getItem('product_type_search_titles');
        if (!$resetCache && $cacheContainer->isHit()) {
            return $cacheContainer->get() ?: [];
        }

        $result = $this->em->getRepository(Entity\ProductType::class)->getAllPublished();
        $titles = [];
        foreach ($result as $productType) {
            $titles[$productType->getTitleForSearch()] = $productType;
            foreach ($productType->getTitleForSearchAlternatives(true) as $alternative) {
                if (strlen($alternative) > 0) {
                    $titles[$alternative] = $productType;
                }
            }
        }

        // Sort array keys from strlen highest to lowest, so that "bags" would be matched first over "bag".
        uksort($titles, function ($a,$b) {
            return strlen($b) - strlen($a);
        });

        $cacheContainer->set($titles);
        $this->cache->save($cacheContainer);

        return $titles;
    }

    /**
     * @return array
     */
    public function getPublishedProductTypeCategorySearchTitles($resetCache = false)
    {
        $cacheContainer = $this->cache->getItem('product_type_category_search_titles');
        if (!$resetCache && $cacheContainer->isHit()) {
            return $cacheContainer->get() ?: [];
        }

        $result = $this->em->getRepository(Entity\ProductTypeCategory::class)->getAllPublished();
        $titles = [];
        foreach ($result as $productTypeCategory) {
            $titles[$productTypeCategory->getTitleForSearch()] = $productTypeCategory;
            foreach ($productTypeCategory->getTitleForSearchAlternatives(true) as $alternative) {
                if (strlen($alternative) > 0) {
                    $titles[$alternative] = $productTypeCategory;
                }
            }
        }

        // Sort array keys from strlen highest to lowest, so that "bags" would be matched first over "bag".
        uksort($titles, function ($a,$b) {
            return strlen($b) - strlen($a);
        });

        $cacheContainer->set($titles);
        $this->cache->save($cacheContainer);

        return $titles;
    }
}