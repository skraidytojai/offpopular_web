<?php

namespace App\Service;

class HumanFriendlyService
{
    private static $availableProductTypes = [
        'fine_art_canvas' => 'Museum canvas (Fine Art)',
        'photo_print' => 'Poster',
        'phone_case' => 'Phone case',
        'tablet' => 'Tablet (iPad) case',
        'cushion' => 'Cushion',
        'tote_bag' => 'Tote bag',
        'towel' => 'Towel',
        'mug' => 'Mug',
        'tshirt_sublimation' => 'T-Shirt',
        'greeting_card' => 'Greeting cards',
        'flip_flops' => 'Flip flops',
        'drawstring_bag' => 'Sport bag',
        'backpack' => 'Backpack',
    ];

    private static $mainProductFeatures = [
        'fine_art_canvas' => [
            'Long-lasting canvas',
            'Expressive, vibrant colors',
            'Gallery wrap, wooden underframe',
        ],
        'photo_print' => [
            'Giclée print',
            'High quality paper',
        ],
        'phone_case' => [
            'For iPhone or Samsung',
            'Solid, long-lasting polycarb back',
            'Protects from scratches, dust, oil, and dirt',
        ],
        'tablet' => [
            'Slim and chic Snap case',
            'Solid build (space grade materials)',
        ],
        'cushion' => ['Suede, canvas or linen - will fit your interior well.'],
        'tote_bag' => [
            'Printed on BOTH sides!',
            'Attracts attention',
        ],
        'towel' => [
            'Good for bath or beach',
            'Three sizes',
        ],
        'mug' => [
            'Orca coating',
            'Dishwasher safe',
        ],
        'tshirt_sublimation' => [
            'Innovative print technique',
            'Printed on separate parts, cut & sewn',
            'Printed on BOTH sides!',
            'See size guide below',
        ],
        'greeting_card' => [
            'Pack (5 or 10 units)',
            '2 different sizes',
            '330 GSM Fedrigoni paper',
            'Gloss UV coating',
        ],
        'flip_flops' => [
            'Easy to wear, perfect for summer',
            '3 different sizes',
            'See size guide below',
        ],
        'drawstring_bag' => [
            'Printed on BOTH sides!',
            '100% spun polyester',
            'Ergonomic bag straps',
        ],
        'backpack' => [
            'Pocket for 15" laptop',
            'Hidden pocket on back side',
            'Ergonomic bag straps',
        ],
    ];

    public function allProductTypes()
    {
        return self::$availableProductTypes;
    }

    public function mainProductFeatures()
    {
        return self::$mainProductFeatures;
    }

    public static function humanizeProductType($type)
    {
        return self::$availableProductTypes[$type] ?? $type;
    }

    public static function allProductTypeKeys()
    {
        return array_keys(self::$availableProductTypes);
    }
}