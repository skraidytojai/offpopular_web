<?php

namespace App\Service;

use App\Entity;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use GuzzleHttp\Client as GuzzleClient;
use Doctrine\ORM\EntityManagerInterface;
use Aws\Lambda\LambdaClient;

class CloudProcessingService
{
    const PRODUCT_ZIP_BUCKET = 'prochub-zip-in';
    const PRODUCT_EPS_BUCKET = 'prochub-eps-in';

    /** @var S3Client */
    protected $s3;

    /** @var GuzzleClient */
    protected $lambdaApi;

    /** @var GuzzleClient */
    protected $ec2Api;

    /** @var LambdaClient */
    protected $lambdaClient;

    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->s3 = new S3Client([
            'version' => getenv('AWS_VERSION'),
            'region'  => getenv('AWS_REGION'),
        ]);

        $this->lambdaApi = new GuzzleClient(['base_uri' => 'https://0ulko0cuq5.execute-api.us-east-2.amazonaws.com/api/']);
        $this->ec2Api = new GuzzleClient(['base_uri' => 'http://13.59.34.114/']);
        $this->lambdaClient = new LambdaClient([
            'version' => getenv('AWS_VERSION'),
            'region'  => getenv('AWS_REGION'),
        ]);

        $this->em = $em;
    }

    /**
     * @param Entity\Product $product
     * @return bool
     */
    public function initiateProductUnzipAndEpsTransfer(Entity\Product $product)
    {
        $response = $this->lambdaApi->post('unzip_and_transfer_eps', [
            'debug' => getenv('APP_ENV') === 'dev',
            'body' => json_encode(['product_id' => $product->getId()]),
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);

        $body = (string)$response->getBody();
        $bodyDecoded = json_decode($body, true);

        if ($response->getStatusCode() == 200 && $bodyDecoded['status'] == 'ok') {
            $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('aws_lambda', 'unzip_and_transfer_eps', [
                'product' => $product,
                'outcome' => $response->getStatusCode(),
            ]);

            return true;
        }

        $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('aws_lambda', 'unzip_and_transfer_eps', [
            'product' => $product,
            'outcome' => $response->getStatusCode(),
            'error' => $body,
        ]);

        return false;
    }

    public function initiateEpsToJpgConversion(Entity\Product $product)
    {
        $response = $this->ec2Api->post('convert_eps_to_jpg', [
            'debug' => getenv('APP_ENV') === 'dev',
            'body' => json_encode(['product_id' => $product->getId()]),
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);

        $body = (string)$response->getBody();
        $bodyDecoded = json_decode($body, true);

        if ($bodyDecoded['status'] == 200 && $bodyDecoded['message'] == 'ok') {
            $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('ec2_processor', 'convert_eps_to_jpg', [
                'product' => $product,
                'outcome' => $bodyDecoded['status'],
            ]);

            return true;
        }

        $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('ec2_processor', 'convert_eps_to_jpg', [
            'product' => $product,
            'outcome' => $bodyDecoded['status'],
            'error' => $body,
        ]);

        return false;
    }

    /**
     * @param Entity\Product $product
     * @return bool
     */
    public function initiateProductJpgResize(Entity\Product $product)
    {
        $result = $this->lambdaClient->invoke([
            'FunctionName' => 'indep_product_jpg_resize',
            'Payload' => json_encode(['product_id' => $product->getId()]),
        ]);

        $response = json_decode((string)$result->get('Payload'), true);

        if ($response['status'] == 200 && $response['message'] == 'ok') {
            $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('aws_lambda', 'product_jpg_resize', [
                'product' => $product,
                'outcome' => $response['status'],
            ]);

            return true;
        }

        $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('aws_lambda', 'product_jpg_resize', [
            'product' => $product,
            'outcome' => $response['status'],
            'error' => json_encode($response),
        ]);

        return false;
    }

    /**
     * Usually this process is invoked automatically by s3->lambda direct event communication, but we can do it from here as well (in those cases when it's essential/the auto pipeline didn't work out)
     *
     * @param Entity\ProductMockup $productMockup
     * @return bool
     */
    public function initiateProductMockupThumbGen(Entity\ProductMockup $productMockup)
    {
        // ['Records'][0]['s3']['object']['key']

        $payload = [
            'Records' => [
                0 => [
                    's3' => [
                        'object' => [
                            'key' => $productMockup->getProduct()->getId() . '/' . $productMockup->getId() . '/' . $productMockup->getId() . 'jpg',
                        ]
                    ]
                ]
            ]
        ];

        $result = $this->lambdaClient->invoke([
            'FunctionName' => 'product_mockup_resize_v2',
            'Payload' => json_encode($payload),
        ]);

        $response = json_decode((string)$result->get('Payload'), true);

        if ($response['status'] == 200 && $response['message'] == 'ok') {
            $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('aws_lambda', 'product_mockup_resize_v2', [
                'product' => $productMockup->getProduct(),
                'outcome' => $response['status'],
            ]);

            return true;
        }

        $this->em->getRepository(Entity\ExternalApiCall::class)->logCall('aws_lambda', 'product_mockup_resize_v2', [
            'product' => $productMockup->getProduct(),
            'outcome' => $response['status'],
            'error' => json_encode($response),
        ]);

        return false;
    }
}