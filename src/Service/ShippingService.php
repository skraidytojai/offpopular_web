<?php

namespace App\Service;

use Braintree;
use App\Entity;
use Psr\Log\LoggerInterface;
use App\Service\PrintfulService;

class ShippingService
{
    const SHIPPING_TYPE_FREE = 'free';
    const SHIPPING_TYPE_PARTIALLY_FREE = 'partially free';
    const SHIPPING_TYPE_PAID = 'paid';
    const SHIPPING_TYPE_INFERRED = 'inferred'; // when we fail to communicate with printful api


    /** @var PrintfulService */
    protected $ps;

    public function __construct
    (
        PrintfulService $ps
    )
    {
        $this->ps = $ps;
    }

    /**
     * @param array $recipientInfo
     * @param Entity\ProductVariant[] $productVariants
     * @return array
     */
    public function calculateShippingPrice(array $recipientInfo, array $productVariants)
    {
        $productVariantsExcludedFromFreeShipping = [];
        $amountTowardsFreeShipping = 0;
        $totalAmount = 0;

        foreach ($productVariants as $productVariant) {
            $totalAmount += $productVariant->getPrice();
            if (true === $productVariant->getProductType()->getIsFreeShipExcluded()) {
                $productVariantsExcludedFromFreeShipping[] = $productVariant;
            } else {
                $amountTowardsFreeShipping += $productVariant->getPrice();
            }
        }

        if ($amountTowardsFreeShipping >= getenv('MIN_FREE_SHIPPING_AMOUNT') && !$productVariantsExcludedFromFreeShipping) {
            // totally free shipping: amount reached, and no items excluded from free shipping programme
            return [
                'price' => 0,
                'type' => self::SHIPPING_TYPE_FREE,
            ];
        } else if ($amountTowardsFreeShipping >= getenv('MIN_FREE_SHIPPING_AMOUNT') && $productVariantsExcludedFromFreeShipping) {
            // partially free shipping: amount reached, but we have some items excluded from free shipping programme. let's calculate shipping for them separately
            $productVariantsForShippingCalc = $productVariantsExcludedFromFreeShipping;
            $shippingType = self::SHIPPING_TYPE_PARTIALLY_FREE;
        } else {
            // paid shipping, because amount is not reached
            $productVariantsForShippingCalc = $productVariants;
            $shippingType = self::SHIPPING_TYPE_PAID;
        }

        try {
            $price = $this->ps->calculatePrintfulShippingPrice($recipientInfo, $productVariantsForShippingCalc, getenv('DISPLAY_CURRENCY'));
        } catch (\Exception $e) {
            $price = $shippingType == (self::SHIPPING_TYPE_PAID) ? ($totalAmount * 0.1) : ($shippingType == self::SHIPPING_TYPE_PARTIALLY_FREE ? $totalAmount * 0.05 : 0);
            $shippingType = self::SHIPPING_TYPE_INFERRED;
        }

        return [
            'type' => $shippingType,
            'price' => round($price, 2),
        ];
    }
}