<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class TextPageController extends Controller
{
    /** @var EntityManagerInterface */
    protected $em;

    /**
     * MainController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function viewAction($slug)
    {
        $textPageRepo = $this->em->getRepository('App:TextPage');
        $textPage = $textPageRepo->getPublishedBySlug($slug);

        if (!$textPage) {
            throw $this->createNotFoundException();
        }

        return $this->render('TextPage/view.html.twig', [
            'textPage' => $textPage,
        ]);
    }

    public function whatIsOffpopularAction(Request $request)
    {
        return $this->render('TextPage/whatIsOffpopular.html.twig', []);
    }
}