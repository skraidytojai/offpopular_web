<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use App\Entity;
use App\Service\CloudStorageService;
use Symfony\Component\Filesystem\Filesystem;
use App\Service\SearchService;

class AdminController extends Controller
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var CloudStorageService */
    protected $cs;

    /** @var SearchService */
    protected $searchService;


    public function __construct
    (
        EntityManagerInterface $em,
        CloudStorageService $cs,
        SearchService $searchService
    )
    {
        $this->em = $em;
        $this->cs = $cs;
        $this->searchService = $searchService;
    }


    public function productMassEditAction(Request $request)
    {
        $page = $request->get('page', 1);
        $limit = 50;
        $offset = ($page - 1) * $limit;

        $productRepo = $this->em->getRepository(Entity\Product::class);
        $products = $productRepo->findBy([], ['id' => 'ASC'], $limit, $offset);

        $jpgFileUrls = [];
        foreach ($products as $product) {
            $jpgFileUrls[$product->getId()] = $this->cs->getAwsJpgDesignFileSignedUrl($product, null, 'default', '1k');
        }

        $productType = $this->em->getRepository(Entity\ProductType::class)->find(1);

        return $this->render('Admin/productMassEdit.html.twig', [
            'products' => $products,
            'page' => $page,
            'productType' => $productType,
            'jpgFileUrls' => $jpgFileUrls,
        ]);
    }

    public function qualityControlAction(Request $request)
    {
        $productsBatch = $this->em->getRepository(Entity\Product::class)->getForQualityReview();

        $jpgFileUrls = [];
        foreach ($productsBatch as $product) {
            $jpgFileUrls[$product->getId()] = $this->cs->getAwsJpgDesignFileSignedUrl($product, null, 'default', '3k');
        }

        $randomProductMockups = [];
        foreach ($productsBatch as $product) {
            $randomProductMockups[$product->getId()] = $this->em->getRepository(Entity\ProductMockup::class)->getRandomForProduct($product);
        }

        foreach ($productsBatch as $product) {
            if (isset($jpgFileUrls[$product->getId()]) && $jpgFileUrls[$product->getId()]) {
                $product->qualityControlAccept(); // initial acceptance before showing it to the human reviewer
            } else {
                $product->qualityControlReject();
            }
        }

        $this->em->flush();

        return $this->render('Admin/qualityControl.html.twig', [
            'productsBatch' => $productsBatch,
            'jpgFileUrls' => $jpgFileUrls,
            'randomProductMockups' => $randomProductMockups,
        ]);
    }

    public function exportPotentialIndexedSearchesAction(Request $request)
    {
        // https://keywordseverywhere.com/ke/4/manual.php 2K at a time!

        $fs = new Filesystem();
        $fileDir = TEMP_DIRECTORY . '/indexed_searches';
        if ($fs->exists($fileDir)) {
            $fs->remove($fileDir);
        }
        $fs->mkdir($fileDir);

        $tagRepo = $this->em->getRepository(Entity\Tag::class);
        $productTypeRepo = $this->em->getRepository(Entity\ProductType::class);

        $tags = $tagRepo->getAll();
        $productTypes = $productTypeRepo->getAllPublished();

        $list = [];
        /** @var Entity\Tag $tag */
        foreach ($tags as $tag) {
            foreach ($productTypes as $productType) {
                $list[] = strtolower($tag->getTitle()) . ' ' . strtolower($productType->getTitleForSearch());
            }
        }

        $listChunked = array_chunk($list, 2000);

        $filePaths = [];
        foreach ($listChunked as $key => $list) {
            $filePath = $fileDir . "/searches_" . $key . ".csv";
            $file = fopen($filePath,"w");

            // put 'Keyword as header'
            //fputcsv($file, ['Keyword']);

            foreach ($list as $line) {
                fputcsv($file, [$line]);
            }

            fclose($file);

            $filePaths[] = $filePath;
        }

        $zipFilePath = TEMP_DIRECTORY . '/potential_indexed_searches.zip';
        $zip = new \ZipArchive();
        $zip->open($zipFilePath, \ZipArchive::CREATE);

        foreach ($filePaths as $filePath) {
            $zip->addFile($filePath);
        }

        $zip->close();

        return $this->file($zipFilePath);
    }

    public function importKeywodsEverywhereDataAction(Request $request)
    {
        ini_set('memory_limit','1024M');
        set_time_limit(1200);

        $fs = new Filesystem();
        $fileDir = TEMP_DIRECTORY . '/keywords_everywhere_import';

        $finder = new Finder();
        $finder->files()->in($fileDir);

        $csvPaths = [];
        foreach ($finder as $file) {
            $csvPaths[] = $file->getRealPath();
        }

        foreach ($csvPaths as $csvPath) {
            // $fp is file pointer to file sample.csv
            $n = 0;
            if (($fp = fopen($csvPath, "r")) !== FALSE) {
                while (($row = fgetcsv($fp, 1000, ",")) !== FALSE) {
                    $n++;
                    if ($n <= 1) {
                        continue; // skip header row
                    }

                    $keyword = $row[1];
                    $volume = (int) preg_replace('/[^0-9]+/', '', $row[2]);
                    $cpc = (float) preg_replace('/[^0-9.]+/', '', $row[3]);
                    $compScore = (float) $row[4];
                    $volumeGlobal = (int) preg_replace('/[^0-9]+/', '', $row[5]);
                    $cpcGlobal = (float) preg_replace('/[^0-9.]+/', '', $row[6]);
                    $compScoreGlobal = (float) $row[7];

                    $indexedSearch = (new Entity\IndexedSearch)
                        ->setKeyword($keyword)
                        ->setSearchVolume($volume)
                        ->setCpc($cpc)
                        ->setCompetitionScore($compScore)
                        ->setSearchVolumeGlobal($volumeGlobal)
                        ->setCpcGlobal($cpcGlobal)
                        ->setCompetitionScoreGlobal($compScoreGlobal);

                    $this->em->persist($indexedSearch);
                }
                fclose($fp);
            }

            $this->em->flush();
        }

        exit('ok');
    }

    public function addTagForSearchAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $tagTitle = $request->request->get('tag');
            $searchKeyword = $request->request->get('search_keyword');

            $tag = $this->em->getRepository(Entity\Tag::class)->getOrCreateByTitle($tagTitle);
            $productsPaginator = $this->searchService->searchProducts($searchKeyword, 30);

            while (true) {
                /** @var Entity\Product[] $products */
                $products = $productsPaginator->getCurrentPageResults();
                foreach ($products as $product) {
                    if (!$product->hasTag($tag)) {
                        $product->addTag($tag);
                    }
                }
                $this->em->flush();

                if (!$productsPaginator->hasNextPage()) {
                    break;
                }

                $productsPaginator->setCurrentPage($productsPaginator->getNextPage());
            }
        }

        return $this->render('Admin/addTagForSearch.html.twig');
    }
}