<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use App\Helper\HeroHelper;

class PostController extends Controller
{
    /** @var EntityManagerInterface */
    protected $em;

    /**
     * MainController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function allAction()
    {
        $repo = $this->em->getRepository('App:Post');
        $posts = $repo->getAllPublished();

        return $this->render('Post/all.html.twig', [
            'posts' => $posts,
        ]);
    }

    public function viewAction($slug)
    {
        $repo = $this->em->getRepository('App:Post');
        $post = $repo->getPublishedBySlug($slug);

        if (!$repo) {
            throw $this->createNotFoundException();
        }

        $heroProductShowcase = HeroHelper::getHeroProductShowcase();
        $productRepo = $this->em->getRepository('App:Product');
        $allProductsIndexed = $productRepo->getPublishedProducts(0, 100, true);

        return $this->render('Post/view.html.twig', [
            'post' => $post,
            'heroProductShowcase' => $heroProductShowcase,
            'indexedProducts' => $allProductsIndexed,
        ]);
    }
}