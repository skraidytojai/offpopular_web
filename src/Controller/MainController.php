<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\HumanFriendlyService;
use Symfony\Component\HttpFoundation\Request;
use App\Helper\CountryHelper;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\BraintreeService;
use App\Service\SearchService;
use App\Entity;
use App\Service\CacheService;

class MainController extends Controller
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var HumanFriendlyService */
    protected $humanFriendlyService;

    /** @var BraintreeService */
    protected $braintreeService;

    /** @var SearchService */
    protected $searchService;

    /** @var CacheService */
    protected $cacheService;


    public function __construct(
        EntityManagerInterface $em,
        HumanFriendlyService $humanFriendlyService,
        BraintreeService $braintreeService,
        SearchService $searchService,
        CacheService $cacheService
    )
    {
        $this->em = $em;
        $this->humanFriendlyService = $humanFriendlyService;
        $this->braintreeService = $braintreeService;
        $this->searchService = $searchService;
        $this->cacheService = $cacheService;
    }


    public function homeAction()
    {
        /*$productRepo = $this->em->getRepository('App:Product');
        $products = $productRepo->getPublishedProducts(0, 30);

        $productFeed = $productRepo->getPublishedProductResults(0, 60);*/

        $spotlight = [
            'https://d32ubx6bmpqnoi.cloudfront.net/1647/1552643/1552643_350px.jpg' => 'https://www.offpopular.com/design/summer-sunburst-on-red-yellow-blue-shapes-illustration.1647/dress-cut-sew',
            'https://d32ubx6bmpqnoi.cloudfront.net/828/1192425/1192425_350px.jpg' => 'https://www.offpopular.com/design/sexy-woman-with-red-lipstick-kissed-a-man-pop-art.828/towel',
            'https://d32ubx6bmpqnoi.cloudfront.net/687/1129491/1129491_350px.jpg' => 'https://www.offpopular.com/design/colorful-sphynx-cat-pop-art-style.687/canvas-stretched',
            'https://d32ubx6bmpqnoi.cloudfront.net/2025/1846172/1846172_350px.jpg' => 'https://www.offpopular.com/design/zombie-eyeballs-ornament.2025/basic-pillow',
            'https://d32ubx6bmpqnoi.cloudfront.net/1791/1661084/1661084_350px.jpg' => 'https://www.offpopular.com/design/cute-green-monster-showing-tongue.1791/backpack',
            'https://d32ubx6bmpqnoi.cloudfront.net/383/1136521/1136521_350px.jpg' => 'https://www.offpopular.com/design/skull-with-roses.383/iphone-case',
            'https://d32ubx6bmpqnoi.cloudfront.net/2141/1935995/1935995_350px.jpg' => 'https://www.offpopular.com/design/donut-worry-funny-quote.2141/framed-poster-premium-luster',
            'https://d32ubx6bmpqnoi.cloudfront.net/614/1096229/1096229_500px.jpg' => 'https://www.offpopular.com/design/everday-is-a-fresh-start-quote.614/black-foot-socks',
        ];

        return $this->render('Main/home.html.twig', [
            'spotlight' => $spotlight,
        ]);
    }

    public function viewProductAction(Request $request, $slug, $id, $productTypeSlug = null)
    {
        $legacyPillowMappings = [
            'square-pillow' => 'premium-pillow',
            'rectangular-pillow' => 'premium-pillow',
            'rectangular-pillow-case-only' => 'premium-pillow-case-only',
            'square-pillow-case-only' => 'premium-pillow-case-only',
        ];
        // Temporary. TODO: Remove after 2018-11-15.
        if (in_array($productTypeSlug, array_keys($legacyPillowMappings))) {
            return $this->redirectToRoute('view_product', [
                'slug' => $slug,
                'id' => $id,
                'productTypeSlug' => $legacyPillowMappings[$productTypeSlug],
            ], 301); // permanent redirect - so that search engines would know
        }

        $productRepo = $this->em->getRepository('App:Product');
        $product = $productRepo->find($id);
        $includeTestProduct = $request->get('test') ? true : false; // to show test option for $0.1

        if ($slug != $product->getSlug()) {
            return $this->redirectToRoute('view_product', [
                'slug' => $product->getSlug(),
                'id' => $id,
                'productTypeSlug' => $productTypeSlug,
            ], 301);
        }

        $productType = $this->em->getRepository('App:ProductType')->findOneBy(['slug' => $productTypeSlug]);

        if (!$productType || !$product) {
            throw $this->createNotFoundException();
        }

        $topLevelCategories = $this->em->getRepository('App:ProductTypeCategory')->getPublishedTopLevel();

        $maxMockupsPerVariant = 10; // sometimes mockups get duplicated for unknown reasons and we see even 20 of them - too much for the UI

        return $this->render('Main/viewProduct.html.twig', [
            'product' => $product,
            'productType' => $productType,
            'topLevelCategories' => $topLevelCategories,
            'maxMockupsPerVariant' => $maxMockupsPerVariant,
        ]);
    }

    public function viewProductOldAction(Request $request, $slug, $id)
    {
        $productRepo = $this->em->getRepository('App:Product');
        $product = $productRepo->find($id);
        $includeTestProduct = $request->get('test') ? true : false; // to show test option for $0.1

        $productTypes = $this->em->getRepository('App:ProductType')->getAllPublished();
        $productType = $productTypes[0];

        return $this->render('Main/viewProduct.html.twig', [
            'product' => $product,
            'productTypes' => $productTypes,
            'productType' => $productType,
        ]);
    }

    public function checkoutAction(Request $request)
    {
        $visitorCountry = CountryHelper::determineCountryCodeByIP($request->getClientIp());
        $allCountries = CountryHelper::getAllCountries();

        $isTestPayment = $request->get('test') ? true : false; // to skip adding shipping price in the final charge value, if you want to pay for $0.1 test product in braintree production mode (real payment)

        $braintreeClientToken = $this->braintreeService->getClientToken();
        $finalChargeCurrencyRate = $this->em->getRepository('App:CurrencyRate')->getLatestCurrencyRate(getenv('DISPLAY_CURRENCY'), getenv('FINAL_CHARGE_CURRENCY'));

        return $this->render('Main/checkout.html.twig', [
            'visitorCountry' => $visitorCountry,
            'allCountries' => $allCountries,
            'braintreeClientToken' => $braintreeClientToken,
            'isTestPayment' => $isTestPayment,
            'finalChargeCurrencyRate' => $finalChargeCurrencyRate,
        ]);
    }

    public function searchEmptyPageAction(Request $request)
    {
        // TODO: implement empty page - showing a search box again in the middle (when search string is empty, important for google)
    }

    public function searchAction(Request $request, string $searchString = '')
    {
        $page = $request->get('page', 1);
        $maxPerPage = 52;

        $decodedSearchString = urldecode($searchString);
        //$searchResult = $this->searchService->search($decodedSearchString, $maxPerPage, $page);

        $allProductTypes = $this->cacheService->getAllPublishedProductTypes();
        $allProductTypeCategories = $this->cacheService->getAllPublishedProductTypeCategoriesTopLevel();

        $productTypesPerProductTypeCategory = $this->cacheService->getProductTypesPerProductTypeCategory();
        $topLevelCategoriesPerProductType = $this->cacheService->getTopLevelCategoriesPerProductType();

        return $this->render('Main/search.html.twig', [
            //'searchResult' => $searchResult,
            'allProductTypes' => $allProductTypes,
            'allProductTypeCategories' => $allProductTypeCategories,
            'productTypesPerProductTypeCategory' => $productTypesPerProductTypeCategory,
            'maxPerPage' => $maxPerPage,
            'page' => $page,
            'topLevelCategoriesPerProductType' => $topLevelCategoriesPerProductType,
            'decodedSearchString' => $decodedSearchString,
        ]);
    }
}