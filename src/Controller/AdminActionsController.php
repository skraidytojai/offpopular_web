<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity;

class AdminActionsController extends AbstractApiController
{
    /** @var EntityManagerInterface */
    protected $em;

    /**
     * AdminController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function productJpgReprocessAction(Request $request, $id)
    {
        $productRepo = $this->em->getRepository(Entity\Product::class);
        $product = $productRepo->find($id);
        /** @var Entity\Product $product */
        $product
            ->setIsPublished(false)
            ->setJpgDesignProcessingStartedAt(null)
            ->setJpgDesignCreatedAt(null)
            ->setMockupGenInitStartedAt(null)
            ->setMockupGenInitFinishedAt(null);

        $this->em->flush();

        exit('Submitted for reprocessing');
    }

    public function productUpdateTitleAction(Request $request)
    {
        $requestContent = json_decode($request->getContent(), true);
        $id = $requestContent['id'];
        $title = $requestContent['title'];

        $productRepo = $this->em->getRepository(Entity\Product::class);
        $product = $productRepo->find($id);

        $product->setTitle($title);

        $this->em->flush();

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'message' => 'Title updated!',
        ]);
    }

    public function productDeleteAction(Request $request, $id)
    {
        $productRepo = $this->em->getRepository(Entity\Product::class);
        $product = $productRepo->find($id);
        /** @var Entity\Product $product */
        $product
            ->setIsPublished(false)
            ->setIsDeleted(true);

        $this->em->flush();

        exit('Deleted');
    }

    public function apiQualityControlProductAcceptAction(Request $request)
    {
        $requestContent = json_decode($request->getContent(), true);
        $productId = $requestContent['product_id'];
        $isExceptional = $requestContent['is_exceptional'];

        $productRepo = $this->em->getRepository(Entity\Product::class);
        $product = $productRepo->find($productId);
        /** @var Entity\Product $product */
        $product->qualityControlAccept($isExceptional);

        $this->em->flush();

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'message' => 'Accepted!',
        ]);
    }

    public function apiQualityControlProductRejectAction(Request $request)
    {
        $requestContent = json_decode($request->getContent(), true);
        $productId = $requestContent['product_id'];

        $productRepo = $this->em->getRepository(Entity\Product::class);
        $product = $productRepo->find($productId);
        /** @var Entity\Product $product */
        $product->qualityControlReject();

        $this->em->flush();

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'message' => 'Rejected!',
        ]);
    }
}