<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use JMS;
use App\Sitemap;
use App\Service\HumanFriendlyService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use App\Entity;

class SeoController extends Controller
{

    /** @var HumanFriendlyService */
    protected $humanFriendlyService;

    const SITEMAPS_DIR = PUBLIC_DIRECTORY . '/sitemaps';

    /**
     * SeoController constructor.
     * @param HumanFriendlyService $humanFriendlyService
     */
    public function __construct(HumanFriendlyService $humanFriendlyService)
    {
        $this->humanFriendlyService = $humanFriendlyService;
    }

    public function sitemapGenerateFilesAction()
    {
        $fs = new Filesystem();
        $em = $this->getDoctrine()->getManager();
        $maxUrlsPerSitemapFile = 50000; //limit by google

        if ($fs->exists(self::SITEMAPS_DIR)) {
            $fs->remove(self::SITEMAPS_DIR);
        }

        $fs->mkdir(self::SITEMAPS_DIR);

        /** MAIN */
        $links = [
            $this->generateUrl('index', [], UrlGeneratorInterface::ABSOLUTE_URL)
        ];

        $xmlContent = $this->generateXmlContent($links, 1);
        $fs->appendToFile(self::SITEMAPS_DIR . '/' . 'sitemap_main.xml', $xmlContent);


        /** EACH PRODUCT PAGE */
        $productsCount = $em->getRepository('App:Product')->getPublishedProductsCount();

        $productTypes = $em->getRepository('App:ProductType')->getAllPublished();
        $productTypesCount = count($productTypes);

        $productsPerSingleFile = floor($maxUrlsPerSitemapFile / $productTypesCount);

        $offset = 0;
        $i = 0;
        while (true) {
            $links = [];
            $products = $em->getRepository('App:Product')->getPublishedProducts($offset, $productsPerSingleFile);
            if (!$products) {
                break;
            }

            foreach ($products as $product) {
                foreach ($productTypes as $productType) {
                    $links[] = $this->generateUrl('view_product', ['slug' => $product->getSlug(), 'id' => $product->getId(), 'productTypeSlug' => $productType->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
                }
            }

            $xmlContent = $this->generateXmlContent($links);
            $fs->appendToFile(self::SITEMAPS_DIR . '/' . 'sitemap_products_' . $i . '.xml', $xmlContent);

            $offset += $productsPerSingleFile;
            $i++;
        }


        /** TEXT PAGES */
        $textPages = $em->getRepository('App:TextPage')->getAllPublished();
        foreach ($textPages as $textPage) {
            $links[] = $this->generateUrl('text_page_view', ['slug' => $textPage->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $xmlContent = $this->generateXmlContent($links);
        $fs->appendToFile(self::SITEMAPS_DIR . '/' . 'sitemap_text_pages.xml', $xmlContent);


        /** INDEXED SEACHES */

        $tagRepo = $em->getRepository(Entity\Tag::class);
        $productTypeRepo = $em->getRepository(Entity\ProductType::class);

        $tags = $tagRepo->findAll();
        $productTypes = $productTypeRepo->getAllPublished();

        $tagsPerPage = floor($maxUrlsPerSitemapFile / count($productTypes));
        $chunkedTags = array_chunk($tags, $tagsPerPage);

        $i = 0;
        foreach ($chunkedTags as $tags) {
            $links = [];
            /** @var Entity\Tag $tag */
            foreach ($tags as $tag) {
                foreach ($productTypes as $productType) {
                    $links[] = $this->generateUrl('search', ['searchString' => urlencode(strtolower($tag->getTitle()) . ' ' . strtolower($productType->getTitleForSearch()))], UrlGeneratorInterface::ABSOLUTE_URL);
                }
            }

            $xmlContent = $this->generateXmlContent($links);
            $fs->appendToFile(self::SITEMAPS_DIR . '/' . 'sitemap_search_links_' . $i . '.xml', $xmlContent);

            $i++;
        }

        exit('done!');
    }

    public function sitemapIndexAction()
    {
        $finder = new Finder();
        $finder->files()->in(self::SITEMAPS_DIR);

        $sitemaps = [];
        foreach ($finder as $file) {
            $sitemaps[] = new Sitemap\Sitemap(getenv('BASE_URL') . '/sitemaps/' . $file->getRelativePathname());
        }

        $sitemapIndex = new Sitemap\SitemapIndex($sitemaps);
        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
        $result = $serializer->serialize($sitemapIndex, 'xml');

        $response = new Response($result, 200, [
            'Content-type' => 'text/xml',
        ]);

        return $response;
    }

    /**
     * @param array $links
     * @param float $priority
     * @return string
     */
    private function generateXmlContent(array $links, $priority = 0.9)
    {
        $urls = [];
        foreach ($links as $link) {
            $urls[] = new Sitemap\Url($link, $priority);
        }

        $urlset = new Sitemap\Urlset($urls);
        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($urlset, 'xml');
    }

    public function sitemapOldAction()
    {
        $em = $this->getDoctrine()->getManager();

        $urls = [];
        $urls[] = new Sitemap\Url($this->generateUrl('index', [], UrlGeneratorInterface::ABSOLUTE_URL), 1);

        $links = [];

        // EACH PRODUCT PAGE
        $products = $em->getRepository('App:Product')->getPublishedProducts(0, 10000);
        foreach ($products as $product) {
            foreach ($this->humanFriendlyService->allProductTypes() as $key => $label) {
                $links[] = $this->generateUrl('view_product', ['slug' => $product->getSlug(), 'id' => $product->getId(), 'p' => $key], UrlGeneratorInterface::ABSOLUTE_URL);
            }

        }

        // TEXT PAGES
        $textPages = $em->getRepository('App:TextPage')->getAllPublished();
        foreach ($textPages as $textPage) {
            $links[] = $this->generateUrl('text_page_view', ['slug' => $textPage->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        // BLOG POSTS
        $posts = $em->getRepository('App:Post')->getAllPublished();
        $links[] = $this->generateUrl('post_all', [], UrlGeneratorInterface::ABSOLUTE_URL);
        foreach ($posts as $post) {
            $links[] = $this->generateUrl('post_view', ['slug' => $post->getSlug(), 'id' => $post->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        //// FINALIZE ALL LINKS
        foreach ($links as $link) {
            $urls[] = new Sitemap\Url($link, 0.8);
        }


        $urlset = new Sitemap\Urlset($urls);
        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
        $result = $serializer->serialize($urlset, 'xml');

        $response = new Response($result, 200, [
            'Content-type' => 'text/xml',
        ]);

        return $response;
    }
}
