<?php

namespace App\Controller;

use App\Helper\CountryHelper;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use App\Entity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\BraintreeService;
use App\Service\EmailService;

class OrderController extends AbstractApiController
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var UserManagerInterface */
    protected $um;

    /** @var BraintreeService */
    protected $braintreeService;

    /** @var EmailService */
    protected $emailService;

    public function __construct
    (
        EntityManagerInterface $em,
        UserManagerInterface $um,
        BraintreeService $braintreeService,
        EmailService $emailService
    )
    {
        $this->em = $em;
        $this->um = $um;
        $this->braintreeService = $braintreeService;
        $this->emailService = $emailService;
    }

    protected function calculateAndSetTotals(Entity\Order $order)
    {
        // TODO: move this fn to a service
        $order->calculateSubTotalAmount();

        $order->calculateTotalAmount();

        $finalChargeRate = $this->em->getRepository('App:CurrencyRate')->getLatestCurrencyRate($order->getCurrency(), getenv('FINAL_CHARGE_CURRENCY'));
        $order->updateFinalChargeInformation(getenv('FINAL_CHARGE_CURRENCY'), $finalChargeRate->getRate());

        return $order;
    }

    public function placeOrderAction(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $checkoutForm = $body['checkoutForm'];
        $cart = $body['cart'];
        $isTestPayment = $body['isTestPayment'];

        $shippingApiResponse = $body['shippingApiResponse'];
        $shippingPrice = @$shippingApiResponse['price'];
        $shippingPrice = $shippingPrice ?: 0;
        if ($isTestPayment) {
            $shippingPrice = 0;
        }

        // Do some form field checking:
        foreach ($checkoutForm as $field => $value) {
            if (!$value && $field != 'state') { // hardcode to exclude state field from checking if empty/not
                return new JsonResponse([
                    'status' => 'error',
                    'message' => 'check_form_fields',
                ]);
            }
        }

        if (false === filter_var($checkoutForm['email'], FILTER_VALIDATE_EMAIL)) {
            return new JsonResponse([
                'status' => 'error',
                'message' => 'email_invalid',
            ]);
        }

        $buyerEmail = $checkoutForm['email'];
        $buyerFirstName = $checkoutForm['firstName'];
        $buyerLastName = $checkoutForm['lastName'];
        $buyerPhone = $checkoutForm['phone'];
        $countryCodeTwoLetter = $checkoutForm['country'];
        $country = CountryHelper::getCountryNameByTwoLetterCode($countryCodeTwoLetter);
        $state = $checkoutForm['state'];

        $user = $this->em->getRepository('App:User')->getOrCreateUser($buyerEmail, $buyerFirstName, $buyerLastName);
        $order = (new Entity\Order)
            ->setUser($user)
            ->setShippingName($buyerFirstName . ' ' . $buyerLastName)
            ->setShippingAddress($checkoutForm['address'])
            ->setShippingCity($checkoutForm['city'])
            ->setShippingPostCode($checkoutForm['postCode'])
            ->setShippingCountryTwoLetter($countryCodeTwoLetter)
            ->setShippingCountry($country)
            ->setShippingState($state)
            ->setShippingType(Entity\Order::SHIPPING_TYPE_COURIER)
            ->setCurrency('USD')
            ->setBuyerEmail($buyerEmail)
            ->setBuyerFirstName($buyerFirstName)
            ->setBuyerLastName($buyerLastName)
            ->setBuyerPhone($buyerPhone)
            ->setIsTest($isTestPayment)
            ->setShippingAmount($shippingPrice);

        $this->em->persist($order);

        foreach ($cart['cartItems'] as $cartItem) {
            $product = $this->em->getRepository('App:Product')->find($cartItem['productObj']['id']);
            $productVariant = $this->em->getRepository('App:ProductVariant')->find($cartItem['productVariant']['id']);

            $order->addOrderItem($this->em->getRepository('App:OrderItem')->create($product, $productVariant));
        }

        $this->calculateAndSetTotals($order);

        $this->em->flush();

        return new JsonResponse([
            'status' => 'ok',
            'order_id' => $order->getId(),
        ]);
    }

    public function receiveBraintreePaymentMethodNonceAction(Request $request)
    {
        $body = json_decode($request->getContent(), true);

        $nonce = $body['nonce'];
        $orderId = $body['orderId'];

        $order = $this->em->getRepository(Entity\Order::class)->getOrderForPayment($orderId);

        if (!$order) {
            return new JsonResponse([
                'status' => 'error',
            ]);
        }

        $status = $this->braintreeService->initiateTransaction($order, $nonce);

        if ($status == BraintreeService::STATUS_SUCCESS) {
            $order->confirmOrder();

            $this->em->flush();

            $this->emailService->sendPurchaseReceipt($order);

            $redirectUrl = $this->generateUrl('payment_accept', [
                'orderId' => $order->getId()
            ], UrlGeneratorInterface::ABSOLUTE_URL);

            return new JsonResponse([
                'status' => 'ok',
                'redirect_url' => $redirectUrl,
            ]);
        }

        // If everything above fails:
        return new JsonResponse([
            'status' => 'error',
        ]);
    }
}