<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity;
use WebToPay, WebToPayException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\EmailService;
use Symfony\Component\HttpFoundation\Response;

class PaymentController extends Controller
{

    /** @var EmailService */
    protected $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }


    protected function em()
    {
        return $this->getDoctrine()->getManager();
    }

    public function acceptAction($orderId)
    {
        $orderRepo = $this->getDoctrine()->getManager()->getRepository('App:Order');
        $order = $orderRepo->find($orderId);

        if (!$order) {
            exit('Error. Please contact admins.');
        }

        return $this->render('./Payment/accept.html.twig', [
            'order' => $order,
        ]);
    }

    public function cancelAction()
    {
        return $this->render('./Payment/cancel.html.twig');
    }
}
