<?php

namespace App\Controller;

use App\DataObject\ProductResult;
use App\Service\EmailService;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\ShippingService;
use App\Service\SearchService;
use App\Service\FastMemoryService;

class InternalApiController extends AbstractApiController
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var ShippingService */
    protected $ss;

    /** @var SearchService */
    protected $searchService;

    /** @var EmailService */
    protected $emailService;

    /** @var FastMemoryService */
    protected $fms;

    public function __construct
    (
        EntityManagerInterface $em,
        ShippingService $ss,
        SearchService $searchService,
        EmailService $emailService,
        FastMemoryService $fms
    )
    {
        $this->em = $em;
        $this->ss = $ss;
        $this->searchService = $searchService;
        $this->emailService = $emailService;
        $this->fms = $fms;
    }

    public function getProductFeedAction(Request $request)
    {
        $offset = $request->get('offset');
        $limit = $request->get('limit');

        $productRepo = $this->em->getRepository('App:Product');
        $productFeed = $productRepo->getPublishedProductResults($offset, $limit, true);

        foreach ($productFeed as $productResult) {
            $productResult->url = $this->generateUrl('view_product', [
                'slug' => $productResult->product->getSlug(),
                'id' => $productResult->product->getId(),
                'productTypeSlug' => $productResult->productType->getSlug(),
            ], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $productFeed = $this->fms->populateProductResultArrayWithMockupIds($productFeed);

        //return $this->render('Test/test.html.twig', []);

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'list' => $productFeed,
        ]);
    }

    public function getSimilarProductsFeedAction(Request $request)
    {
        $productRepo = $this->getDoctrine()->getManager()->getRepository(Entity\Product::class);

        $productTypeId = $request->get('product_type_id');
        $productType = $this->em->getRepository('App:ProductType')->find($productTypeId);

        if (!$productType) {
            throw $this->createNotFoundException();
        }

        $products = $productRepo->getRandomPublishedProducts(12);

        $productResults = [];
        foreach ($products as $product) {
            $productResults[$product->getId()] = new ProductResult($product, $productType);
        }

        foreach ($productResults as $productResult) {
            $productResult->url = $this->generateUrl('view_product', [
                'slug' => $productResult->product->getSlug(),
                'id' => $productResult->product->getId(),
                'productTypeSlug' => $productResult->productType->getSlug(),
            ], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $this->fms->populateProductResultArrayWithMockupIds($productResults);

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'list' => array_values($productResults),
        ]);
    }

    public function calculateShippingPriceAction(Request $request)
    {
        $body = json_decode($request->getContent(), true);

        $recipientInfo = $body['recipientInfo'];
        $cartItems = $body['cartItems'];

        /*if (getenv('APP_ENV') == 'dev') {
            $recipientInfo['country'] = 'LT';
        }*/

        $repo = $this->em->getRepository(Entity\ProductVariant::class);
        $productVariants = [];
        foreach ($cartItems as $cartItem) {
            $productVariants[] = $repo->find($cartItem['productVariant']['id']);
        }

        $result = $this->ss->calculateShippingPrice($recipientInfo, $productVariants);

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'price' => $result['price'],
            'type' => $result['type'],
            'currency' => getenv('DISPLAY_CURRENCY'),
        ]);
    }

    public function submitExclusiveBuyoutFormAction(Request $request)
    {
        // name, email, product, productType,selected Product Variant
        $body = json_decode($request->getContent(), true);
        $form = $body['form'];
        $productId = $body['productId'];
        $productTypeId = $body['productTypeId'];
        $productVariantId = $body['productVariantId'];

        // Do some form field checking:

        if (false === filter_var($form['email'], FILTER_VALIDATE_EMAIL)) {
            return $this->serializeAndMakeJsonResponse([
                'status' => 'error',
                'message' => 'email_invalid',
            ]);
        }

        $this->emailService->sendExclusiveBuyoutInquiry([
            'form' => $form,
            'productId' => $productId,
            'productTypeId' => $productTypeId,
            'productVariantId' => $productVariantId,
        ]);

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'message' => 'Inquiry received',
        ]);
    }

    public function getRelatedProductTypesProductResultsAction(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $productId = $body['productId'] ?? 0;

        $product = $this->em->getRepository(Entity\Product::class)->find($productId);

        if (!$product) {
            return $this->serializeAndMakeJsonResponse([
                'status' => 'error',
                'message' => 'Product not found',
            ]);
        }

        // related = same product, different product types
        $relatedProducts = $this->em->getRepository('App:Product')->getRelatedProductTypeProductResults($product);

        foreach ($relatedProducts as $productResult) {
            $productResult->url = $this->generateUrl('view_product', [
                'slug' => $productResult->product->getSlug(),
                'id' => $productResult->product->getId(),
                'productTypeSlug' => $productResult->productType->getSlug(),
            ], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $this->fms->populateProductResultArrayWithMockupIds($relatedProducts);

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'list' => $relatedProducts,
        ]);
    }

    public function searchAction(Request $request)
    {
        $page = $request->get('page');
        $maxPerPage = $request->get('maxPerPage');
        $searchString = $request->get('searchString');

        $decodedSearchString = urldecode($searchString);
        $searchResult = $this->searchService->search($decodedSearchString, $maxPerPage, $page);

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'search_result' => $searchResult,
        ]);
    }

    public function subscribeNewsletterAction(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $email = $body['email'];
        $firstName = $body['first_name'];
        $source = $body['source'];
        $gameNo = $body['game_no'];
        $gameRoute = $body['game_route'];
        $ticketCount = $body['ticket_count'] ?? 1;

        $invitersEmail = $body['inviters_email'] ?? null;
        $invitersFirstName = $body['inviters_first_name'] ?? null;

        if (false === filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->serializeAndMakeJsonResponse([
                'status' => 'error',
                'message' => 'email_invalid',
            ]);
        }

        $originalEmail = $invitersEmail ?? $email;
        $originalFirstName = $invitersFirstName ?? $firstName;

        $originalUser = $this->em->getRepository(Entity\User::class)->getOrCreateUser($originalEmail, $originalFirstName);
        $originalUser->setSource($source);
        $this->em->flush();

        // EMAIL SENDING (if needed)
        switch ($source) {
            case Entity\User::SOURCE_PILLOW_CLUB:
                $this->emailService->sendParticipateInGameConfirmation($gameRoute, $originalUser);
                break;
            case Entity\User::SOURCE_PILLOW_CLUB_FRIEND_INVITATION:
                $invitedUser = $this->em->getRepository(Entity\User::class)->getOrCreateUser($email, $firstName);
                $this->emailService->sendFriendsInvitationToContest($gameRoute, $originalUser, $invitedUser);
                // use inviters email and first name as well
                break;
            default:
        }

        $this->em->getRepository(Entity\GameParticipant::class)->userParticipate($originalUser, $gameNo, $source, $ticketCount);

        $ticketCount = $this->em->getRepository(Entity\GameParticipant::class)->getTicketCount($originalUser, $gameNo);

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'ticket_count' => $ticketCount,
        ]);
    }
}