<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\HumanFriendlyService;
use Symfony\Component\HttpFoundation\Request;
use App\Helper\CountryHelper;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\BraintreeService;
use App\Service\SearchService;
use App\Entity;
use App\Service\CacheService;

class LandingController extends Controller
{
    /** @var EntityManagerInterface */
    protected $em;


    public function __construct
    (
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
    }

    public function pillowCoverClubAction(Request $request)
    {
        $products = $this->em->getRepository(Entity\Product::class)->getByGroup('pillow-cover-club', 0, 200);

        /** @var Entity\ProductType $productType */
        $productType = $this->em->getRepository(Entity\ProductType::class)->findOneBy(['slug' => 'inspirational-pillow-cover']);
        $productVariant = $productType->getProductVariants()->first();

        $allProductsIndexedById = [];
        foreach ($products as $product) {
            $allProductsIndexedById[$product->getId()] = $product;
        }

        $sources = [
            'giveaway' => Entity\User::SOURCE_PILLOW_CLUB,
            'invite_friend' => Entity\User::SOURCE_PILLOW_CLUB_FRIEND_INVITATION,
            'social_share' => Entity\User::SOURCE_PILLOW_CLUB_SOCIAL,
        ];

        $gameNo = 1;
        $socialShareUrl = $this->generateUrl('landing_pillow_cover_club', [], UrlGeneratorInterface::ABSOLUTE_URL);

        $email = $request->get('email', '');
        $firstName = $request->get('first_name', '');

        $ticketCount = 1;
        if ($email) {
            $user = $this->em->getRepository(Entity\User::class)->getOrCreateUser($email, $firstName);
            $ticketCount = $this->em->getRepository(Entity\GameParticipant::class)->getTicketCount($user, $gameNo);
        }

        return $this->render('Landing/pillowCoverClub.html.twig', [
            'products' => $products,
            'productType' => $productType,
            'productVariant' => $productVariant,
            'allProductsIndexedById' => $allProductsIndexedById,
            'sources' => $sources,
            'gameNo' => $gameNo,
            'socialShareUrl' => $socialShareUrl,
            'email' => $email,
            'firstName' => $firstName,
            'ticketCount' => $ticketCount,
        ]);
    }
}