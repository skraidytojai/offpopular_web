<?php

namespace App\Controller;

use App\Service\SearchService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS;
use App\Service\PrintfulService;
use App\Entity;
use Symfony\Component\HttpFoundation\Request;
use Aws\Lambda\LambdaClient;
use App\Helper\MockupGenerationHelper;
use App\Service\CloudStorageService;
use App\Service\ShippingService;
use App\Service\CacheService;

class TestController extends Controller
{
    /** @var PrintfulService */
    protected $printful;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var CloudStorageService */
    protected $cs;

    /** @var ShippingService */
    protected $ss;

    /** @var SearchService */
    protected $search;

    /** @var CacheService */
    protected $cacheService;

    public function __construct
    (
        PrintfulService $printful,
        EntityManagerInterface $em,
        CloudStorageService $cs,
        ShippingService $ss,
        SearchService $search,
        CacheService $cacheService
    )
    {
        $this->printful = $printful;
        $this->em = $em;
        $this->cs = $cs;
        $this->ss = $ss;
        $this->search = $search;
        $this->cacheService = $cacheService;
    }

    public function testRemoteIpAction(Request $request)
    {
        echo $request->getClientIp();

        die;
    }

    public function testPrintfulAction()
    {
        // die('disabled');

        $products = $this->printful->getAllProductsInfo();
        echo json_encode($products);

        die;
    }

    public function testRetrieveResultAction($id)
    {
        die('disabled');

        //$genTask = $repo->find($id);

        //$this->printful->retrieveMockupGenTaskResult($genTask);
    }

    public function dumpTagsAction()
    {
        $repo = $this->getDoctrine()->getManager()->getRepository(Entity\Tag::class);

        $tags = $repo->findAll();

        $chunkedTags = array_chunk($tags, 800);

        foreach ($chunkedTags as $chunk) {
            echo "========<br><br>";
            foreach ($chunk as $tag) {
                echo $tag . '<br>';
            }
        }

        die;
    }

    public function importTagGoogleDataAction($file)
    {
        $filename = realpath(PUBLIC_DIRECTORY . '/../data/tag_google_search_data/' . $file . '.csv');

        $delimiter=',';

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        unset($data[0]);

        foreach ($data as $row) {
            list($tagTitle, $searchVolume) = explode(';', array_values($row)[0]);

            $q = $this->em->createQueryBuilder()
                ->update('App:Tag', 't')
                ->set('t.monthlyGoogleSearch', $searchVolume)
                ->where('t.title = :title')
                ->setParameter('title', $tagTitle)
                ->getQuery();

            $q->execute();
        }

        exit('done');
    }

    public function testShippingPriceCalcAction(Request $request)
    {
        //$productVariantIds = [1, 15, 15, 211];
        $productVariantIds = [1, 15];
        $recipientInfo = [
            'country' => 'AU',
            //'state_code' => 'AK',
        ];

        $productVariants = [];
        $repo = $this->getDoctrine()->getManager()->getRepository(Entity\ProductVariant::class);
        foreach ($productVariantIds as $productVariantId) {
            $productVariants[] = $repo->find($productVariantId);
        }

        $price = $this->ss->calculateShippingPrice($recipientInfo, $productVariants);

        dump($price);
        die;
    }

    public function testSearchAction()
    {
        $a = $this->cacheService->getProductTypesPerProductTypeCategory();
        dump($a);
        die;

        $topLevelCategory = $this->em->getRepository(Entity\ProductTypeCategory::class)->find(8);
        return $this->renderView('Test/test.html.twig', [
            'category' => $topLevelCategory,
        ]);

        $a = $this->cacheService->getPublishedProductTypeSearchTitles();
        dump($a);
        die;

        $string = 'tote bag';
        dump($string);
        $a = $this->search->determineProductType($string);
        dump($a);
        die;

        /*
        dump($this->search->searchProductTypes('skirt'));
        dump($this->search->searchProductTypes('halloween party skirt'));
        dump($this->search->searchProductTypes('mini skirt'));
        dump($this->search->searchProductTypes('halloween party mini skirt'));
        */
/*
        dump($this->search->searchProducts('skirt'));
        dump($this->search->searchProducts('halloween party skirt'));
        dump($this->search->searchProducts('mini skirt'));
        dump($this->search->searchProducts('halloween party mini skirt'));
        dump($this->search->searchProducts('helloween'));
        dump($this->search->searchProducts('cherry tomas pencil skirt'));*/

        //dump($this->search->searchProducts('floral')->setCurrentPage(1)->getCurrentPageResults());
        //dump($this->search->searchProducts('floral')->setCurrentPage(2)->getCurrentPageResults());
        die;
    }

    public function addCustomProductsAction(Request $request, int $quantity)
    {
        $title = 'Inspirational Pillow';
        $group = 'pillow-cover-club';

        for ($n = 1; $n <= $quantity; $n++) {
            $product = (new Entity\Product)
                ->setTitle($title . ' ' . $n)
                ->setGroup($group)
                ->setIsPublished(false)
                ->setSource(Entity\Product::SOURCE_EBAY)
                ->setIdAtSource($n);

            $this->em->persist($product);
        }

        $this->em->flush();

        exit('done');
    }
}
