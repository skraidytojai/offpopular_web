<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;

class AbstractApiController extends Controller
{
    protected function serializeAndMakeJsonResponse($response, $groups = [])
    {
        $serializer = $this->get('jms_serializer');
        $json = $groups ? $serializer->serialize($response, 'json', SerializationContext::create()->setGroups($groups)) : $serializer->serialize($response, 'json');

        return new JsonResponse($json, 200, [], true);
    }
}