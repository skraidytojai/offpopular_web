<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use App\Entity;

class FreepikController extends AbstractApiController
{
    /** @var EntityManagerInterface */
    protected $em;

    public function __construct
    (
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
    }

    public function makeProductFromFreepikPageAction(Request $request)
    {
        $requestContent = json_decode($request->getContent(), true);
        $designTitle = $requestContent['title'];

        $designTagsRaw = $requestContent['tags_raw_html'];
        $crawler = new Crawler($designTagsRaw);

        $tagTitles = $crawler->filter('a')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        $product = $this->em->getRepository(Entity\Product::class)->addScrapedProduct($designTitle, $tagTitles, [
            'author_name' => $requestContent['author_name'],
            'author_url' => $requestContent['author_url'],
            'design_has_text' => $requestContent['design_has_text'],
            'design_is_single_image' => $requestContent['design_is_single_image'],
            'id_at_source' => $requestContent['id_at_source'],
        ]);

        if (!$product) {
            return $this->serializeAndMakeJsonResponse([
                'status' => 'error',
                'message' => 'ERROR: Product already exists.',
            ]);
        }

        return $this->serializeAndMakeJsonResponse([
            'status' => 'ok',
            'message' => $product->getId() . ' [ID] SUCCESS. No of tags added: ' . $product->getTags()->count(),
        ]);
    }
}