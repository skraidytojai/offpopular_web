<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;

class CreateThumbnailsSecondFlowCommand extends Command
{
    use LockableTrait;

    protected $em;

    public function __construct
    (
        EntityManagerInterface $em
    ){
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:thumbnails_second_flow:create');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        set_time_limit(360);

        $output->writeln('Starting!');

        $mockups = $this->em->getRepository(Entity\ProductMockup::class)->getWithoutThumbnailsSecondFlow();
        $fs = new Filesystem();

        foreach ($mockups as $mockup) {
            $fullJpgPath = PUBLIC_DIRECTORY . $mockup->getServerPicturePath();
            $directory = dirname($fullJpgPath);

            if (!$mockup->getThumb50pxCreatedAt()) {
                $thumbPathJpg = "$directory/{$mockup->getId()}_50px_80.jpg";
                exec("convert $fullJpgPath -resize 50x -quality 80 $thumbPathJpg");

                $thumbPathWebp = "$directory/{$mockup->getId()}_50px_80.webp";
                exec("convert $fullJpgPath -resize 50x -quality 80 $thumbPathWebp");

                if ($fs->exists($thumbPathJpg) && $fs->exists($thumbPathWebp)) {
                    $mockup->setThumb50pxCreatedAt(new \DateTime);
                } else {
                    $mockup->setThumb50pxFailedAt(new \DateTime);
                }
            }

            if (!$mockup->getThumb100pxCreatedAt()) {
                $thumbPathJpg = "$directory/{$mockup->getId()}_100px_80.jpg";
                exec("convert $fullJpgPath -resize 100x -quality 80 $thumbPathJpg");

                $thumbPathWebp = "$directory/{$mockup->getId()}_100px_80.webp";
                exec("convert $fullJpgPath -resize 100x -quality 80 $thumbPathWebp");

                if ($fs->exists($thumbPathJpg) && $fs->exists($thumbPathWebp)) {
                    $mockup->setThumb100pxCreatedAt(new \DateTime);
                } else {
                    $mockup->setThumb100pxFailedAt(new \DateTime);
                }
            }

            if (!$mockup->getThumb500pxCreatedAt()) {
                $thumbPathJpg = "$directory/{$mockup->getId()}_500px_80.jpg";
                exec("convert $fullJpgPath -resize 500x -quality 80 $thumbPathJpg");

                $thumbPathWebp = "$directory/{$mockup->getId()}_500px_80.webp";
                exec("convert $fullJpgPath -resize 500x -quality 80 $thumbPathWebp");

                if ($fs->exists($thumbPathJpg) && $fs->exists($thumbPathWebp)) {
                    $mockup->setThumb500pxCreatedAt(new \DateTime);
                } else {
                    $mockup->setThumb500pxFailedAt(new \DateTime);
                }
            }
        }

        $this->em->flush();

        $output->writeln('Finished!');

        $this->release();

        return 0;
    }
}