<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;

/**
 * Class RetrieveGeneratedMockupsCommand
 * @package App\Command
 *
 * @deprecated
 */
class RetrieveGeneratedMockupsCommand extends Command
{
    use LockableTrait;

    protected $em;

    /** @var PrintfulService */
    protected $printful;

    /** @var Entity\PrintfulVariant[]|array */
    protected $loadedPrintfulVariants = []; // indexed by printfulVariantId, not id

    public function __construct
    (
        EntityManagerInterface $em,
        PrintfulService $printful
    ){
        $this->em = $em;
        $this->printful = $printful;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:mockups:retrieve');
    }

    protected function loadPrintfulVariantsByIds(array $ids)
    {
        $idsToLoad = [];
        foreach ($ids as $id) {
            if (!isset($this->loadedPrintfulVariants[$id])) {
                $idsToLoad[] = $id;
            }
        }

        if (empty($idsToLoad)) {
            return;
        }

        $printfulVariants = $this->em->getRepository(Entity\PrintfulVariant::class)->findBy(['printfulVariantId' => $idsToLoad]);

        foreach ($printfulVariants as $printfulVariant) {
            $this->loadedPrintfulVariants[$printfulVariant->getPrintfulVariantId()] = $printfulVariant;
        }
    }

    /**
     * @param array $ids
     * @return Entity\ProductVariant[]
     */
    protected function getProductVariantsByPrintfulVariantIds(array $ids)
    {
        $this->loadPrintfulVariantsByIds($ids);
        $productVariants = [];

        foreach ($ids as $id) {
            $printfulVariant = $this->loadedPrintfulVariants[$id];
            if (!$printfulVariant) {
                continue; // TODO: log missing PrintfulVariant in our DB - might be because sth new appeared on Printful system, and we're not synced yet
            }

            foreach ($printfulVariant->getProductVariants() as $productVariant) {
                $productVariants[$productVariant->getId()] = $productVariant;
            }
        }

        return $productVariants;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(3600);

        $output->writeln('Starting!');

        // 1. take uncompleted tasks
        for ($i = 0; $i < 6; $i++) {
            $tasks = $this->em->getRepository(Entity\PrintfulMockupGenTask::class)->getUncompleted(0, 1);
            if (!$tasks) {
                break;
            }

            $task = $tasks[0];

            // 2. retrieve printful result for each task. if completed - download the pics into according dir, by ProductType->id
            $output->writeln('Working with task id: ' . $task->getId());

            $result = $this->printful->retrieveMockupGenTaskResult($task);
            if ($result->isFailed()) {
                $task->setStatus(Entity\PrintfulMockupGenTask::STATUS_FAILED);
                $task->setError($result->error);

                $this->em->flush();
                continue;
            }

            if (true !== $result->isCompleted()) {

                sleep(5);
                continue; // Will try again later
            }

            $task->setStatus(Entity\PrintfulMockupGenTask::STATUS_PROCESSING); // prevent other scripts from taking this task
            $this->em->flush();

            $createdMockupEntities = [];
            foreach ($result->mockupList->mockups as $mockupItem) {
                $printfulVariantIds = $mockupItem->variantIds;
                $placementFile = $mockupItem->placement;

                $productMockup = (new Entity\ProductMockup())
                    ->setDefaultProductType($task->getProductType())
                    ->setProduct($task->getProduct())
                    ->setPrintfulPictureUrl($mockupItem->mockupUrl)
                    ->setPlacement($placementFile)
                    ->setType(Entity\ProductMockup::TYPE_MAIN);

                $this->em->persist($productMockup);
                foreach ($this->getProductVariantsByPrintfulVariantIds($printfulVariantIds) as $productVariant) {
                    $productVariant->addProductMockup($productMockup);
                }

                $createdMockupEntities[] = $productMockup;

                // EXTRA MOCKUPS:
                foreach ($mockupItem->extraMockups as $extraMockupItem) {
                    $productMockup = (new Entity\ProductMockup())
                        ->setDefaultProductType($task->getProductType())
                        ->setProduct($task->getProduct())
                        ->setPrintfulPictureUrl($extraMockupItem->url)
                        ->setPlacement($placementFile)
                        ->setTitle($extraMockupItem->title)
                        ->setType(Entity\ProductMockup::TYPE_EXTRA);

                    $this->em->persist($productMockup);
                    foreach ($this->getProductVariantsByPrintfulVariantIds($printfulVariantIds) as $productVariant) {
                        $productVariant->addProductMockup($productMockup);
                    }

                    $createdMockupEntities[] = $productMockup;
                }

                $this->em->flush();
            }

            $directory = PUBLIC_DIRECTORY . '/uploads/images/product/' . $task->getProduct()->getId() . '/mockups_by_id';
            $fileSystem = new Filesystem();
            if (!$fileSystem->exists($directory)) {
                $fileSystem->mkdir($directory);
            }

            /** @var Entity\ProductMockup $createdMockupEntity */
            foreach ($createdMockupEntities as $createdMockupEntity) {
                file_put_contents($directory . '/' . $createdMockupEntity->getId() . '.jpg', fopen($createdMockupEntity->getPrintfulPictureUrl(), 'r'));
                $createdMockupEntity->setServerPicturePath('/uploads/images/product/' . $task->getProduct()->getId() . '/mockups_by_id/' . $createdMockupEntity->getId() . '.jpg');

                $this->em->flush();
            }

            // 3. update task fields, mark it as completed

            $task
                ->setStatus(Entity\PrintfulMockupGenTask::STATUS_COMPLETED)
                ->setMockups(serialize($result->mockupList->mockups));

            $this->em->flush();
        }

        $output->writeln('Finished!');

        return 0;
    }
}