<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;

/**
 * Class InitiateFailedMockupGenerationCommand
 * @package App\Command
 *
 * @deprecated
 */
class InitiateFailedMockupGenerationCommand extends Command
{
    use LockableTrait;

    protected $em;

    /** @var PrintfulService */
    protected $printful;

    public function __construct
    (
        EntityManagerInterface $em,
        PrintfulService $printful
    ){
        $this->em = $em;
        $this->printful = $printful;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:mockups:reinitiate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        set_time_limit(3600);

        $output->writeln('Starting!');

        // 1. Get failed tasks

        $tasks = $this->em->getRepository(Entity\PrintfulMockupGenTask::class)->getFailedForReinitiation(0, 1);

        foreach ($tasks as $task) {
            $output->writeln('Working with TASK ID: ' . $task->getId());

            $this->printful->initiateMockupGenTask($task->getProductType(), $task->getProduct(), true);

            $task->setReinitiated(true);
            $this->em->flush();

            sleep(18); // spreading out requests to printful api evenly
        }

        $output->writeln('Finished!');

        $this->release();

        return 0;
    }
}