<?php

namespace App\Command\Cache;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;
use App\Service\FastMemoryService;

class CacheProductMockupsCommand extends Command
{
    use LockableTrait;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var FastMemoryService */
    protected $fms;

    public function __construct
    (
        EntityManagerInterface $em,
        FastMemoryService $fms
    ){
        $this->em = $em;
        $this->fms = $fms;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:cache:product_mockups');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        ini_set('memory_limit','512M');

        $output->writeln('Starting!');

        $products = $this->em->getRepository(Entity\Product::class)->getForMockupCaching(70);

        foreach ($products as $product) {
            $output->writeln('Working with PRODUCT ID: ' . $product->getId());

            $mockupPerProductType = [];
            $mockups = $product->getMainProductMockupsByVariants();

            if ($mockups) {
                foreach ($mockups as $productVariantId => $mockup) {
                    if (in_array($productVariantId, [246, 248, 250, 252])) {
                        continue; // "dont take rectangular pillow" hard fix - we want squared ones instead
                    }

                    if (!isset($mockupPerProductType[$mockup->getDefaultProductType()->getId()])) {
                        $mockupPerProductType[$mockup->getDefaultProductType()->getId()] = $mockup->getId();
                    }
                }

                $this->fms->cacheMockups($product->getId(), $mockupPerProductType);
            }

            $product->setLastMockupCacheAt(new \DateTime);
        }

        $this->em->flush();

        $output->writeln('Finished!');

        $this->release();

        return 0;
    }
}