<?php

namespace App\Command\Cache;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;
use App\Service\CacheService;

class AppCacheUpdateCommand extends Command
{
    use LockableTrait;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var CacheService */
    protected $cacheService;

    public function __construct
    (
        EntityManagerInterface $em,
        CacheService $cacheService
    ){
        $this->em = $em;
        $this->cacheService = $cacheService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:app_cache:update');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        set_time_limit(360);

        $output->writeln('Starting!');

        $this->cacheService->getAllPublishedProductTypes(true);
        $this->cacheService->getAllPublishedProductTypeCategoriesTopLevel(true);
        $this->cacheService->getPublishedProductTypeSearchTitles(true);
        $this->cacheService->getPublishedProductTypeCategorySearchTitles(true);

        $this->cacheService->getProductTypesPerProductTypeCategory(true);

        $this->cacheService->getPublishedProductsCount(true);
        $this->cacheService->getPublishedProductTypesCount(true);

        $output->writeln('Finished!');

        $this->release();

        return 0;
    }
}