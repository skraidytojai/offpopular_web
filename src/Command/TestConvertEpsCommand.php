<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Imagick;
use ZipArchive;

class TestConvertEpsCommand extends Command
{
    protected $em;

    /** @var PrintfulService */
    protected $printful;

    public function __construct
    (
        EntityManagerInterface $em,
        PrintfulService $printful
    ){
        $this->em = $em;
        $this->printful = $printful;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:eps:test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit','1024M');
        $fs = new Filesystem();

        $output->writeln('Starting!');

        $epsRealPath = PUBLIC_DIRECTORY . "/uploads/test/81.eps";
        if (true !== $fs->exists($epsRealPath)) {
            $output->writeln('EPS file was not found, skipping for now');
        }

        $jpg4kRealDir = PUBLIC_DIRECTORY . "/uploads/test";

        // exec("convert -density 500 $epsRealPath -resize 4000x $jpg4kRealDir/19_imagemagick_test1.jpg");
        // exec("convert -density 500 $epsRealPath -colorspace RGB -resize 4000x $jpg4kRealDir/19_imagemagick_test2.jpg");
        // exec("convert -density 500 $epsRealPath -colorspace sRGB -resize 4000x $jpg4kRealDir/19_imagemagick_test3.jpg");
        // exec("convert -density 500 $epsRealPath -colorspace sRGB -intent absolute -resize 4000x $jpg4kRealDir/19_imagemagick_test4.jpg");
        // exec("convert -density 500 $epsRealPath -colorspace sRGB -intent perceptual -resize 4000x $jpg4kRealDir/19_imagemagick_test5.jpg");
        // exec("convert -density 500 $epsRealPath -colorspace sRGB -intent relative -resize 4000x $jpg4kRealDir/19_imagemagick_test6.jpg");
        // exec("convert -density 500 $epsRealPath -colorspace sRGB -intent saturation -resize 4000x $jpg4kRealDir/19_imagemagick_test7.jpg");

        // exec("convert -density 500 -colorspace sRGB -intent absolute $epsRealPath -resize 4000x $jpg4kRealDir/19_imagemagick_test8.jpg");
        // exec("convert -density 500 -colorspace sRGB -intent perceptual $epsRealPath -resize 4000x $jpg4kRealDir/19_imagemagick_test9.jpg");
        // exec("convert -density 500 -colorspace sRGB -intent relative $epsRealPath -resize 4000x $jpg4kRealDir/19_imagemagick_test10.jpg");
        // exec("convert -density 500 -colorspace sRGB -intent saturation $epsRealPath -resize 4000x $jpg4kRealDir/19_imagemagick_test11.jpg");

        // exec("convert -density 500 -colorspace sRGB $epsRealPath -resize 4000x $jpg4kRealDir/19_imagemagick_test12.jpg");
        // exec("convert -density 500 -colorspace sRGB -resize 4000x $epsRealPath $jpg4kRealDir/19_imagemagick_test13.jpg");

        // exec("convert -flop $jpg4kRealDir/19_imagemagick_test13.jpg $jpg4kRealDir/19_imagemagick_test13_mirrored.jpg");

        // exec("convert $jpg4kRealDir/19_imagemagick_test13_mirrored.jpg $jpg4kRealDir/19_imagemagick_test13.jpg +append $jpg4kRealDir/19_imagemagick_test13_extended_mirrored.jpg");

        $jpg4kRealPath = PUBLIC_DIRECTORY . '/uploads/test/81.jpg';

        /*dump(filesize($jpg4kRealPath));
        $maxSize = 5 * 1024 * 1024;
        dump($maxSize);
        die;*/

        exec("convert -density 500 -colorspace sRGB -resize 4000x $epsRealPath $jpg4kRealPath");

        if (filesize($jpg4kRealPath) > 5000000) {
            exec("convert -density 500 -colorspace sRGB -resize 4000x -quality 75 $epsRealPath $jpg4kRealPath");
        }

        $output->writeln('Finished!');
    }
}