<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use App\Entity;
use Symfony\Component\Console\Input\InputArgument;

class PopulateProductTypeFieldsCommand extends Command
{
    protected $em;

    /** @var PrintfulService */
    protected $printful;

    public function __construct
    (
        EntityManagerInterface $em,
        PrintfulService $printful
    ){
        $this->em = $em;
        $this->printful = $printful;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:product_type:populate');
        $this->addArgument('product_type_id', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting!');

        $productTypeId = $input->getArgument('product_type_id');
        $productTypes = $productTypeId ? [$this->em->getRepository(Entity\ProductType::class)->find($productTypeId)] : $this->em->getRepository(Entity\ProductType::class)->findAll();

        /** @var Entity\ProductType $productType */
        foreach ($productTypes as $productType) {
            $info = $this->printful->getProductInfoAndVariants($productType->getPrintfulProductId());

            $variantIds = array_map(function ($variant) {
                return $variant['id'];
            }, $info['variants']);
            $variantNames = array_map(function ($variant) {
                return $variant['name'];
            }, $info['variants']);
            $variantSizes = array_map(function ($variant) {
                return $variant['size'];
            }, $info['variants']);
            $variantPrices = array_map(function ($variant) {
                return $variant['price'];
            }, $info['variants']);

            $placementFiles = array_map(function ($file) {
                return $file['type'];
            }, $info['product']['files']);

            $productType
                ->setPrintfulVariantIds(implode(',', $variantIds))
                ->setPlacementFiles(implode(',', $placementFiles))
                ->setPrintfulVariantsJson(json_encode($info));

            $this->em->flush();
        }


        $output->writeln('Finished!');
    }
}