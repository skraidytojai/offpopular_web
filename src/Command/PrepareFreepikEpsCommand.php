<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Imagick;
use ZipArchive;
use Symfony\Component\Console\Command\LockableTrait;

class PrepareFreepikEpsCommand extends Command
{
    use LockableTrait;

    protected $em;

    /** @var PrintfulService */
    protected $printful;

    public function __construct
    (
        EntityManagerInterface $em,
        PrintfulService $printful
    ){
        $this->em = $em;
        $this->printful = $printful;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:freepik:prepare');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        ini_set('memory_limit','1024M');
        set_time_limit(3600);

        $fs = new Filesystem();

        $output->writeln('Starting!');

        $unconvertedProducts = $this->em->getRepository(Entity\Product::class)->getProductsWithUnstartedJpgFilesProcessing(1);

        foreach ($unconvertedProducts as $unconvertedProduct) {
            $zipRealPath = PUBLIC_DIRECTORY . "/uploads/images/new_design_zip_files/{$unconvertedProduct->getId()}.zip";
            $zipExtractedDirectory = PUBLIC_DIRECTORY . "/uploads/images/new_design_extracted_zip_files/{$unconvertedProduct->getId()}/";
            $epsRealPath = PUBLIC_DIRECTORY . "/uploads/images/new_design_eps_files/{$unconvertedProduct->getId()}.eps";

            if (!$fs->exists($zipRealPath)) {
                $output->writeln('Product ID: ' . $unconvertedProduct->getId() . ' ZIP file has not been uploaded yet, skipping');
                continue;
            }

            $output->writeln('Working with Product ID: ' . $unconvertedProduct->getId());

            $unconvertedProduct->setJpgDesignProcessingStartedAt(new \DateTime);
            $this->em->flush();

            $unzipAndCopyNeeded = false;
            if (true !== $fs->exists($zipExtractedDirectory)) {
                $fs->mkdir($zipExtractedDirectory);
                $unzipAndCopyNeeded = true;
            }

            if ($unzipAndCopyNeeded) {
                // UNZIP
                $zip = new ZipArchive;
                if ($zip->open($zipRealPath) === TRUE) {
                    $zip->extractTo($zipExtractedDirectory);
                    $zip->close();
                    $output->writeln('Unzip completed');
                } else {
                    $output->writeln('Unzip failed');
                    continue; // check back later
                }

                // COPY THE EPS FILE FROM UNZIPPED FOLDER

                $finder = new Finder();
                $files = iterator_to_array($finder->files()->in($zipExtractedDirectory)->name('*.eps'));
                /** @var \SplFileInfo $epsFile */
                $epsFile = array_shift($files);

                $fs->copy($epsFile->getRealPath(), $epsRealPath);
            }

            if (true !== $fs->exists($epsRealPath)) {
                $output->writeln('EPS file was not found, skipping for now');
                continue; // check back later TODO: log
            }

            $jpg1kRealPath = PUBLIC_DIRECTORY . "/uploads/images/processed_design_jpg_files_1k/{$unconvertedProduct->getId()}.jpg";

            $jpg4kRealPath = PUBLIC_DIRECTORY . "/uploads/images/processed_design_jpg_files_4k/{$unconvertedProduct->getId()}.jpg";
            $jpg4kRealPathMirrored = PUBLIC_DIRECTORY . "/uploads/images/processed_design_jpg_files_4k/{$unconvertedProduct->getId()}_mirrored.jpg";
            $jpg4kRealPathExtendedMirrored = PUBLIC_DIRECTORY . "/uploads/images/processed_design_jpg_files_4k/{$unconvertedProduct->getId()}_extended_mirrored.jpg";

            $jpg8kRealPath = PUBLIC_DIRECTORY . "/uploads/images/processed_design_jpg_files_8k/{$unconvertedProduct->getId()}.jpg";
            $jpg8kRealPathMirrored = PUBLIC_DIRECTORY . "/uploads/images/processed_design_jpg_files_8k/{$unconvertedProduct->getId()}_mirrored.jpg";
            $jpg8kRealPathExtendedMirrored = PUBLIC_DIRECTORY . "/uploads/images/processed_design_jpg_files_8k/{$unconvertedProduct->getId()}_extended_mirrored.jpg";

            // 1000px
            exec("convert -density 500 -colorspace sRGB -resize 1000x $epsRealPath $jpg1kRealPath");

            // 4000px
            exec("convert -density 500 -colorspace sRGB -resize 4000x $epsRealPath $jpg4kRealPath");
            if (filesize($jpg4kRealPath) > 5000000) { // printful limit is 10MB, so for extended_mirrored we need 2x of that. So we'll use a safe expression of 5MB here (a bit less).
                exec("convert -density 500 -colorspace sRGB -resize 4000x -quality 75 $epsRealPath $jpg4kRealPath");
            }
            if (filesize($jpg4kRealPath) > 5000000) { // printful limit is 10MB, so for extended_mirrored we need 2x of that. So we'll use a safe expression of 5MB here (a bit less).
                exec("convert -density 500 -colorspace sRGB -resize 4000x -quality 60 $epsRealPath $jpg4kRealPath");
            }
            exec("convert -flop $jpg4kRealPath $jpg4kRealPathMirrored");
            exec("convert $jpg4kRealPathMirrored $jpg4kRealPath +append $jpg4kRealPathExtendedMirrored");

            // 8000px
            exec("convert -density 500 -colorspace sRGB -resize 8000x $epsRealPath $jpg8kRealPath");
            exec("convert -flop $jpg8kRealPath $jpg8kRealPathMirrored");
            exec("convert $jpg8kRealPathMirrored $jpg8kRealPath +append $jpg8kRealPathExtendedMirrored");

            if ( !($fs->exists($jpg4kRealPath) && $fs->exists($jpg8kRealPath)) ) {
                throw new \Exception('Imagick did not save jpg properly');
            }

            $unconvertedProduct->setJpgDesignCreatedAt(new \DateTime);

            $this->em->flush();
            gc_collect_cycles();
        }

        $output->writeln('Finished!');

        $this->release();

        return 0;
    }
}