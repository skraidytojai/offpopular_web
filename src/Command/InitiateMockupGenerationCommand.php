<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;

/**
 * Class InitiateMockupGenerationCommand
 * @package App\Command
 *
 * @deprecated
 */
class InitiateMockupGenerationCommand extends Command
{
    use LockableTrait;

    protected $em;

    /** @var PrintfulService */
    protected $printful;

    public function __construct
    (
        EntityManagerInterface $em,
        PrintfulService $printful
    ){
        $this->em = $em;
        $this->printful = $printful;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:mockups:initiate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        set_time_limit(3600);

        $output->writeln('Starting!');

        // 1. Get products

        $products = $this->em->getRepository(Entity\Product::class)->getProductsWithoutGeneratedMockups(1);
        $productTypes = $this->em->getRepository(Entity\ProductType::class)->getAllPublished();

        foreach ($products as $product) {
            $output->writeln('Working with PRODUCT ID: ' . $product->getId());

            if (true !== $product->getProductMockups()->isEmpty()) {
                foreach ($product->getProductMockups() as $productMockup) {
                    $productMockup->setIsObsolete(true); // retrying
                }

                $this->em->flush();
            }

            $product->setMockupGenInitStartedAt(new \DateTime);
            $this->em->flush();

            // 2. issue mockup gen tasks
            foreach ($productTypes as $productType) {
                $output->writeln('Working with Product Type: ' . $productType . '!');

                $this->printful->initiateMockupGenTask($productType, $product);
                sleep(9); // spreading out requests to printful api evenly
            }

            $product->setMockupGenInitFinishedAt(new \DateTime);
            $this->em->flush();
        }

        $output->writeln('Finished!');

        $this->release();

        return 0;
    }
}