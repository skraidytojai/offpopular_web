<?php

namespace App\Command\Aws;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;
use App\Service\CloudStorageService;

class UploadZipCommand extends Command
{
    use LockableTrait;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var CloudStorageService */
    protected $cs;

    public function __construct
    (
        EntityManagerInterface $em,
        CloudStorageService $cs
    ){
        $this->em = $em;
        $this->cs = $cs;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:aws:upload_zip');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        die('deprecated');

        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        set_time_limit(3600);

        $output->writeln('Starting');

        // Get 50 unuploaded
        $products = getenv('APP_ENV') === 'prod' ? $this->em->getRepository(Entity\Product::class)->getAwsReadyToUploadZip() : [$this->em->getRepository(Entity\Product::class)->find(20)];

        // check if zip file exists on php side, make upload requests, mark date of success on db
        foreach ($products as $product) {
            $output->writeln('Working with product id: ' . $product->getId());

            if ( ($zipPath = $product->getZipFilePath()) ) {
                if ($this->cs->uploadProductZipFile($product, $zipPath, $product->getId() . '.zip')) {
                    $product->setAwsZipUploadedAt(new \DateTime);
                    $this->em->flush();
                }
            }
        }

        $output->writeln('Finished!');
        $this->release();
        return 0;

        // put to cron on server every min, with lock
    }
}