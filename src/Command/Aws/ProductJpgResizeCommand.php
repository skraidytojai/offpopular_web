<?php

namespace App\Command\Aws;

use App\Service\CloudProcessingService;
use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;
use App\Service\CloudStorageService;

class ProductJpgResizeCommand extends Command
{
    use LockableTrait;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var CloudStorageService */
    protected $cs;

    /** @var CloudProcessingService */
    protected $cp;

    public function __construct
    (
        EntityManagerInterface $em,
        CloudStorageService $cs,
        CloudProcessingService $cp
    ){
        $this->em = $em;
        $this->cs = $cs;
        $this->cp = $cp;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:aws:product_jpg_resize');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // NON LOCKED COMMAND

        set_time_limit(1800);

        $output->writeln('Starting');

        $products = getenv('APP_ENV') === 'prod' ? $this->em->getRepository(Entity\Product::class)->getAwsReadyForProductJpgResize() : [$this->em->getRepository(Entity\Product::class)->find(20)];

        // check if zip file exists on php side, make upload requests, mark date of success on db
        foreach ($products as $product) {
            $output->writeln('Working with product id: ' . $product->getId());

            $product->setAwsJpgResizeRequestMadeAt(new \DateTime);
            $this->em->flush();

            if ($this->cp->initiateProductJpgResize($product)) {
                $product->setAwsJpgResizeSucceededAt(new \DateTime);
                $this->em->flush();
            }
        }

        $output->writeln('Finished!');
        return 0;

        // put to cron on server every min
    }
}