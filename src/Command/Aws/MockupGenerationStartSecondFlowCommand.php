<?php

namespace App\Command\Aws;

class MockupGenerationStartSecondFlowCommand extends MockupGenerationStartCommand
{
    protected function configure()
    {
        $this->setName('app:aws:mockup_start_second_flow');
    }
}