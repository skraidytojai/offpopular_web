<?php

namespace App\Command\Aws;

use App\Service\CloudProcessingService;
use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;
use App\Service\CloudStorageService;

class UnzipAndTransferEpsCommand extends Command
{
    use LockableTrait;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var CloudStorageService */
    protected $cs;

    /** @var CloudProcessingService */
    protected $cp;

    public function __construct
    (
        EntityManagerInterface $em,
        CloudStorageService $cs,
        CloudProcessingService $cp
    ){
        $this->em = $em;
        $this->cs = $cs;
        $this->cp = $cp;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:aws:unzip_and_eps');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        set_time_limit(3600);

        $output->writeln('Starting');

        // Get 50 unuploaded
        $products = getenv('APP_ENV') === 'prod' ? $this->em->getRepository(Entity\Product::class)->getAwsReadyToUnzipAndEps() : [$this->em->getRepository(Entity\Product::class)->find(20)];

        // check if zip file exists on php side, make upload requests, mark date of success on db
        foreach ($products as $product) {
            $output->writeln('Working with product id: ' . $product->getId());

            $product->setAwsUnzipRequestMadeAt(new \DateTime);
            $this->em->flush();

            if ($this->cp->initiateProductUnzipAndEpsTransfer($product)) {
                $product->setAwsUnzipSucceededAt(new \DateTime);
                $this->em->flush();
            }
        }

        $output->writeln('Finished!');
        $this->release();
        return 0;

        // put to cron on server every min, with lock
    }
}