<?php

namespace App\Command\Aws;

use App\Service\CloudProcessingService;
use App\Service\CloudStorageService;
use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;

class MockupThumbsCheckCommand extends Command
{
    use LockableTrait;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var CloudStorageService */
    protected $cs;

    /** @var CloudProcessingService */
    protected $cp;

    public function __construct
    (
        EntityManagerInterface $em,
        CloudStorageService $cs,
        CloudProcessingService $cp
    ){
        $this->em = $em;
        $this->cs = $cs;
        $this->cp = $cp;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:aws:mockup_thumb_check');
        $this->addArgument('flow_id', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock($this->getName() . $input->getArgument('flow_id'))) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        set_time_limit(900);

        $output->writeln('Starting!');

        /** @var Entity\ProductMockup[] $productMockups */
        $productMockups = getenv('APP_ENV') == 'prod' ? $this->em->getRepository(Entity\ProductMockup::class)->getForAwsThumbCheck(10) : [$this->em->getRepository(Entity\ProductMockup::class)->find(999918)];

        foreach ($productMockups as $productMockup) {
            $output->writeln('Working with ProductMockup ID: ' . $productMockup->getId());

            $productMockup->setIsThumbsChecked(true);
            $this->em->flush();

            $key = $productMockup->getProduct()->getId() . '/' . $productMockup->getId() . '/' . $productMockup->getId() . '_500px.jpg';
            $fileSize = $this->cs->getS3ObjectFileSize(CloudStorageService::PRODUCT_MOCKUPS_RESIZED_BUCKET, $key);

            if ($fileSize > 0) {
                // OK, continue
                continue;
            }

            $productMockup->setThumbsRegenInitiatedAt(new \DateTime);
            $this->em->flush();

            if ($this->cp->initiateProductMockupThumbGen($productMockup)) {
                $productMockup->setIsThumbsRegenSuccess(true);
                $this->em->flush();
            }
        }

        $output->writeln('Finished!');

        $this->release();

        return 0;
    }
}