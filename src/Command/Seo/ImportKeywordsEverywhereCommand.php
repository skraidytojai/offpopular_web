<?php

namespace App\Command\Seo;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;

class ImportKeywordsEverywhereCommand extends Command
{
    use LockableTrait;

    protected $em;

    public function __construct
    (
        EntityManagerInterface $em
    ){
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:seo:import_keywords_everywhere');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        ini_set('memory_limit','1024M');

        $output->writeln('Starting!');

        $fileDir = realpath(__DIR__ . '/../../../temp/keywords_everywhere_import');

        $finder = new Finder();
        $finder->files()->in($fileDir);

        $csvPaths = [];
        foreach ($finder as $file) {
            $csvPaths[] = $file->getRealPath();
        }

        foreach ($csvPaths as $csvPath) {

            $output->writeln('CSV PATH: ' . $csvPath);

            // $fp is file pointer to file sample.csv
            $n = 0;
            if (($fp = fopen($csvPath, "r")) !== FALSE) {
                while (($row = fgetcsv($fp, 1000, ",")) !== FALSE) {
                    $n++;
                    if ($n <= 1) {
                        continue; // skip header row
                    }

                    $output->writeln('Row number: ' . $n);

                    $keyword = $row[1];
                    $volume = (int) preg_replace('/[^0-9]+/', '', $row[2]);
                    $cpc = (float) preg_replace('/[^0-9.]+/', '', $row[3]);
                    $compScore = (float) $row[4];
                    $volumeGlobal = (int) preg_replace('/[^0-9]+/', '', $row[5]);
                    $cpcGlobal = (float) preg_replace('/[^0-9.]+/', '', $row[6]);
                    $compScoreGlobal = (float) $row[7];

                    $indexedSearch = (new Entity\IndexedSearch)
                        ->setKeyword($keyword)
                        ->setSearchVolume($volume)
                        ->setCpc($cpc)
                        ->setCompetitionScore($compScore)
                        ->setSearchVolumeGlobal($volumeGlobal)
                        ->setCpcGlobal($cpcGlobal)
                        ->setCompetitionScoreGlobal($compScoreGlobal);

                    $this->em->persist($indexedSearch);
                }
                fclose($fp);
            }

            $this->em->flush();
        }

        $output->writeln('Finished!');

        $this->release();

        return 0;
    }
}