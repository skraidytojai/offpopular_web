<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use App\Entity;
use Symfony\Component\Console\Input\InputArgument;


class PopulateProductVariantsCommand extends Command
{
    protected $em;

    /** @var PrintfulService */
    protected $printful;

    public function __construct
    (
        EntityManagerInterface $em,
        PrintfulService $printful
    ){
        $this->em = $em;
        $this->printful = $printful;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:product_variant:populate');
        $this->addArgument('product_type_id', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting!');

        $productTypeId = $input->getArgument('product_type_id');
        $productTypes = $productTypeId ? [$this->em->getRepository(Entity\ProductType::class)->find($productTypeId)] : $this->em->getRepository(Entity\ProductType::class)->findAll();

        foreach ($productTypes as $productType) {
            $output->writeln('Working with product type: ' . $productType);

            $info = json_decode($productType->getPrintfulVariantsJson(), true);
            $variants = $info['variants'];

            foreach ($variants as $variant) {
                $output->writeln('Processing Variant ID: ' . $variant['id']);

                $printfulVariant = (new Entity\PrintfulVariant())
                    ->setPrintfulVariantId($variant['id'])
                    ->setPrintfulProductId($variant['product_id'])
                    ->setName($variant['name'])
                    ->setSize($variant['size'])
                    ->setColor($variant['color'])
                    ->setColorCode($variant['color_code'])
                    ->setImageUrl($variant['image'])
                    ->setPrintfulPrice($variant['price'])
                    ->setIsInStock($variant['in_stock']);

                $this->em->persist($printfulVariant);

                $productVariant = (new Entity\ProductVariant())
                    ->setPrintfulVariant($printfulVariant)
                    ->setProductType($productType)
                    ->setName($printfulVariant->getName())
                    ->setSize($printfulVariant->getSize())
                    ->setColor($printfulVariant->getColor())
                    ->setColorCode($printfulVariant->getColorCode())
                    ->setIsInStock($printfulVariant->getIsInStock())
                    ->setImageUrl($printfulVariant->getImageUrl());

                $this->em->persist($productVariant);
                $this->em->flush();
            }
        }

        $output->writeln('Finished!');
    }
}