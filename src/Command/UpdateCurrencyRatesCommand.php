<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCurrencyRatesCommand extends Command
{
    protected $em;

    const CURRENCIES = [
        ['from' => 'USD', 'to' => 'EUR'],
    ];

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:currency_rates:update');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // TODO: add error catcher/handler for file_get_contents, as external requests are never 100% predictable

        $output->writeln('Updating!');

        foreach (self::CURRENCIES as $currencyPair) {
            $rateId = "{$currencyPair['from']}_{$currencyPair['to']}";
            $data = file_get_contents("http://free.currencyconverterapi.com/api/v5/convert?q={$rateId}&compact=y");

            $decoded = json_decode($data, true);
            $rate = $decoded[$rateId]['val'];

            $this->em->getRepository('App:CurrencyRate')->insertNewCurrencyRateData($currencyPair['from'], $currencyPair['to'], $rate);

            $output->writeln($rate);
        }

        $output->writeln('Finished!');
    }
}