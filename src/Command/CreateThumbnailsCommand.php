<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\PrintfulService;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity;
use Symfony\Component\Console\Command\LockableTrait;

class CreateThumbnailsCommand extends Command
{
    use LockableTrait;

    protected $em;

    public function __construct
    (
        EntityManagerInterface $em
    ){
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:thumbnails:create');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        set_time_limit(360);

        $output->writeln('Starting!');

        $mockups = $this->em->getRepository(Entity\ProductMockup::class)->getWithoutThumbnails();
        $fs = new Filesystem();

        foreach ($mockups as $mockup) {
            $fullJpgPath = PUBLIC_DIRECTORY . $mockup->getServerPicturePath();
            $directory = dirname($fullJpgPath);

            if (!$mockup->getJpg350pxCreatedAt()) {
                $thumbPath = "$directory/{$mockup->getId()}_350px_80.jpg";
                exec("convert $fullJpgPath -resize 350x -quality 80 $thumbPath");

                $fs->exists($thumbPath) ? $mockup->setJpg350pxCreatedAt(new \DateTime) : $mockup->setJpg350pxFailedAt(new \DateTime);
            }

            if (!$mockup->getWebp350pxCreatedAt()) {
                $thumbPath = "$directory/{$mockup->getId()}_350px_80.webp";
                exec("convert $fullJpgPath -resize 350x -quality 80 $thumbPath");

                $fs->exists($thumbPath) ? $mockup->setWebp350pxCreatedAt(new \DateTime) : $mockup->setWebp350pxFailedAt(new \DateTime);
            }
        }

        $this->em->flush();

        $output->writeln('Finished!');

        $this->release();

        return 0;
    }
}