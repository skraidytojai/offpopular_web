<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180828094707 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_mockup ADD thumb50px_created_at DATETIME DEFAULT NULL, ADD thumb100px_created_at DATETIME DEFAULT NULL, ADD thumb500px_created_at DATETIME DEFAULT NULL, ADD thumb50px_failed_at DATETIME DEFAULT NULL, ADD thumb100px_failed_at DATETIME DEFAULT NULL, ADD thumb500px_failed_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `product_mockup` DROP thumb50px_created_at, DROP thumb100px_created_at, DROP thumb500px_created_at, DROP thumb50px_failed_at, DROP thumb100px_failed_at, DROP thumb500px_failed_at');
    }
}
