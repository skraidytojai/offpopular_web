<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180810095452 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `external_api_call` (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, product_type_id INT DEFAULT NULL, api_name VARCHAR(50) DEFAULT NULL, purpose VARCHAR(50) DEFAULT NULL, outcome SMALLINT NOT NULL, error VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_275EB3E4584665A (product_id), INDEX IDX_275EB3E14959723 (product_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `external_api_call` ADD CONSTRAINT FK_275EB3E4584665A FOREIGN KEY (product_id) REFERENCES `product` (id)');
        $this->addSql('ALTER TABLE `external_api_call` ADD CONSTRAINT FK_275EB3E14959723 FOREIGN KEY (product_type_id) REFERENCES `product_type` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `external_api_call`');
    }
}
