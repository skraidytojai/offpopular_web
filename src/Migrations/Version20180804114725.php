<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180804114725 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `printful_variant` (id INT AUTO_INCREMENT NOT NULL, printful_variant_id INT NOT NULL, printful_product_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, size VARCHAR(255) DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, color_code VARCHAR(255) DEFAULT NULL, image_url VARCHAR(255) DEFAULT NULL, printful_price NUMERIC(10, 2) NOT NULL, is_in_stock TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `product_variant` (id INT AUTO_INCREMENT NOT NULL, printful_variant_id INT DEFAULT NULL, product_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, size VARCHAR(255) DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, color_code VARCHAR(255) DEFAULT NULL, image_url VARCHAR(255) DEFAULT NULL, printful_price NUMERIC(10, 2) NOT NULL, is_in_stock TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_209AA41D24F34218 (printful_variant_id), INDEX IDX_209AA41D4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `product_variant` ADD CONSTRAINT FK_209AA41D24F34218 FOREIGN KEY (printful_variant_id) REFERENCES `printful_variant` (id)');
        $this->addSql('ALTER TABLE `product_variant` ADD CONSTRAINT FK_209AA41D4584665A FOREIGN KEY (product_id) REFERENCES `product` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `product_variant` DROP FOREIGN KEY FK_209AA41D24F34218');
        $this->addSql('DROP TABLE `printful_variant`');
        $this->addSql('DROP TABLE `product_variant`');
    }
}
