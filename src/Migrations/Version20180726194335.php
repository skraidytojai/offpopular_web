<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180726194335 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE printful_mockup_gen_task ADD product_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE printful_mockup_gen_task ADD CONSTRAINT FK_EBE45DA314959723 FOREIGN KEY (product_type_id) REFERENCES `product_type` (id)');
        $this->addSql('CREATE INDEX IDX_EBE45DA314959723 ON printful_mockup_gen_task (product_type_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `printful_mockup_gen_task` DROP FOREIGN KEY FK_EBE45DA314959723');
        $this->addSql('DROP INDEX IDX_EBE45DA314959723 ON `printful_mockup_gen_task`');
        $this->addSql('ALTER TABLE `printful_mockup_gen_task` DROP product_type_id');
    }
}
