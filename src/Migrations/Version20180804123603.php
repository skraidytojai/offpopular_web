<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180804123603 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `product_mockup` (id INT AUTO_INCREMENT NOT NULL, default_product_type_id INT DEFAULT NULL, product_id INT DEFAULT NULL, printful_picture_url VARCHAR(255) DEFAULT NULL, server_picture_path VARCHAR(255) DEFAULT NULL, placement VARCHAR(255) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, type SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_13EA9CBC8CC33FFF (default_product_type_id), INDEX IDX_13EA9CBC4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_variants_product_mockups (product_variant_id INT NOT NULL, product_mockup_id INT NOT NULL, INDEX IDX_8CF99898A80EF684 (product_variant_id), INDEX IDX_8CF99898372E0C27 (product_mockup_id), PRIMARY KEY(product_variant_id, product_mockup_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `product_mockup` ADD CONSTRAINT FK_13EA9CBC8CC33FFF FOREIGN KEY (default_product_type_id) REFERENCES `product_type` (id)');
        $this->addSql('ALTER TABLE `product_mockup` ADD CONSTRAINT FK_13EA9CBC4584665A FOREIGN KEY (product_id) REFERENCES `product` (id)');
        $this->addSql('ALTER TABLE product_variants_product_mockups ADD CONSTRAINT FK_8CF99898A80EF684 FOREIGN KEY (product_variant_id) REFERENCES `product_variant` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_variants_product_mockups ADD CONSTRAINT FK_8CF99898372E0C27 FOREIGN KEY (product_mockup_id) REFERENCES `product_mockup` (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_variants_product_mockups DROP FOREIGN KEY FK_8CF99898372E0C27');
        $this->addSql('DROP TABLE `product_mockup`');
        $this->addSql('DROP TABLE product_variants_product_mockups');
    }
}
