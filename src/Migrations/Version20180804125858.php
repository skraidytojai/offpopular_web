<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180804125858 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_type ADD mockup_generation_strategy SMALLINT NOT NULL, DROP printful_variant_names, DROP printful_variant_sizes, DROP printful_variant_prices');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `product_type` ADD printful_variant_names LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD printful_variant_sizes LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD printful_variant_prices LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP mockup_generation_strategy');
    }
}
