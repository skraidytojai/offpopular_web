<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180914212040 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX is_uploaded_to_s3_idx ON product_mockup (is_uploaded_to_s3)');
        $this->addSql('CREATE INDEX is_thumbs_checked_idx ON product_mockup (is_thumbs_checked)');
        $this->addSql('ALTER TABLE product_type ADD hide_size_guide TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX is_uploaded_to_s3_idx ON `product_mockup`');
        $this->addSql('DROP INDEX is_thumbs_checked_idx ON `product_mockup`');
        $this->addSql('ALTER TABLE `product_type` DROP hide_size_guide');
    }
}
