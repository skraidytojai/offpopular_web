<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180814132534 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_item ADD product_type_id INT DEFAULT NULL, ADD product_variant_id INT DEFAULT NULL, DROP product_type, DROP product_dimension, DROP product_option, DROP product_option_label');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F0914959723 FOREIGN KEY (product_type_id) REFERENCES `product_type` (id)');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F09A80EF684 FOREIGN KEY (product_variant_id) REFERENCES `product_variant` (id)');
        $this->addSql('CREATE INDEX IDX_52EA1F0914959723 ON order_item (product_type_id)');
        $this->addSql('CREATE INDEX IDX_52EA1F09A80EF684 ON order_item (product_variant_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `order_item` DROP FOREIGN KEY FK_52EA1F0914959723');
        $this->addSql('ALTER TABLE `order_item` DROP FOREIGN KEY FK_52EA1F09A80EF684');
        $this->addSql('DROP INDEX IDX_52EA1F0914959723 ON `order_item`');
        $this->addSql('DROP INDEX IDX_52EA1F09A80EF684 ON `order_item`');
        $this->addSql('ALTER TABLE `order_item` ADD product_type VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD product_dimension VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD product_option VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD product_option_label VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP product_type_id, DROP product_variant_id');
    }
}
