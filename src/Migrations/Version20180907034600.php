<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180907034600 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD aws_zip_uploaded_at DATETIME DEFAULT NULL, ADD aws_unzip_request_made_at DATETIME DEFAULT NULL, ADD aws_unzip_succeeded_at DATETIME DEFAULT NULL, ADD aws_eps_to_jpg_request_made_at DATETIME DEFAULT NULL, ADD aws_eps_to_jpg_succeeded_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product_mockup ADD aws_jpg_transfered_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `product` DROP aws_zip_uploaded_at, DROP aws_unzip_request_made_at, DROP aws_unzip_succeeded_at, DROP aws_eps_to_jpg_request_made_at, DROP aws_eps_to_jpg_succeeded_at');
        $this->addSql('ALTER TABLE `product_mockup` DROP aws_jpg_transfered_at');
    }
}
