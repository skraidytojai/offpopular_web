<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180726183058 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP title_en, DROP description_en, DROP year, DROP video1_url, DROP video2_url, DROP file_max_resolution_mp, DROP file_aspect_ratio, DROP author_short_comment');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `product` ADD title_en VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD description_en LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD year VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD video1_url VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD video2_url VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD file_max_resolution_mp INT NOT NULL, ADD file_aspect_ratio NUMERIC(10, 2) NOT NULL, ADD author_short_comment VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
