<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180813173315 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `product_type_category` (id INT AUTO_INCREMENT NOT NULL, parent_product_type_category_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, sysname VARCHAR(50) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, is_top_level TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_115E22CF989D9B62 (slug), INDEX IDX_115E22CF2885C3FF (parent_product_type_category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `product_type_category` ADD CONSTRAINT FK_115E22CF2885C3FF FOREIGN KEY (parent_product_type_category_id) REFERENCES `product_type_category` (id)');
        $this->addSql('ALTER TABLE product_type ADD product_type_category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_type ADD CONSTRAINT FK_1367588D7D32E8F FOREIGN KEY (product_type_category_id) REFERENCES `product_type_category` (id)');
        $this->addSql('CREATE INDEX IDX_1367588D7D32E8F ON product_type (product_type_category_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `product_type` DROP FOREIGN KEY FK_1367588D7D32E8F');
        $this->addSql('ALTER TABLE `product_type_category` DROP FOREIGN KEY FK_115E22CF2885C3FF');
        $this->addSql('DROP TABLE `product_type_category`');
        $this->addSql('DROP INDEX IDX_1367588D7D32E8F ON `product_type`');
        $this->addSql('ALTER TABLE `product_type` DROP product_type_category_id');
    }
}
