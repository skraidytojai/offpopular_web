<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180918141134 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_type ADD title_for_search VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE product_type_category ADD title_for_search VARCHAR(50) DEFAULT NULL');

        $this->addSql('UPDATE product_type SET title_for_search = title');
        $this->addSql('UPDATE product_type_category SET title_for_search = title');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `product_type` DROP title_for_search');
        $this->addSql('ALTER TABLE `product_type_category` DROP title_for_search');
    }
}
