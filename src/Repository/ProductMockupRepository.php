<?php

namespace App\Repository;

use App\Entity;
use App\DataObject\ProductResult;

class ProductMockupRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param Entity\Product $product
     * @param int $limit
     * @return Entity\ProductMockup[]|array
     */
    public function getRandomForProduct(Entity\Product $product, $limit = 5)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductMockup::class)->createQueryBuilder('pm')
            ->where('pm.isObsolete = :isObsolete AND pm.product = :product')
            ->setParameter('isObsolete', false)
            ->setParameter('product', $product)
            ->setMaxResults($limit);

        $qb->addSelect('RAND() as HIDDEN rand')->orderBy('rand');

        return $qb->getQuery()->getResult();
    }
    
    public function getForAwsThumbCheck($limit = 20)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductMockup::class)->createQueryBuilder('pm')
            ->where('pm.isObsolete = :isObsolete AND pm.product <= :maxProductId AND pm.createdAt >= :earliestDate AND pm.createdAt <= :tenMinutesAgo')
            ->andWhere('pm.isUploadedToS3 = :isUploadedToS3 AND pm.isThumbsChecked = :isThumbsChecked')
            ->setParameter('isObsolete', false)
            ->setParameter('maxProductId', 500)
            ->setParameter('earliestDate', new \DateTime('2018-09-13 10:00:00'))
            ->setParameter('tenMinutesAgo', new \DateTime('-10 minutes'))
            ->setParameter('isUploadedToS3', true)
            ->setParameter('isThumbsChecked', false)
            ->setMaxResults($limit);

        $qb->addSelect('RAND() as HIDDEN rand')->orderBy('rand');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return Entity\ProductMockup[]|array
     */
    public function getWithoutThumbnails($limit = 250)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductMockup::class)->createQueryBuilder('pm')
            ->where('pm.isObsolete = :isObsolete')
            ->andWhere('pm.jpg350pxCreatedAt IS NULL OR pm.webp350pxCreatedAt IS NULL')
            ->andWhere('pm.jpg350pxFailedAt IS NULL AND pm.webp350pxFailedAt IS NULL')
            ->setParameter('isObsolete', false)
            ->orderBy('pm.createdAt', 'ASC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return Entity\ProductMockup[]|array
     */
    public function getWithoutThumbnailsSecondFlow($limit = 250)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductMockup::class)->createQueryBuilder('pm')
            ->where('pm.isObsolete = :isObsolete')
            ->andWhere('pm.thumb50pxCreatedAt IS NULL OR pm.thumb100pxCreatedAt IS NULL OR pm.thumb500pxCreatedAt IS NULL')
            ->andWhere('pm.thumb50pxFailedAt IS NULL AND pm.thumb100pxFailedAt IS NULL AND pm.thumb500pxFailedAt IS NULL')
            ->setParameter('isObsolete', false)
            ->orderBy('pm.createdAt', 'ASC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @deprecated
     *
     * @param ProductResult[]|array $productResults
     * @return Entity\ProductMockup[]|array
     */
    public function getMainProductMockupsForProductResultArray(array $productResults)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductMockup::class)->createQueryBuilder('pm');

        $qb->where('(1 = 0)');

        foreach ($productResults as $productResult) {
            //             $qb->orWhere("(pm.product = {$productResult->product->getId()} AND pm.defaultProductType = {$productResult->productType->getId()} AND pm.isObsolete = 0 AND pm.type = {$type})");
            $qb->orWhere("(pm.product = {$productResult->product->getId()} AND pm.defaultProductType = {$productResult->productType->getId()})");
        }

        $qb->andWhere('(pm.isObsolete = :isObsolete AND pm.type = :type)')
            ->setParameter('isObsolete', false)
            ->setParameter('type', Entity\ProductMockup::TYPE_MAIN);

        //dump($qb->getQuery()->getSQL());
        //die;

        return $qb->getQuery()->getResult();
    }
}