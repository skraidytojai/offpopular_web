<?php

namespace App\Repository;

use App\Entity;

class ProductTypeRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @param bool $grouped
     * @return Entity\ProductType[]|array|bool
     */
    public function getAllPublished($grouped = false)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductType::class)->createQueryBuilder('pt')
            ->where('pt.isPublished = :isPublished')
            ->setParameter('isPublished', true);

        /** @var Entity\ProductType[] $result */
        $result = $qb->getQuery()->getResult();

        if ($grouped) {
            $grouped = [];
            foreach ($result as $productType) {
                $grouped[$productType->getGroup()][] = $productType;
            }

            return $grouped;
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getPublishedCount()
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductType::class)->createQueryBuilder('pt')
            ->select('COUNT(pt.id)')
            ->where('pt.isPublished = :isPublished')
            ->setParameter('isPublished', true);

        return $qb->getQuery()->getSingleScalarResult();
    }
}