<?php

namespace App\Repository;

use App\Entity;

class OrderRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param $orderId
     * @return Entity\Order
     */
    public function getOrderForPayment($orderId)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Order::class)->createQueryBuilder('ord')
            ->where('ord.id = :orderId AND ord.status = :unpaidStatus')
            ->setParameter('orderId', $orderId)
            ->setParameter('unpaidStatus', Entity\Order::STATUS_PLACED_UNPAID)
            ->setMaxResults(1);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Exception $e) {
            return null;
        }
    }
}