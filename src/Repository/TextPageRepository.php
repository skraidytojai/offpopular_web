<?php

namespace App\Repository;

use App\Entity;

class TextPageRepository extends \Doctrine\ORM\EntityRepository
{
    public function getPublishedBySlug($slug)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\TextPage::class)->createQueryBuilder('tp')
            ->where('tp.slug  = :slug AND tp.isPublished = :isPublished')
            ->setParameter('slug', $slug)
            ->setParameter('isPublished', true);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @return Entity\TextPage[]|array
     */
    public function getAllPublished()
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\TextPage::class)->createQueryBuilder('tp')
            ->where('tp.isPublished = :isPublished')
            ->setParameter('isPublished', true)
            ->orderBy('tp.id', 'ASC');

        return $qb->getQuery()->getResult();
    }
}