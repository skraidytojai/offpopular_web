<?php

namespace App\Repository;

use App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use App\DataObject\ProductResult;
use App\Service\FastMemoryService;

class ProductRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @param int $offset
     * @param int $limit
     * @return ProductResult[]|array
     */
    public function getPublishedProductResults($offset = 0, $limit = 30, $exceptionalFirst = false)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.isPublished = :isPublished')
            ->setParameter('isPublished', true)
            ->orderBy('p.rank', 'ASC')
            ->addOrderBy('p.createdAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        if ($exceptionalFirst && $offset == 0) {
            $qb->andWhere('p.isExceptional = :isExceptional')
                ->setParameter('isExceptional', true);
        } else {
            $qb->addSelect('RAND() as HIDDEN rand')->orderBy('rand');
        }

        /** @var Entity\Product[]|array $products */
        $products = $qb->getQuery()->getResult();

        $productTypesGrouped = $em->getRepository(Entity\ProductType::class)->getAllPublished(true);
        $groups = new ArrayCollection(array_keys($productTypesGrouped));

        $productResults = [];
        foreach ($products as $product) {
            $group = $groups->current();
            /** @var Entity\ProductType $resultProductType */
            $resultProductType = $productTypesGrouped[$group][array_rand($productTypesGrouped[$group])];

            $productResults[$product->getId()] = new ProductResult($product, $resultProductType);

            if (!$groups->next()) {
                $groups->first(); // reached the end of array
            }
        }

        return array_values($productResults); // rearrange array indexes from 0, to avoid JS transforming from array to object on the frontend...
    }

    public function getRelatedProductTypeProductResults(Entity\Product $product)
    {
        $em = $this->getEntityManager();
        $productTypes = $em->getRepository('App:ProductType')->getAllPublished();

        $productResults = [];
        foreach ($productTypes as $productType) {
            $productResults[$productType->getId()] = new ProductResult($product, $productType);
        }

        return array_values($productResults); // rearrange array indexes from 0, to avoid JS transforming from array to object on the frontend...
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getPublishedProducts($offset = 0, $limit = 30, $indexed = false)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.isPublished = :isPublished')
            ->setParameter('isPublished', true)
            ->orderBy('p.rank', 'ASC')
            ->addOrderBy('p.createdAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        $products = $qb->getQuery()->getResult();

        if (true === $indexed) {
            $allProductsIndexed = [];
            foreach ($products as $product) {
                $allProductsIndexed[$product->getId()] = $product;
            }

            return $allProductsIndexed;
        }

        return $products;
    }

    /**
     * @return int
     */
    public function getPublishedProductsCount()
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.isPublished = :isPublished')
            ->setParameter('isPublished', true);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getRandomPublishedProducts($limit = 8)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.isPublished = :isPublished')
            ->setParameter('isPublished', true)
            ->setMaxResults($limit);

        $qb->addSelect('RAND() as HIDDEN rand')->orderBy('rand');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getProductsWithUnstartedJpgFilesProcessing($limit = 15)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.jpgDesignProcessingStartedAt IS NULL')
            ->orWhere('p.jpgDesignCreatedAt IS NULL AND p.jpgDesignProcessingStartedAt IS NOT NULL AND p.jpgDesignProcessingStartedAt < :eightyMinutesAgo') // retrying after 80 minutes
            ->orderBy('p.createdAt', 'ASC')
            ->setParameter('eightyMinutesAgo', new \DateTime('-80 minutes'))
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return Entity\Product[]|array
     *
     * @deprecated
     */
    public function getProductsWithoutGeneratedMockups($limit = 30)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.mockupGenInitStartedAt IS NULL AND p.jpgDesignCreatedAt IS NOT NULL')
            ->orWhere('p.mockupGenInitStartedAt IS NOT NULL AND p.mockupGenInitStartedAt < :twelveHoursAgo AND p.mockupGenInitFinishedAt IS NULL')
            ->setParameter('twelveHoursAgo', new \DateTime('-12 hours'))
            ->orderBy('p.createdAt', 'ASC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $title
     * @param array $tagTitles
     * @param array $options
     * @return Entity\Product|bool
     */
    public function addScrapedProduct(string $title, array $tagTitles, array $options = [])
    {
        // check for existing
        if (isset($options['id_at_source'])) {
            if ( $this->getEntityManager()->getRepository(Entity\Product::class)->findBy(['idAtSource' => $options['id_at_source']]) ) {
                return false;
            }
        }

        $tagTitles = array_map(function ($tagTitle) {
            return strtolower($tagTitle);
        }, $tagTitles);

        $tags = array_map(function ($tagTitle) {
            return $this->getEntityManager()->getRepository(Entity\Tag::class)->getOrCreateByTitle($tagTitle);
        }, $tagTitles);

        $product = (new Entity\Product)
            ->setTitle($title)
            ->setTitleOriginal($title)
            ->addManyTags($tags);

        if (isset($options['author_name'])) {
            $product->setAuthorName($options['author_name']);
        }

        if (isset($options['author_url'])) {
            $product->setAuthorUrl($options['author_url']);
        }

        if (isset($options['design_has_text']) && $options['design_has_text']) {
            $product->setDesignHasText(true);
        }

        if (isset($options['design_is_single_image']) && $options['design_is_single_image']) {
            $product->setDesignIsSingleImage(true);
        }

        if (isset($options['id_at_source'])) {
            $product->setIdAtSource($options['id_at_source']);
        }

        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush();

        return $product;
    }

    /**
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getAwsReadyToUploadZip($limit = 50)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.awsZipUploadedAt IS NULL')
            ->orderBy('p.createdAt', 'ASC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getAwsReadyToUnzipAndEps($limit = 50)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.awsZipUploadedAt IS NOT NULL AND p.awsUnzipRequestMadeAt IS NULL')
            ->orderBy('p.createdAt', 'ASC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getAwsReadyForEpsToJpgConversion($limit = 6)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.awsUnzipSucceededAt IS NOT NULL AND p.awsEpsToJpgRequestMadeAt IS NULL')
            ->orderBy('p.hasPriorityForProcessing', 'DESC')
            ->addOrderBy('p.createdAt', 'ASC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getAwsReadyForProductJpgResize($limit = 5)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.awsEpsToJpgSucceededAt IS NOT NULL AND p.awsJpgResizeRequestMadeAt IS NULL')
            ->setMaxResults($limit);

        $qb->addSelect('RAND() as HIDDEN rand')->orderBy('rand');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getAwsReadyForProductMockupGeneration($limit = 30)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.mockupGenInitStartedAt IS NULL AND p.awsJpgResizeSucceededAt IS NOT NULL')
            ///->orWhere('p.mockupGenInitStartedAt IS NOT NULL AND p.mockupGenInitStartedAt < :twelveHoursAgo AND p.mockupGenInitFinishedAt IS NULL') // restart
            //->setParameter('twelveHoursAgo', new \DateTime('-12 hours'))
            ->orderBy('p.hasPriorityForProcessing', 'DESC')
            ->addOrderBy('p.createdAt', 'ASC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return Entity\Product[]
     */
    public function getForQualityReview($limit = 20)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.qualityControlAcceptedAt IS NULL AND p.qualityControlRejectedAt IS NULL AND p.isDeleted = :isDeleted AND p.awsEpsToJpgRequestMadeAt <= :awsEpsToJpgRequestMadeAt')
            ->setParameter('isDeleted', false)
            ->setParameter('awsEpsToJpgRequestMadeAt', new \DateTime('-1 day'))
            ->orderBy('p.id', 'ASC')
            ->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    /**
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getForMockupCaching($limit = 100)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.mockupGenInitFinishedAt IS NOT NULL AND p.isDeleted = :isDeleted ')
            ->setParameter('isDeleted', false)
            ->orderBy('p.lastMockupCacheAt', 'ASC')
            ->addOrderBy('p.id', 'ASC')
            ->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    /**
     * @param string $group
     * @param int $offset
     * @param int $limit
     * @return Entity\Product[]|array
     */
    public function getByGroup(string $group, int $offset, int $limit)
    {
        // this method is intended to be used by "special/promotional" landing pages, therefore, we don't filter by "isPublished" property,
        // as promotional products are hidden from the main website

        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Product::class)->createQueryBuilder('p')
            ->where('p.group = :group AND p.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false)
            ->setParameter('group', $group)
            ->orderBy('p.rank', 'ASC')
            ->addOrderBy('p.id', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

}