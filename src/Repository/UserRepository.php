<?php

namespace App\Repository;

use App\Entity;
use Doctrine\ORM\NoResultException;

class UserRepository extends \Doctrine\ORM\EntityRepository
{

    public function getOrCreateUser(string $email, string $firstName = null, string $lastName = null, string $phone = null)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\User::class)->createQueryBuilder('u')
            ->where('u.emailCanonical = :email')
            ->setParameter('email', $email);

        /** @var Entity\User|null $user */
        $user = null;
        try {
            $user = $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {

        }

        if ($user) {
            if ($firstName) {
                $user->setFirstName($firstName);
                $em->flush();
            }
            if ($lastName) {
                $user->setLastName($lastName);
                $em->flush();
            }
            if ($phone) {
                $user->setPhone($phone);
                $em->flush();
            }

            return $user;
        }

        $user = (new Entity\User)
            ->setEmail($email)
            ->setFirstName($firstName)
            ->setPhone($phone)
            ->setPlainPassword(bin2hex(random_bytes(5)));

        $em->persist($user);
        $em->flush();

        return $user;
    }
}