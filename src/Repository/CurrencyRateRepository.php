<?php

namespace App\Repository;

use App\Entity;

class CurrencyRateRepository extends \Doctrine\ORM\EntityRepository
{
    public function insertNewCurrencyRateData(string $currencyFrom, string $currencyTo, float $rate)
    {
        $currencyRate = (new Entity\CurrencyRate)
            ->setCurrencyFrom($currencyFrom)
            ->setCurrencyTo($currencyTo)
            ->setRate($rate)
            ->setDateCaptured(new \DateTime);

        $this->getEntityManager()->persist($currencyRate);
        $this->getEntityManager()->flush();

        return true;
    }

    /**
     * @param string $currencyFrom
     * @param string $currencyTo
     * @return Entity\CurrencyRate|null
     */
    public function getLatestCurrencyRate(string $currencyFrom, string $currencyTo)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\CurrencyRate::class)->createQueryBuilder('cr')
            ->where('cr.currencyFrom = :currencyFrom AND cr.currencyTo = :currencyTo AND cr.rate > 0')
            ->setParameter('currencyFrom', $currencyFrom)
            ->setParameter('currencyTo', $currencyTo)
            ->orderBy('cr.dateCaptured', 'DESC')
            ->setMaxResults(1);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Exception $e) {
            return null;
        }
    }
}