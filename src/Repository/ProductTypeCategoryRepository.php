<?php

namespace App\Repository;

use App\Entity;

class ProductTypeCategoryRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return Entity\ProductTypeCategory[]|array
     */
    public function getPublishedTopLevel()
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductTypeCategory::class)->createQueryBuilder('ptc')
            ->where('ptc.isPublished = :isPublished AND ptc.isTopLevel = :isTopLevel')
            ->setParameter('isPublished', true)
            ->setParameter('isTopLevel', true)
            ->orderBy('ptc.rank', 'ASC');

       return $qb->getQuery()->getResult();
    }

    /**
     * @return Entity\ProductTypeCategory[]|array
     */
    public function getAllPublished()
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductTypeCategory::class)->createQueryBuilder('ptc')
            ->where('ptc.isPublished = :isPublished')
            ->setParameter('isPublished', true)
            ->orderBy('ptc.rank', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Entity\ProductTypeCategory $productTypeCategory
     * @return Entity\ProductType[]|array
     */
    public function getPublishedChildProductTypes(Entity\ProductTypeCategory $productTypeCategory)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\ProductType::class)->createQueryBuilder('pt')
            ->where('IDENTITY(pt.productTypeCategory) IN (:ids) AND pt.isPublished = :isPublished')
            ->setParameter('isPublished', true);

        $ids = [$productTypeCategory->getId()];
        foreach ($productTypeCategory->getChildCategories() as $childCategory) {
            $ids[] = $childCategory->getId();
        }

        $qb->setParameter('ids', $ids);

        return $qb->getQuery()->getResult();
    }
}