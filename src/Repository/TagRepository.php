<?php

namespace App\Repository;

use App\Entity;

class TagRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param $title
     * @return Entity\Tag
     */
    public function getOrCreateByTitle($title)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Tag::class)->createQueryBuilder('t')
            ->where('t.title  = :title')
            ->setParameter('title', $title);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Exception $e) {
            $tag = new Entity\Tag($title);

            $this->getEntityManager()->persist($tag);
            $this->getEntityManager()->flush();

            return $tag;
        }
    }

    /**
     * @return Entity\Tag[]|array
     */
    public function getAllPublished()
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Tag::class)->createQueryBuilder('t')
            ->where('t.isPublished = :isPublished')
            ->setParameter('isPublished', true)
            ->orderBy('t.monthlyGoogleSearch', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function getAll()
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Tag::class)->createQueryBuilder('t')
            ->orderBy('t.monthlyGoogleSearch', 'DESC');

        return $qb->getQuery()->getResult();
    }
}