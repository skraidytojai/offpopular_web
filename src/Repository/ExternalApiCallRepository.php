<?php

namespace App\Repository;

use App\Entity;

class ExternalApiCallRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param $apiName
     * @param $purpose
     * @param array $options
     * @return Entity\ExternalApiCall
     */
    public function logCall($apiName, $purpose, array $options = [])
    {
        $apiCall = (new Entity\ExternalApiCall())
            ->setApiName($apiName)
            ->setPurpose($purpose);

        foreach ($options as $key => $value) {
            $setter = 'set' . ucfirst($key);
            $apiCall->{$setter}($value);
        }

        $this->getEntityManager()->persist($apiCall);
        $this->getEntityManager()->flush();

        return $apiCall;
    }
}