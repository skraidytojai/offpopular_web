<?php

namespace App\Repository;

use App\Entity;

class PrintfulMockupGenTaskRepository extends \Doctrine\ORM\EntityRepository
{
    public function createNewTask(string $taskKey, Entity\ProductType $productType, array $imagePlacements, string $error = null, Entity\Product $product = null)
    {
        $task = (new Entity\PrintfulMockupGenTask)
            ->setTaskKey($taskKey)
            ->setPrintfulProductId($productType->getPrintfulProductId())
            ->setImagePlacements(json_encode($imagePlacements))
            ->setError($error)
            ->setProduct($product)
            ->setProductType($productType);

        $this->getEntityManager()->persist($task);
        $this->getEntityManager()->flush();

        return $task;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return Entity\PrintfulMockupGenTask[]|array
     */
    public function getUncompleted($offset = 0, $limit = 10)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\PrintfulMockupGenTask::class)->createQueryBuilder('pmgt')
            ->where('pmgt.status = :status')
            ->setParameter('status', Entity\PrintfulMockupGenTask::STATUS_PENDING)
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        $qb->addSelect('RAND() as HIDDEN rand')->orderBy('rand');

        if (getenv('APP_ENV') == 'prod') {
            $qb->andWhere('pmgt.createdAt <= :fiveMinutesAgo')
                ->setParameter('fiveMinutesAgo', new \DateTime('-5 minutes'));
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return Entity\PrintfulMockupGenTask[]|array
     */
    public function getFailedForReinitiation($offset = 0, $limit = 10)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\PrintfulMockupGenTask::class)->createQueryBuilder('pmgt')
            ->where('pmgt.status = :failedStatus AND pmgt.reinitiated = :reinitiated AND pmgt.createdAt >= :earliestDate ')
            ->orWhere('pmgt.status = :processingStatus AND pmgt.reinitiated = :reinitiated AND pmgt.createdAt >= :earliestDate AND pmgt.updatedAt <= :twelveHoursAgo') // restart those "processing ones" that were pending for over 12 hours now - it probably hung because of problem on our side
            ->setParameter('failedStatus', Entity\PrintfulMockupGenTask::STATUS_FAILED)
            ->setParameter('processingStatus', Entity\PrintfulMockupGenTask::STATUS_PROCESSING)
            ->setParameter('reinitiated', false)
            ->setParameter('earliestDate', new \DateTime('2018-09-13 13:00:00'))
            ->setParameter('twelveHoursAgo', new \DateTime('-12 hours'))
            ->orderBy('pmgt.createdAt', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }
}