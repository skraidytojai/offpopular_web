<?php

namespace App\Repository;

use App\Entity;

class GameParticipantRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param Entity\User $user
     * @param int $gameNo
     * @param string $source
     * @return Entity\GameParticipant
     */
    public function userParticipate(Entity\User $user, int $gameNo, string $source, int $ticketCount = 1)
    {
        $em = $this->getEntityManager();

        $gameParticipant = (new Entity\GameParticipant)
            ->setUser($user)
            ->setGameNo($gameNo)
            ->setSource($source)
            ->setTicketCount($ticketCount);

        $em->persist($gameParticipant);
        $em->flush();

        return $gameParticipant;
    }

    /**
     * @param Entity\User $user
     * @param int $gameNo
     * @return int
     */
    public function getTicketCount(Entity\User $user, int $gameNo)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\GameParticipant::class)->createQueryBuilder('gp')
            ->select('SUM(gp.ticketCount)')
            ->where('gp.user  = :user AND gp.gameNo = :gameNo')
            ->setParameter('user', $user)
            ->setParameter('gameNo', $gameNo);

        try {
            return (int) $qb->getQuery()->getSingleScalarResult() ?? 0;
        } catch (\Exception $e) {
            return 0;
        }
    }
}