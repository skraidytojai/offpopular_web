<?php

namespace App\Repository;

use App\Entity;

class PostRepository extends \Doctrine\ORM\EntityRepository
{
    public function getPublishedBySlug($slug)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Post::class)->createQueryBuilder('p')
            ->where('p.slug  = :slug AND p.isPublished = :isPublished')
            ->setParameter('slug', $slug)
            ->setParameter('isPublished', true);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @return Entity\Post[]|array
     */
    public function getAllPublished()
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Post::class)->createQueryBuilder('p')
            ->where('p.isPublished = :isPublished')
            ->setParameter('isPublished', true)
            ->orderBy('p.id', 'ASC');

        return $qb->getQuery()->getResult();
    }
}