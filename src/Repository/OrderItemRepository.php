<?php

namespace App\Repository;

use App\Entity;
use App\Helper\PricingHelper;

class OrderItemRepository extends \Doctrine\ORM\EntityRepository
{
    public function create(Entity\Product $product, Entity\ProductVariant $productVariant)
    {
        $orderItem = (new Entity\OrderItem)
            ->setProduct($product)
            ->setProductType($productVariant->getProductType())
            ->setProductVariant($productVariant)
            ->setPrice($productVariant->getPrice())
            ->setCurrency(getenv('DISPLAY_CURRENCY'));

        $this->getEntityManager()->persist($orderItem);
        $this->getEntityManager()->flush();

        return $orderItem;
    }
}