<?php

namespace App\Twig;

use App\Entity;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig\TwigFilter;
use App\Helper\CountryHelper;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\CacheService;

class AppExtension extends AbstractExtension
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var CacheService */
    protected $cacheService;

    public function __construct
    (
        EntityManagerInterface $em,
        CacheService $cacheService
    )
    {
        $this->em = $em;
        $this->cacheService = $cacheService;
    }


    public function getFunctions()
    {
        return [
            new TwigFunction('country_code_by_ip', array(CountryHelper::class, 'determineCountryCodeByIP')),
            new TwigFunction('country_flag_path_by_ip', array(CountryHelper::class, 'getCountryFlagPathByIP')),
            new TwigFunction('get_top_level_categories', array($this, 'getPublishedTopLevelProductTypeCategories')),
            new TwigFunction('cache_service', array($this, 'getCacheService')),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('php_url_encode', array($this, 'phpUrlEncodeFilter'))
        ];
    }

    public function phpUrlEncodeFilter($string)
    {
        return urlencode($string);
    }

    public function getPublishedTopLevelProductTypeCategories()
    {
        return $this->em->getRepository(Entity\ProductTypeCategory::class)->getPublishedTopLevel();
    }

    public function getCacheService()
    {
        return $this->cacheService;
    }
}