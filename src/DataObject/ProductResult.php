<?php

namespace App\DataObject;

use App\Entity;

class ProductResult
{
    /** @var Entity\Product */
    public $product;

    /** @var Entity\ProductType */
    public $productType;

    /** @var string */
    public $url;

    /**
     * @var Entity\ProductMockup
     *
     * @deprecated
     */
    public $mainProductMockup;

    /** @var int */
    public $mainProductMockupId;

    /**
     * ProductResult constructor.
     * @param Entity\Product $product
     * @param Entity\ProductType $productType
     */
    public function __construct(Entity\Product $product, Entity\ProductType $productType)
    {
        $this->product = $product;
        $this->productType = $productType;
    }


}