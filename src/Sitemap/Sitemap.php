<?php

namespace App\Sitemap;

use JMS\Serializer\Annotation\XmlElement;

/**
 * To be used in SitemapIndex file!
 *
 * Class Sitemap
 * @package App\Sitemap
 */
class Sitemap
{
    /** @XmlElement(cdata=false) */
    protected $loc;

    /** @XmlElement(cdata=false) */
    protected $lastmod = '';

    /**
     * @param $loc
     */
    public function __construct($loc)
    {
        $this->loc = $loc;
        $this->lastmod = (new \DateTime('Last Monday'))->format('Y-m-d');
    }


}