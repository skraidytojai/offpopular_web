<?php

namespace App\Sitemap;

use JMS\Serializer\Annotation\XmlList;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\XmlNamespace;
use App\Sitemap\Url;

/**
 * @XmlRoot("urlset")
 * @XmlNamespace(uri="http://www.sitemaps.org/schemas/sitemap/0.9")
 */
class Urlset
{
    /**
     * @var Url[]
     *
     * @XmlList(inline = true, entry = "url")
     */
    protected $urls = [];

    /**
     * Urlset constructor.
     * @param array $urls
     */
    public function __construct(array $urls)
    {
        $this->urls = $urls;
    }

}