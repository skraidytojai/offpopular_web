<?php

namespace App\Sitemap;

use JMS\Serializer\Annotation\XmlElement;

class Url
{
    /** @XmlElement(cdata=false) */
    protected $loc;

    /** @XmlElement(cdata=false) */
    protected $priority = 0.8;

    /** @XmlElement(cdata=false) */
    protected $changefreq = 'daily';

    /**
     * Url constructor.
     * @param $loc
     * @param float $priority
     * @param string $changefreq
     */
    public function __construct($loc, $priority, $changefreq = 'daily')
    {
        $this->loc = $loc;
        $this->priority = $priority;
        $this->changefreq = $changefreq;
    }


}