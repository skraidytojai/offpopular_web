<?php

namespace App\Sitemap;

use JMS\Serializer\Annotation\XmlList;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\XmlNamespace;
use App\Sitemap\Url;

/**
 * @XmlRoot("sitemapindex")
 * @XmlNamespace(uri="http://www.sitemaps.org/schemas/sitemap/0.9")
 */
class SitemapIndex
{
    /**
     * @var Sitemap[]
     *
     * @XmlList(inline = true, entry = "sitemap")
     */
    protected $sitemaps = [];

    public function __construct(array $sitemaps)
    {
        $this->sitemaps = $sitemaps;
    }

}