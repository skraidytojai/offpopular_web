<?php

namespace App\Helper;

use App\Entity;

class PricingHelper
{
    /**
     * @var array
     *
     * Base edge is always the longest edge.
     * No matter what's the aspect ratio of the painting - either width or height will always correspond to the base edge.
     * For example, if base edge is 90cm, then landscape-format canvas can be: 90x60, 90x70, 90x80, portrait-format: 50x90, 70x90, 80x90, etc.
     */
    public static $baseEdgePricing = [
        'fine_art_canvas' => [
            40 => 29.99,
            60 => 59.99,
            80 => 89.99,
        ],
        'photo_print' => [
            40 => 8.49,
            60 => 16.99,
            80 => 27.99,
        ],
        'economy_canvas' => [
            40 => 14.99,
            60 => 22.99,
            80 => 33.99,
        ],
    ];

    public static function getPriceByProductTypeAndProductOption(Entity\Product $product, $productType, $productOption)
    {
        $pricingTable = self::getPricingTableForProduct($product);

        return $pricingTable[$productType][$productOption];
    }

    private static function extractBaseEdgeFromDimension($dimension)
    {
        $edges = explode('x', $dimension);

        return $edges[0] > $edges[1] ? $edges[0] : $edges[1];
    }

    public static function getPricingTableForProduct(Entity\Product $product)
    {
        $pricing = [];

        $additionalPricings = [
            'fine_art_canvas' => [
                '16x12in' => 149.95,
                '20x16in' => 159.95,
                '24x18in' => 189.95,
                '32x24in' => 269.95,
                'test' => 0.1,
            ],
            'photo_print' => [
                'a4' => 39.95,
                'a3' => 44.95,
                'a2' => 53.95,
            ],
            'phone_case' => [
                'iphone_x' => 34.99,
                'iphone_7_8' => 34.99,
                'iphone_7plus_8plus' => 34.99,
                'iphone_6_6s' => 34.99,
                'iphone_6plus_6splus' => 34.99,
                'galaxy_s7' => 34.99,
                'galaxy_s7_edge' => 34.99,
                'galaxy_s8' => 34.99,
                'galaxy_s8_plus' => 34.99,
                'galaxy_s9' => 34.99,
                'galaxy_s9_plus' => 34.99,
            ],
            'tablet' => [
                'ipad_air' => 49.95,
                'ipad_air_2' => 49.95,
                'ipad_mini_2_3' => 49.95,
                'ipad_mini_4' => 49.95,
                'ipad_2_3_4' => 49.95,
            ],
            'cushion' => [
                '12x12in_suede' => 48.99,
                '18x18in_suede' => 55.99,
                '12x12in_canvas' => 50.99,
                '18x18in_canvas' => 57.99,
                '12x12in_linen' => 51.99,
                '18x18in_linen' => 58.99,
            ],
            'tote_bag' => [
                '16x15in' => 49.95,
            ],
            'towel' => [
                'small_42x22in' => 39.95,
                'medium_58x30in' => 59.95,
                'large_66x34in' => 65.95,
            ],
            'flip_flops' => [
                'small_6_8_us' => 49.99,
                'medium_9_11_us' => 49.99,
                'large_11_13_us' => 49.99,
            ],
            'mug' => [
                '11oz' => 29.99
            ],
            'tshirt_sublimation' => [
                'xs_female' => 64.99,
                'xs_unisex' => 64.99,
                's_female' => 64.99,
                's_unisex' => 64.99,
                'm_female' => 64.99,
                'm_unisex' => 64.99,
                'l_female' => 64.99,
                'l_unisex' => 64.99,
                'xl_female' => 64.99,
                'xl_unisex' => 64.99,
                '2xl_female' => 64.99,
                '2xl_unisex' => 64.99,
            ],
            'greeting_card' => [
                'pack10_a5' => 38.99,
                'pack10_5x7in' => 38.99,
                'pack5_a5' => 25.99,
                'pack5_5x7in' => 25.99,
            ],
            'drawstring_bag' => [
                '15x17in' => 47.99,
            ],
            'backpack' => [
                '16x12x3in' => 99.99,
            ]
        ];

        $pricing = array_merge($pricing, $additionalPricings);

        return $pricing;
    }
}