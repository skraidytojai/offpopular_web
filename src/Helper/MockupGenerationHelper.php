<?php

namespace App\Helper;

use App\Entity;

class MockupGenerationHelper
{
    public static function getGenerationStrategyString(Entity\Product $product, Entity\ProductType $productType = null, string $placement = 'default')
    {
        if (is_null($productType) || $product->getDesignHasText() || $product->getDesignIsSingleImage()) {
            return '';
        }

        $genStrategyString = '';

        switch ($productType->getMockupGenerationStrategy()) {
            case Entity\ProductType::STRATEGY_MIRRORED_SLEEVE_RIGHT:
                if (in_array($placement, ['sleeve_right'])) {
                    $genStrategyString = '_mirrored';
                }
                break;
            case Entity\ProductType::STRATEGY_EXTENDED_MIRRORED:
                $genStrategyString = '_extended_mirrored';
                break;
        }

        return $genStrategyString;
    }

    public static function getJpgDesignFileUrl(Entity\Product $product, Entity\ProductType $productType, string $placement = 'default', string $resolution = '3k')
    {
        $genStrategyString = self::getGenerationStrategyString($product, $productType, $placement);

        return getenv('BASE_URL') . '/uploads/images/processed_design_jpg_files_' . $resolution . '/' . $product->getId() . $genStrategyString . '.jpg';
    }
}