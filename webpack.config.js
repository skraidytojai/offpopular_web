var Encore = require('@symfony/webpack-encore');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

Encore
// the project directory where all compiled assets will be stored
    .setOutputPath('public/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    // will create public/build/app.js and public/build/app.css
    .addEntry('app', './assets/js/app.js')
    .addStyleEntry('css', './assets/css/app.sass')
    //.addEntry('app', ['./assets/js/app.js', './assets/css/app.sass'])
    //.addStyleEntry('sass', './assets/css/app.sass')
    // allow sass/scss files to be processed
    //.enableSassLoader()
    /*.addLoader({
        test: /\.s[ac]ss$/,
        loader: 'fast-sass-loader',
        options: {
            sourceMap: !Encore.isProduction()
        }
    })*/
    //.addPlugin(new ExtractTextPlugin("app.css"))
    .addLoader({
        test: /\.(scss|sass)$/,
        use: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [
                { loader: 'css-loader', options: { minimize: Encore.isProduction() } },
                "fast-sass-loader"
            ]
        })
        /*use: [
            //'style-loader',
            //'css-loader',
            { loader: 'fast-sass-loader' }
        ]*/
    })
    //.enableSassLoader()

    // allow legacy applications to use $/jQuery as a global variable
    // .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    // create hashed filenames (e.g. app.abc123.css)
   .enableVersioning()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();